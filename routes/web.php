<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// =========================================USER===============================================================
//home
Route::get('/', 'user\userController@index');
Route::get('about', 'user\userController@about');

//user product
Route::get('product', 'user\userProductController@product');
Route::post('product/search', 'user\userProductController@productSearch');
Route::post('product/search-list', 'user\userProductController@productSearchList')->name('produk.seacrh.list');

// Product Favourite
Route::get('favourite', 'user\ProductFavouriteController@index')->name('favourite.index');
Route::post('favourite/add', 'user\ProductFavouriteController@addFavourite')->name('favourite.add');
Route::post('favourite/remove', 'user\ProductFavouriteController@removeFavourite')->name('favourite.remove');


//keranjang
Route::get('keranjang', 'user\userProductController@keranjang')->middleware('CheckLoginUser');
Route::post('keranjang/add', 'user\userProductController@keranjangTambah')->name('user.tambahKeranjang');
Route::post('keranjang/addDetail', 'user\userProductController@keranjangTambahDetail')->name('user.tambahKeranjangDetail');
Route::get('keranjang/delete/{id}', 'user\userProductController@delete')->name('user.keranjangDelete');
Route::delete('keranjang/delete/', 'user\userProductController@deleteAll')->name('keranjang.delete2');
Route::get('keranjang/detail/{id}', 'user\userProductController@detail')->name('detail.keranjang');
Route::post('keranjang/usecoupon', 'user\userProductController@useCoupon')->name('user.useCoupon');

//checkout
Route::get('checkout/{nota}', 'user\CheckoutController@checkout');
Route::post('checkout/add', 'user\CheckoutController@post');
Route::post('checkout/{nota}', 'user\CheckoutController@postCheckout')->name('checkoutPesanan');

//ongkir
Route::get('ongkir', 'user\OngkirController@get_Kota')->name('get.kota');
Route::post('ongkir', 'user\OngkirController@cekOngkir')->name('cek.ongkir');

//bayar
Route::get('checkout/bayar/tf/{nota}', 'user\CheckoutController@bayarTf')->name('checkout.bayar.tf');
Route::patch('checkout/bayar/tf/Simpan/{nota}', 'user\CheckoutController@simpan')->name('bayarTf.simpandata');
Route::get('checkout/bayar/kasir/{nota}', 'user\CheckoutController@bayarKasir')->name('checkout.bayar.kasir');

//pesanan saya
Route::get('pesanan', 'user\CheckoutController@pesanan')->middleware('CheckLoginUser');
Route::get('pesanan/detail/{nota}', 'user\CheckoutController@Detailpesanan')->name('detail');
Route::get('pesanan/resi/{nota}', 'user\CheckoutController@Resi')->name('resi');
Route::patch('pesanan/resi/konfirmasi/{nota}', 'user\CheckoutController@updateProcess')->name('resi.konfirmasi');
Route::patch('pesanan/resi/konfirmasi/batal/{nota}', 'user\CheckoutController@updateProcessBatalkan')->name('detail.konfirmasi');

//reedem point
Route::get('reedemPoint', 'user\userMemberController@reedemPoint')->middleware('CheckLoginUser');
Route::post('reedemPoint/add/{reedem_id}', 'user\userMemberController@addProcess')->name('reedem.add');

//reedem saya
Route::get('reedemSaya', 'user\userMemberController@reedemSaya')->middleware('CheckLoginUser');
Route::get('reedem/kasir/{id}', 'user\userMemberController@cekReedem')->name('reedem.kode');

//voucer
Route::get('user/voucer', 'user\userVoucerController@index');
Route::get('user/voucer/{id}/detail', 'user\userVoucerController@detail')->name('detail.voucer');

//user register
Route::get('user/register', 'user\userRegisterController@index');
Route::post('user/register/add', 'user\userRegisterController@addProcess')->name('user.register');

//user member
Route::get('user/member/status', 'user\userMemberController@status')->name('status.member')->middleware('CheckLoginUser');

//user login
Route::post('user/login', 'user\userLoginController@login')->name('user.login');

//logout
Route::get('user/logout', 'user\userLoginController@logout')->name('user.logout');

//forgot password
Route::get('user/forgotpw', 'user\UserForgotPW@index')->name('forgotpw');
Route::post('user/forgotpw2', 'user\UserForgotPW@tampil')->name('forgotpw2');
Route::patch('user/forgotpw3', 'user\UserForgotPW@forgotPw')->name('forgotpw3');

//profil
Route::get('user/akun/{no_telp}', 'user\userProfilController@update')->name('user.update')->middleware('CheckLoginUser');
Route::patch('user/{no_telp}', 'user\userProfilController@updateProcess')->name('user.updateProcess');

//update password
Route::get('user/pw/{no_telp}', 'user\userProfilController@updatePw')->name('user.update.pw')->middleware('CheckLoginUser');
Route::patch('use/pw/{no_telp}', 'user\userProfilController@updatePassword')->name('user.updatePassword');

//verifikasi OTP
Route::get('user/member/OTP', 'user\VerifikasiOtpController@index')->name('user.verifikasi');
Route::post('user/member/OTP', 'user\VerifikasiOtpController@verify')->name('verify');




//====================================ADMIN===================================
//dashboard
Route::get('/dashboard', function () {
    if (session('berhasil_login')) {
        return view('Admin.dashboard');
    } else {
        return redirect('/login');
    }
});

//login
Route::get('/login', 'admin\LoginController@index')->name('login');
Route::post('adminlogin', 'admin\LoginController@login')->name('login');

//logout
Route::get('logout', 'admin\LoginController@logout')->name('logout');

//register
Route::get('register', 'admin\RegisterController@index');
Route::post('register/add', 'admin\RegisterController@addProcess')->name('register');

//forgot password
Route::get('admin/forgot', 'admin\forgotPassword@index');
Route::post('admin/forgot/noTelp', 'admin\forgotPassword@tampil');
Route::patch('admin/forgot/pw', 'admin\forgotPassword@forgotPw');

//profil
Route::get('admin/profil/{id}', 'admin\ProfilController@index')->name('admin.update')->middleware('CheckLoginAdmin');
Route::patch('admin/{id}', 'admin\ProfilController@updateProcess');

//update password
Route::get('admin/pw/{id}', 'admin\ProfilController@updatePw')->name('admin.update.pw')->middleware('CheckLoginAdmin');
Route::patch('admin/pw/ubah/{id}', 'admin\ProfilController@updatePassword')->name('admin.updatePassword');

//Admin
Route::get('dataadmin', 'admin\AdminController@data')->middleware('CheckLoginAdmin');
Route::get('dataadmin/add', 'admin\AdminController@add');
Route::post('dataadmin', 'admin\AdminController@addProcess')->name('admin.simpandata');
Route::get('dataadmin/update/{id}', 'admin\AdminController@update')->name('admin.update');
Route::patch('dataadmin/{id}', 'admin\AdminController@updateProcess')->name('admin.updateProcess');
Route::delete('dataadmin/{id}', 'admin\AdminController@delete');

//User
Route::get('datauser', 'admin\UserAdminController@data')->middleware('CheckLoginAdmin');
Route::delete('datauser/delete/{id}', 'admin\UserAdminController@delete');

//Member
Route::get('datamember', 'admin\MemberController@data')->middleware('CheckLoginAdmin');
Route::get('datamember/update/{id}', 'admin\MemberController@update')->name('member.update');
Route::patch('datamember/{id}', 'admin\MemberController@updateProcess')->name('member.updateProcess');
Route::get('datamember/{id}/detail', 'admin\MemberController@detail')->name('member.detaildata');
Route::delete('datamember/delete/{id}', 'admin\MemberController@delete');

//produk
Route::get('produk', 'admin\ProdukController@index')->middleware('CheckLoginAdmin');
Route::get('produk/produkTambah', 'admin\ProdukController@tambah')->name('produk.tambahdata');
Route::post('produk/produkSimpan', 'admin\ProdukController@simpan')->name('produk.simpandata');
Route::get('produk/{id}/edit', 'admin\ProdukController@edit')->name('produk.editdata');
Route::patch('produk/{id}', 'admin\ProdukController@update')->name('produk.update');
Route::delete('produk/delete/{id}', 'admin\ProdukController@delete')->name('produk.deletedata');
Route::get('produk/{id}/detail', 'admin\ProdukController@detail')->name('produk.detaildata');

//Merk
Route::get('merk', 'admin\MerkController@index')->middleware('CheckLoginAdmin');
Route::get('merk/merkTambah', 'admin\MerkController@tambah')->name('merk.tambahdata');
Route::post('merk/merkSimpan', 'admin\MerkController@simpan')->name('merk.simpandata');
Route::get('merk/{id}/edit', 'admin\MerkController@edit')->name('merk.editdata');
Route::patch('merk/{id}', 'admin\MerkController@update')->name('merk.update');
Route::delete('merk/delete/{id}', 'admin\MerkController@delete')->name('merk.deletedata');

//Kategori
Route::get('kategori', 'admin\KategoriController@index')->middleware('CheckLoginAdmin');
Route::get('kategori/kategoriTambah', 'admin\KategoriController@tambah')->name('kategori.tambahdata');
Route::post('kategori/kategoriSimpan', 'admin\KategoriController@simpan')->name('kategori.simpandata');
Route::get('kategori/{id}/edit', 'admin\KategoriController@edit')->name('kategori.editdata');
Route::patch('kategori/{id}', 'admin\KategoriController@update')->name('kategori.update');
Route::delete('kategori/delete/{id}', 'admin\KategoriControllerr@delete')->name('kategori.deletedata');

//voucer
Route::get('voucer', 'admin\VoucerController@index')->middleware('CheckLoginAdmin');
Route::get('voucer/voucerTambah', 'admin\VoucerController@tambah')->name('voucer.tambahdata');
Route::post('voucer/voucerSimpan', 'admin\VoucerController@simpan')->name('voucer.simpandata');
Route::get('voucer/{id}/edit', 'admin\VoucerController@edit')->name('voucer.editdata');
Route::patch('voucer/{id}', 'admin\VoucerController@update')->name('voucer.update');
Route::delete('voucer/delete/{id}', 'admin\VoucerController@delete')->name('voucer.deletedata');
Route::get('voucer/{id}/detail', 'admin\VoucerController@detail')->name('voucer.detaildata');

//transaksi
Route::get('transaksi', 'admin\TransaksiController@index')->middleware('CheckLoginAdmin');
Route::get('transaksi/update/{nota}', 'admin\TransaksiController@update')->name('transaksi.update');
Route::patch('transaksi/{nota}', 'admin\TransaksiController@updateProcess')->name('transaksi.updateProcess');
Route::get('transaksi/{nota}/detail', 'admin\TransaksiController@detail')->name('transaksi.detaildata');
Route::delete('transaksi/delete/{nota}', 'admin\TransaksiController@delete');

//Reedem Poin
Route::get('reedem', 'admin\reedemPointController@index')->middleware('CheckLoginAdmin');
Route::get('reedem/reedemTambah', 'admin\reedemPointController@tambah')->name('reedem.tambahdata');
Route::post('reedem/reedemSimpan', 'admin\reedemPointController@simpan')->name('reedem.simpandata');
Route::get('reedem/{id}/edit', 'admin\reedemPointController@edit')->name('reedem.editdata');
Route::patch('reedem/{id}', 'admin\reedemPointController@update')->name('reedem.update');
Route::delete('reedem/delete/{id}', 'admin\reedemPointController@delete')->name('reedem.deletedata');
Route::get('reedem/{id}/detail', 'admin\reedemPointController@detail')->name('reedem.detaildata');

//transaksi Reedem
Route::get('transaksiReedem', 'admin\TransaksiReedemController@index')->middleware('CheckLoginAdmin');
Route::get('transaksiReedem/update/{id_transaksi}', 'admin\TransaksiReedemController@update')->name('transaksiReedem.update');
Route::patch('transaksiReedem/{id_transaksi}', 'admin\TransaksiReedemController@updateProcess')->name('transaksiReedem.updateProcess');
Route::delete('transaksiReedem/delete/{id_transaksi}', 'admin\TransaksiReedemController@delete');
