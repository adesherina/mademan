<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    @include('user.layouts.head')
    <link href="https://fonts.googleapis.com/css2?family=B612:ital,wght@1,700&display=swap" rel="stylesheet">
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>My Account</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Riwayat Pesanan</a></li>
                        <li class="breadcrumb-item active">Cek Nomor Resi</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <div class="cart-box-main">
        <div class="container">
            <div class="row new-account-login">
                <div class="col-sm-12 col-lg-12 mb-12">
                    <div class="title-left">
                        <h3>Cek Nomor Resi</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                    <div class="card" style=" box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
                    transition: all 0.3s cubic-bezier(.25,.8,.25,1);">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <p class="h5 text-center"><b>
                                        Nomor Resi
                                    </b></p>
                                    <hr class="text-dark mt-2">
                                </div>
                            </div>
                            <div class="row">
                            <?php
                                $jasa = $transaksi->jasa_kirim;
                                $jasa_kirim = json_decode($jasa, true);
                            ?>
                            @foreach ($jasa_kirim as $key => $d)
                                <div class="col-lg-6 col-md-6 col-sm-12 col-12 mt-3 order-lg-1 order-2">
                                <p class="h5">Cek Nomor Resi Anda!!!</p>
                                    <div class="referal-code mt-3">
                                    @if($d['jasaPengirim'] == "pos")
                                        <div class="col-12">
                                            <img src="{{url('assets/user/images/pos.png')}}" class="d-inline mt-2 " style="width:120px;height:45px" alt="">
                                        </div>
                                            <input type="text" class="form-control d-inline text-center mt-5" id="resi" style="width: 40%;" value="{{$d['resi']}}" readonly>
                                            <button class="d-inline btn btn-primary" style="background-color: #ae8547 !important;border:none !important" onclick="copy_text()"><i class="fa fa-clone" aria-hidden="true"></i></button>
                                    @elseif($d['jasaPengirim'] == "tiki")
                                        <div class="col-12">
                                            <img src="{{url('assets/user/images/tiki.png')}}" class="d-inline mt-2 mb-4" style="width:120px;height:45px" alt="">
                                        </div>
                                        <input type="text" class="form-control d-inline text-center " id="resi" style="width: 40%;" value="{{$d['resi']}}" readonly>
                                        <button class="d-inline btn btn-primary" style="background-color: #ae8547 !important;border:none !important" onclick="copy_text()"><i class="fa fa-clone" aria-hidden="true"></i></button>
                                    @elseif($d['jasaPengirim'] == "jne")
                                        <div class="col-12">
                                            <img src="{{url('assets/user/images/jne.png')}}" class="d-inline mt-2" style="width:120px;height:110px" alt="">
                                        </div>
                                        <input type="text" class="form-control d-inline text-center" id="resi" style="width: 40%;" value="{{$d['resi']}}" readonly>
                                        <button class="d-inline btn btn-primary" style="background-color: #ae8547 !important;border:none !important" onclick="copy_text()"><i class="fa fa-clone" aria-hidden="true"></i></button>
                                    @endif
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-12 order-lg-2 order-1 text-center ">
                                    <img src="{{url('assets/user/images/kurir.png')}}" class="img-fluid"  alt="">
                                </div>
                            </div>
                            @endforeach
                                <form action="{{route('resi.konfirmasi', $nota)}}" method="POST" enctype="multipart/form-data" class=" col-12 float-right">
                                @csrf
                                @method('patch')
                                    <div class="row">
                                        <div class="col-lg-12 col-12 d-flex order-3 order-lg-3 shopping-box mt-4 "> 
                                        <input type="submit" class="ml-auto btn hvr-hover text-white " value="Pesanan Diterima" /> 
                                    </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

       <!-- Start Instagram Feed  -->
       @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
    <script type="text/javascript">
        function copy_text() {
            document.getElementById("resi").select();
            document.execCommand("copy");
            alert("Nomor Resi berhasil dicopy");
        }
    </script>
</body>

</html>