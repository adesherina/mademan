@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Transaksi Penjualan</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Transaksi Penjualan</a></div>
        <div class="breadcrumb-item">Transaksi Penjualan</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('transaksi.updateProcess', $ts->nota)}}" method="POST">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Edit Transaksi Penjualan</h4>
                <div class="card-header-action">
                  <a href="{{url('transaksi')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Lengkap</label>
                  <input type="text" class="form-control" name="nama_lengkap" value="{{$ts->nama_lengkap}}" disabled>
                </div>
                <div class="form-group">
                  <label>No Telepon</label>
                  <input type="text" class="form-control" name="no_telp" value="{{$ts->no_telp}}" disabled>
                </div>
                <div class="form-group">
                  <label>Qty</label>
                  <input type="text" class="form-control" name="jumlah" value="{{$ts->jumlah}}" disabled>
                </div>
                <div class="form-group">
                  <label>Total Bayar</label>
                  <input type="text" class="form-control" name="total" value="Rp.  {{number_format($ts->total,'0','.','.')}}" disabled>
                </div>
                <?php   
                  $jasa_kirim = $ts->jasa_kirim;
                  $jasa_kirim = json_decode($jasa_kirim, true); 
                ?>
                @if($jasa_kirim !== null)
                    @foreach ($jasa_kirim as $key => $d)
                      <div class="form-group">
                        <label>Jasa Kirim</label>
                        <input type="text" class="form-control" name="jasa_kirim" value="{{$d['jasaPengirim']}}" disabled>
                      </div>
                      <div class="form-group">
                        <label>Nomor Resi</label>
                        <input type="hidden" name="no_kurir" value="{{$d['no']}}">
                        <input type="text" class="form-control" name="resi" value="{{$d['resi']}}">
                      </div>
                    @endforeach
                @endif
                <div class="form-group" >
                  <label>Status Pesanan</label>
                  <select class="form-control" name="status_pesanan" >
                    <option value="{{$ts->status_pesanan}}" selected>{{$ts->status_pesanan}}</option>
                    <option value="Menunggu Verifikasi">Menunggu Verifikasi</option>
                    <option value="Dikemas">Dikemas</option>
                    <option value="Dikirim">Dikirim</option>
                    <option value="Selesai">Selesai</option>
                    <option value="Dibatalkan">Dibatalkan</option>
                  </select>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>   
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 