<!DOCTYPE html>
<html lang="en">
<head>
  @include('Admin.layouts.header')
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>

        @include('Admin.layouts.navbar')
        @include('Admin.layouts.sidebar')

        <!-- Main Content -->
       @yield('content') 
       
    </div>
  </div>
  
    @include('Admin.layouts.footer')
    @include('Admin.layouts.footer')
  <!-- General JS Scripts -->
    @include('Admin.layouts.js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/25.0.0/classic/ckeditor.js"></script>
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

    @stack('script')
</body>
</html>
