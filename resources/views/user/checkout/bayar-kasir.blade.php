<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    @include('user.layouts.head')
    <link href="https://fonts.googleapis.com/css2?family=B612:ital,wght@1,700&display=swap" rel="stylesheet">
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Pembayaran</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Checkout</a></li>
                        <li class="breadcrumb-item active">Pembayaran</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <div class="cart-box-main">
        <div class="container">
            <div class="row new-account-login">
                <div class="col-sm-12 col-lg-12 mb-12">
                    <div class="title-left">
                        <h3>Bayar di Kasir</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                    <div class="card" style=" box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
                    transition: all 0.3s cubic-bezier(.25,.8,.25,1);">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <p class="h5 text-center"><b>
                                        Kode Pesanan
                                    </b></p>
                                    <hr class="text-dark mt-2">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-12 mt-5">
                                    {{-- <h3 class="ml-3">Jumlah Harus Dibayar : Rp. </h3> --}}
                                    <h3 class="ml-3">Tunjukan kode dibawah ini pada kasir!!!</h3>
                                    <h4 class="mr-3 text-center mt-4 font-weight-bold mb-5" style="font-size: 2em;font-family: 'B612', sans-serif;">{{$nota}}</h4>
                                    
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                        <img src="{{url('assets/user/images/casier.png')}}" class="float-right" style="width:300px;height:240px" alt="">
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

       <!-- Start Instagram Feed  -->
       @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
    <script type="text/javascript">
    function copy_text() {
        document.getElementById("referal").select();
        document.execCommand("copy");
        alert("Kode Referal berhasil dicopy");
    }
</script>
</body>

</html>