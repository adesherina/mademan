@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
   <section class="section">
      <div class="section-header">
        <h1>Produk</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Data Produk</a></div>
          <div class="breadcrumb-item">Produk</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Data Produk</h4>
            <div class="card-header-action">
              <a href="{{route('produk.tambahdata')}}" class="btn btn-success btn-lg">
              Add
              </a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="tabel-data" class="table table-bordered table-md">
                <thead>
                  <tr>
                    <th class="text-center">
                       No
                    </th>
                    <th>Brand</th>
                    <th>Kategori</th>
                    <th>Nama Produk</th>
                    <th>Harga</th>
                    <th>Stok</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach ($produk as $produk)
                 <tr>
                   <td class="text-center">
                     {{ $loop->iteration }}
                    </td>
                    <td>{{$produk->nama_merk}}</td>
                    <td>{{$produk->nama_kategori}}</td>
                    <td>{{$produk->nama_produk}}</td>
                    <td>Rp.  {{number_format($produk->harga,'0','.','.')}}</td>
                    <td>{{$produk->stok}}</td>
                    <td>
                      <a href="{{route('produk.editdata', $produk->id_produk)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                      </a>
                      <form action="{{route('produk.deletedata', $produk->id_produk)}}" id="delete{{$produk->id_produk}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                      @method('delete')
                      <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                      @csrf
                      </form>
                      <a href="{{route('produk.detaildata', $produk->id_produk)}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-eye"></i></a>
                    </td>
                 </tr>
                @endforeach
                </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</section>
</div> 
@endsection