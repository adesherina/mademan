<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class userProductController extends Controller
{
    //tampil produk
    public function product()
    {
        $produk = DB::table('produk')->get();
        return view('user.product.product', ['produk' => $produk]);
    }

    //detail data
    public function detail($id)
    {
        $produk = DB::table('produk')->where('id_produk', $id)->first();
        return view('user.product.detail', ['produk' => $produk]);
    }

    //tampil keranjang
    public function keranjang()
    {
        $keranjang = DB::table('keranjang')
            ->where('no_telp', Session::get('no_telp'))
            ->join('produk', 'produk.id_produk', '=', 'keranjang.id_produk')
            ->get();
        return view('user.product.keranjang', ['keranjang' => $keranjang]);
    }

    //tambah keranjang
    public function keranjangTambah(Request $request)
    {
        //cari data produk
        $produk = DB::table('keranjang')
            ->where('no_telp', Session::get('no_telp'))
            ->where('id_produk', $request->id_produk)->first();


        if ($produk === null) {
            $keranjang = DB::table('keranjang')->insert(
                [
                    'id_produk' => $request->id_produk,
                    'no_telp' => Session::get('no_telp'),
                    'harga' => $request->harga,
                    'qty' => $request->qty,
                    'subtotal' => $request->harga * $request->qty
                ]
            );
            return json_encode(array(
                "statusCode" => 200,
                "produk" => json_encode($produk)
            ));
        } elseif ($produk != null) {
            $qty = (int)$produk->qty + (int)$request->qty;
            $keranjang = DB::table('keranjang')
                ->where('id_produk', $request->id_produk)
                ->where('no_telp', Session::get('no_telp'))
                ->update(
                    [
                        'qty' => $qty,
                        'subtotal' => (int)$produk->harga * $qty
                    ]
                );
            return json_encode(array(
                "statusCode" => 200,
                "produk" => json_encode($produk)
            ));
        }
    }

    //tambah keranjang Detail
    public function keranjangTambahDetail(Request $request)
    {
        $produk = DB::table('keranjang')
            ->where('no_telp', Session::get('no_telp'))
            ->where('id_produk', $request->id_produk)
            ->first();

        if (Session::get('no_telp') != null) {
            $keranjang = DB::table('keranjang')->insert(
                [

                    'id_produk' => $request->id_produk,
                    'no_telp' => Session::get('no_telp'),
                    'harga' => $request->harga,
                    'qty' => $request->qty,
                    'subtotal' => $request->harga * $request->qty

                ]
            );
        }
        return redirect('keranjang')->with('status', 'Data Berhasil Ditambahkan');
    }

    //hapus keranjang
    public function delete($id)
    {
        DB::table('keranjang')->where('id_keranjang', $id)->delete();

        return redirect('keranjang')->with('status', 'Data Berhasil Dihapus');
    }

    //delete all
    public function deleteAll(Request $request)
    {
        $id = $request->id;
        DB::table("keranjang")->whereIn('id_keranjang', explode(",", $id))->delete();
        return response()->json(['success' => "Products Deleted successfully."]);
    }

    //use Coupon code
    public function useCoupon(Request $request)
    {
        $today = Carbon::now()->toDateString();
        $coupon = DB::table('voucer')->where('kode_voucer', $request->coupon)->where('expaired_voucer', '>=', $today)->first();
        if ($coupon != null) {
            return json_encode(array(
                "statusCode" => 200,
                "coupon" => json_encode($coupon->diskon)
            ));
        } else {
            return json_encode(array(
                "statusCode" => 201,
                "coupon" => "coupon tidak ada atau coupon kadaluwarsa"
            ));
        }
    }


    public function productSearch(Request $req)
    {
        if ($req->sorting === "null") {
            $produk_search = DB::table('produk')->where('nama_produk', 'like', '%' . $req->keyword . '%')->where('merk', 'LIKE', '%' . $req->brand . '%')->where('kategori', 'LIKE', '%' . $req->categories . '%')->get();
        } elseif ($req->sorting === "desc") {
            $produk_search = DB::table('produk')->where('nama_produk', 'like', '%' . $req->keyword . '%')->where('merk', 'LIKE', '%' . $req->brand . '%')->where('kategori', 'LIKE', '%' . $req->categories . '%')->orderByDesc('harga')->get();
        } elseif ($req->sorting === "asc") {
            $produk_search = DB::table('produk')->where('nama_produk', 'like', '%' . $req->keyword . '%')->where('merk', 'LIKE', '%' . $req->brand . '%')->where('kategori', 'LIKE', '%' . $req->categories . '%')->orderBy('harga', 'asc')->get();
        }
        if (count($produk_search) > 0) {
            echo '<div class="row">';
            foreach ($produk_search as $item) {
                echo '<div class="col-sm-6 col-md-6 col-lg-4 col-xl-4 mt-4">';
                echo '<div class="products-single fix">';
                echo '<div class="box-img-hover">';
                echo '<div class="type-lb">';
                echo '<p class="new">New</p>';
                echo '</div>';
                $url = url('/foto-produk/' . $item->foto);
                echo '<img src="' . $url . '" style="width:264px; height:264px" class="img-fluid" alt="Image">';
                echo '<div class="mask-icon">';
                echo '<ul>';
                $url_detail = url('keranjang/detail/' . $item->id_produk);
                $favourite = DB::table('favourites')->where('product_id',$item->id_produk )->where('no_telp',Session::get('no_telp') )->first();
                echo '<li><a href="' . $url_detail . '" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>';
                echo ' <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-sync-alt"></i></a></li>';
                echo '<li><div id="favourite_product_{{$produk->id_produk}}">';
                if ($favourite === null){
                    echo '<a href="#" onclick="addFavourite(' .$item->id_produk .')" id="favourite_'.$item->id_produk .'" data-toggle="tooltip" data-placement="right" data-id-user="'.Session::get('no_telp') .'" title="Add to Wishlist"><i class="far fa-heart"></i></a>';
                }else{
                    echo ' <a href="#" onclick="removeFavourite(' .$item->id_produk .')" id="favourite_'.$item->id_produk.'" data-toggle="tooltip" data-placement="right" data-id-user="'.Session::get('no_telp').'" title="Remove from Wishlist"><i class="fas fa-heart"></i></a>';
                }
                echo '</li>';
                echo '</ul>';
                echo '<a class="cart tambah-keranjang" onclick="addKeranjang(' . $item->id_produk . ')" id="keranjang_btn_' . $item->id_produk . '"  href="#" data-id-user="' . Session::get('no_telp') . '" data-produkid="' . $item->id_produk . '" data-produkharga="' . $item->harga . '" data-produkqty="1" data-produkcsrf="' . Session::token() . '">Add to Cart</a>';
                echo '</div>';
                echo '</div>';
                echo '<div class="why-text"  style="height: 120px;">';
                echo '<h4>' . $item->nama_produk . '</h4>';
                echo "<h5>Rp.  " . number_format($item->harga, '0', '.', '.') . "</h5>";
                echo '</div>';
                echo '</div>';
                echo '</div>';
            }
            echo '</div>';
        } else {
            echo '<div class="row">';
            echo '<div class="col-sm-6 col-md-6 col-lg-4 col-xl-4 mt-4 text-center">';
            echo '<h3 >Produk Tidak Tersedia</div>';
            echo '</div>';
            echo '</div>';
        }
    }

    public function productSearchList(Request $req)
    {
        if ($req->sorting === "null") {
            $produk_search = DB::table('produk')->where('nama_produk', 'like', '%' . $req->keyword . '%')->where('merk', 'LIKE', '%' . $req->brand . '%')->where('kategori', 'LIKE', '%' . $req->categories . '%')->get();
        } elseif ($req->sorting === "desc") {
            $produk_search = DB::table('produk')->where('nama_produk', 'like', '%' . $req->keyword . '%')->where('merk', 'LIKE', '%' . $req->brand . '%')->where('kategori', 'LIKE', '%' . $req->categories . '%')->orderByDesc('harga')->get();
        } elseif ($req->sorting === "asc") {
            $produk_search = DB::table('produk')->where('nama_produk', 'like', '%' . $req->keyword . '%')->where('merk', 'LIKE', '%' . $req->brand . '%')->where('kategori', 'LIKE', '%' . $req->categories . '%')->orderBy('harga', 'asc')->get();
        }
        if (count($produk_search) > 0) {
            echo '<div class="list-view-box">';
            foreach ($produk_search as $item) {
                echo  '<div class="row">';
                echo  '<div class="col-sm-6 col-md-6 col-lg-4 col-xl-4 mt-5">';
                echo '<div class="products-single fix">';
                echo '<div class="box-img-hover">';
                echo '<div class="type-lb">';
                echo '<p class="new">New</p>';
                echo '</div>';
                echo '<img src="' . url('/foto-produk/' . $item->foto) . '" class="img-fluid" alt="Image">';
                echo '<div class="mask-icon">';
                echo '<ul>';
                echo '<li><a href="' . route('detail.keranjang', $item->id_produk) . '" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>';
                echo '<li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-sync-alt"></i></a></li>';
                echo '<li><div id="favourite_product_{{$produk->id_produk}}">';
                $favourite = DB::table('favourites')->where('product_id',$item->id_produk )->where('no_telp',Session::get('no_telp') )->first();
                if ($favourite === null){
                    echo '<a href="#" onclick="addFavourite(' .$item->id_produk .')" id="favourite_'.$item->id_produk .'" data-toggle="tooltip" data-placement="right" data-id-user="'.Session::get('no_telp') .'" title="Add to Wishlist"><i class="far fa-heart"></i></a>';
                }else{
                    echo ' <a href="#" onclick="removeFavourite(' .$item->id_produk .')" id="favourite_'.$item->id_produk.'" data-toggle="tooltip" data-placement="right" data-id-user="'.Session::get('no_telp').'" title="Remove from Wishlist"><i class="fas fa-heart"></i></a>';
                }
                echo '</li>';
                echo '</ul>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '<div class="col-sm-6 col-md-6 col-lg-8 col-xl-8">';
                echo '<div class="why-text full-width single-produk" style="height: 315px;">';
                echo '<h4>' . $item->nama_produk . '</h4>';
                echo '<h5 class="mb-3">Rp. ' . $item->harga . '</h5>';
                echo '<p>' . \Illuminate\Support\Str::words($item->deskripsi, 20, '....') . '</p>';
                echo '<input type="hidden" value="' . $item->harga . '" name="harga" id="harga_input_' . +$item->id_produk . '"  />';
                echo '<a class="btn hvr-hover" onclick="addKeranjangDetail(' . $item->id_produk . ')" id="add_keranjang_detail_' . $item->id_produk . '" href="#" data-id-user="' . Session::get('no_telp') . '" data-produk-id="' . $item->id_produk . '" data-produkharga="' . $item->harga . '" data-produkqty="1" data-produkcsrf="' . Session::token() . '">Add to Cart</a>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
            }
            echo '</div>';
        } else {
            echo '<div class="row">';
            echo '<div class="col-sm-6 col-md-6 col-lg-4 col-xl-4 mt-4 text-center">';
            echo '<h3 >Produk Tidak Tersedia</div>';
            echo '</div>';
            echo '</div>';
        }
    }
}
