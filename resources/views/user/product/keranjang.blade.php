<!DOCTYPE html>
<html lang="en">
<head>
    @include('user.layouts.head')
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->
    
    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Cart</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Shop</a></li>
                        <li class="breadcrumb-item active">Cart</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start Cart  -->
    <div class="cart-box-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    @if($keranjang->isEmpty())
                    <img src="{{url('assets/user/images/keranjang.png')}}" alt="" style="width: 30%;height:80%" class="text-center mb-3" >
                    <div class="d-block mt-2">
                        <a href="{{url('product')}}" class=" text-white btn hvr-hover ">Tambah Produk</a>
                    </div>
                    @else
                    <div class="table-main table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" id="checkAll"  >
                                    </th>
                                    <th>Gambar Produk</th>
                                    <th>Nama Produk</th>
                                    <th>Harga</th>
                                    <th>Quantity</th>
                                    <th>Subtotal</th>
                                    <th><a href="#" id="deleteAll" data-url="{{ route('keranjang.delete2') }}" data-csrf="{{Session::token()}}" class="text-white"> Hapus</a></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($keranjang as $produk)
                                <tr class="penjualan">
                                    <td id="90">
                                        <input type="hidden" name="id_keranjang" class="id_keranjang" id="id_keranjang" value="{{$produk->id_keranjang}}">
                                        <input type="hidden" name="harga" value="{{$produk->harga}}" class="harga-produk">
                                        <input type="hidden" name="id_produk" value="{{$produk->id_produk}}" class="id-produk">
                                        <input type="checkbox" id="60" price="20" name="id_keranjang[]" price="2" class="checkedItem" data-harga="<?= $produk->harga; ?>" data-qty="<?= $produk->qty ?>" data-id="<?= $produk->id_keranjang?>" data-nama="<?$produk->nama_prouduk?>" data-subtotal="<?$produk->subtotal?>"></td>
                                    <td class="thumbnail-img">
                                        <a href="#">
                                        <img class="img-fluid" src="{{url('/foto-produk/'.$produk->foto)}}" alt="gambar produk" />
                                        </a>
                                    </td>
                                    <td class="name-pr">
                                            {{$produk->nama_produk}}
                                    </td>
                                    <td class="price-pr" id="40">
                                        <p>Rp. <span class="harga">{{number_format($produk->harga,'0','.','.')}}</span></p>
                                    </td>
                                    <td class="quantity-box">

                                        <input type="number" size="4" class="qty" id="99" value="{{$produk->qty}}" min="0" step="1" class="c-input-text qty text"></td>
                                    <td class="total-pr" id="100">
                                        <p >Rp. <span class="subtotal" id="44">{{number_format($produk->subtotal,'0','.','.')}}</span> </p>
                                    </td>
                                    <td class="remove-pr">
                                        <a href="{{route('user.keranjangDelete', $produk->id_keranjang)}}" class="deleteKeranjang">
                                        <i class="fas fa-times"></i>
                                    </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row my-5">
                        <div class="col-lg-6 col-sm-6">
                            <div class="coupon-box">
                                <div class="input-group input-group-sm">
                                    <input type="hidden" name="" id="csrf-coupon" value="{{ csrf_token() }}">
                                    <input class="form-control" placeholder="Enter your coupon code" id="coupon-input" aria-label="Coupon code" type="text">
                                    <div class="input-group-append">
                                        <button class="btn btn-theme" id="apply-coupon" type="button">Apply Coupon</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-sm-12"></div>
                            <div class="col-lg-4 col-sm-12 mt-4">
                                <div class="order-box">
                                    <h3>Total Pesanan Anda</h3>
                                    <div class="d-flex">
                                        <h4>Jumlah Produk</h4>
                                        <div class="ml-auto font-weight-bold"> <span id="jml_produk"></span></div>
                                    </div>
                                    <div class="d-flex">
                                        <h4>Sub Total</h4>
                                        <div class="ml-auto font-weight-bold">Rp. <span id="subtotal"></span></div>
                                    </div>
                                    <div class="d-flex">
                                        <h4>Discount</h4>
                                        <div class="ml-auto font-weight-bold">Rp. <span id="voucher-span">0</span></div>
                                    </div>
                                    <hr class="my-1">
                                    <div class="d-flex gr-total">
                                        <h5>Total</h5>
                                        <div class="ml-auto h5">Rp. <span id="total">0</span> </div>
                                    </div>
                                    <hr> 
                                </div>
                            </div>
                            <input type="hidden" name="" id="csrf" value="{{Session::token()}}">
                        <div class="col-12 d-flex shopping-box"><a href="#" id="checkout" class="ml-auto btn hvr-hover">Checkout</a> </div>
                    </div>
                    @endif
                </div>
            </div> 
        </div>
    </div>
    <!-- End Cart -->

    <!-- Start Instagram Feed  -->
    @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')

    <script>
        $('#apply-coupon').click(function(){
            let coupon_inp = $('#coupon-input').val();
            let csrf_coupon = $('#csrf-coupon').val();
            let subtotal_coupon = $('#subtotal').text();

            var tempSubtotal = parseFloat(subtotal_coupon.replace(".", "").replace(".",""));

            if (coupon_inp != "") {
            //   $("#butsave").attr("disabled", "disabled");
            $.ajax({
                url: "{{ route('user.useCoupon') }}",
                type: "POST",
                data: {
                    _token: csrf_coupon,
                    coupon: coupon_inp,
                },
                cache: false,
                success: function (dataResult) {
                    console.log(dataResult);
                    var dataResult = JSON.parse(dataResult);
                    if (dataResult.statusCode == 200) {
                        console.log(dataResult.coupon);
                        let diskon = (parseInt(dataResult.coupon)/100)*tempSubtotal;
                        // convert to rupiah
                        var number_coupon = diskon.toString(),
                            sisacoupon 	= number_coupon.length % 3,
                            rupiahcoupon 	= number_coupon.substr(0, sisacoupon),
                            ribuancoupon 	= number_coupon.substr(sisacoupon).match(/\d{3}/g);
                                
                        if (ribuancoupon) {
                            separatorcoupon = sisacoupon ? '.' : '';
                            rupiahcoupon += separatorcoupon + ribuancoupon.join('.');
                        }
                        $('#voucher-span').text(rupiahcoupon);
                        var total = tempSubtotal-diskon;
                        var number_coupon = total.toString(),
                            sisacoupon 	= number_coupon.length % 3,
                            rupiahcoupon 	= number_coupon.substr(0, sisacoupon),
                            ribuancoupon 	= number_coupon.substr(sisacoupon).match(/\d{3}/g);
                                
                        if (ribuancoupon) {
                            separatorcoupon = sisacoupon ? '.' : '';
                            rupiahcoupon += separatorcoupon + ribuancoupon.join('.');
                        }
                        $('#total').text(rupiahcoupon);


                    } else if (dataResult.statusCode == 201) {
                        swal({
                            title: "Opps!",
                            text: dataResult.coupon,
                            icon: "error",
                        });
                    }
                },
                error: function (error) {
                    console.log(error);
                },
            });
        } else {
            alert("Please fill all the field !");
        }
        });

    </script>

</body>

</html>