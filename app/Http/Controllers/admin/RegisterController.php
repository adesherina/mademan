<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function index(){
        return view('Admin.register');
    }

    public function addProcess(Request $request)
    {
         $rules = [
            'nama' => 'required|min:5|max:100',
            'username' => 'required|min:5',
            'password' => 'required|min:8'
        ];
 
        $messages = [
            'nama.required'  => 'Nama  wajib diisi.',
            'username.required'         => 'Username wajib diisi.',
            'password.required'      => 'Password wajib diisi.',
            'password.min'           => 'Password minimal diisi dengan 8 karakter.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
         
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $timestamps = true;
        //query builder insert
        DB::table('admin')->insert(
            [
                'nama' => $request->nama,
                'username' => $request->username,
                'password' => Hash::make($request->password),
            ]
        );
        //Redirect dengan status 
        // dd($request->all());
        return redirect('/login')->with('status', 'Data Berhasil Ditambahkan');
    }
}
