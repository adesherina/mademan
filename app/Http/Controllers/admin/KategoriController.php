<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    //tampil data
    public function index(){
        $kategori = DB::table('kategori')->get();
        return view('Admin.product.kategori.list', ['kategori'=> $kategori]);
    }

    //tambah data
    public function tambah(){
        return view('Admin.product.kategori.tambah');
    }

    //simpan data
    public function simpan(Request $request){
        $save = DB::table('kategori')->insert(
           [
                'nama_kategori'=>$request->nama_kategori,
           ]);
        return redirect('kategori')->with('status', 'Data Berhasil Ditambahkan');
    }

    //edit data
    public function edit($id){
        $kategori = DB::table('kategori')->where('id_kategori', $id)->first();
        return view('Admin.product.kategori.edit', ['kategori'=> $kategori]);

    }

    //update
    public function update(Request $request, $id)
    {
        DB::table('kategori')
        ->where('id_kategori', $id)->update([
                'nama_kategori'=>$request->nama_kategori,
                ]);
        return redirect('kategori')->with('status', 'Data Berhasil Diedit');

    }

    //hapus data
    public function delete($id)
    {
        DB::table('kategori')->where('id_kategori', $id)->delete();
        
        return redirect('kategori')->with('status', 'Data Berhasil Dihapus');
    }

}
