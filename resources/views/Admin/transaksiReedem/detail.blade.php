@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Detail Transaksi Reedem</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Detail Transaksi Reedem</a></div>
          <div class="breadcrumb-item">Transaksi Reedem</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Detail Transaksi Reedem</h4>
            <div class="card-header-action">
              <a href="{{url('transaksiReedem')}}" class="btn btn-success btn-lg">
              kembali
            </a>
            </div>
          </div>

            <div class="col-12 mt-3 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-body">
                      <div class="row mb-3">
                            <div class="col-2">
                                <p class="card-text text-primary">Nama Lengkap</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$tsr->nama_lengkap}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-2">
                                <p class="card-text text-primary">No Telepon</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$tsr->no_telp}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-2">
                                <p class="card-text text-primary">Point yang dipunyai</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$tsr->point}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-2">
                                <p class="card-text text-primary">Point yg harus ditukar</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$tsr->point}}</p>
                            </div>
                        </div>
                </div>
            </div>

        </div>
      </div>
  </section>
</div> 
@endsection