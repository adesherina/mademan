<!DOCTYPE html>
<html lang="en">

    <head>
        @include('user.layouts.head')
    </head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>My Account</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Riwayat Pesanan</a></li>
                        <li class="breadcrumb-item active">Detail Pesanan</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

<!-- Start Conten -->
<div class="cart-box-main">
    <div class="container">
        <div class="row">
            <?php
                $pembeli = $transaksi->data_pembeli;
                $data_pembeli = json_decode($pembeli, true);
            ?>
            @foreach ($data_pembeli as $key => $d)
                <div class="col-sm-6 col-lg-6 mb-3">
                    <div class="checkout-address">
                        <div class="title-left">
                            <h3>Alamat</h3>
                        </div>
                        <form class="needs-validation" novalidate>
                            <div class="mb-3">
                                <label for="username">Nama Lengkap *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="username" placeholder="" value="{{$d['namaLengkap']}}" name="nama_lengkap" disabled>
                                    </div>
                            </div>
                            <div class="mb-3">
                                <label for="username">No Telepon *</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="username" placeholder=""value="{{$d['noTelp']}}" name="no_telp" disabled>
                                    </div>
                            </div>
                            <div class="mb-3">
                                <label for="email">Alamat *</label>
                                <div class="input-group">
                                    <textarea class="form-control" id="message" name="alamat" rows="4" data-error="Write your message" disabled>{{$d['alamat']}}</textarea>
                                </div>
                            </div>  
                            <hr class="mb-4">
                            <div class="title"> <span>Metode Pembayaran</span> 
                            </div>
                            <div class="d-block my-3">
                                <div class="custom-control custom-radio">
                                    <input id="credit" name="paymentMethod" type="radio" @if($transaksi->jasa_kirim === null) checked @endif value="Bayar di kasir" class="custom-control-input " disabled>
                                    <label class="custom-control-label" for="credit">Bayar di kasir</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input id="debit" name="paymentMethod" type="radio" @if($transaksi->jasa_kirim !== null) checked @endif  value="Bayar Via Transfer" class="custom-control-input" disabled>
                                    <label class="custom-control-label" for="debit">Bayar Via Transfer</label>
                                </div>
                            </div>
                            <hr class="mb-1"> 
                        </form>
                    </div>
                </div>
            @endforeach
            <div class="col-sm-6 col-lg-6 mb-3">
                <div class="row">
                    
                    <div class="col-md-12 col-lg-12">
                        @if($transaksi->jasa_kirim != null)
                            <?php
                                $jasa = $transaksi->jasa_kirim;
                                $jasa_kirim = json_decode($jasa, true);
                            ?>
                            @foreach ($jasa_kirim as $key => $d)
                                <div class="shipping-method-box">
                                    <div class="title-left">
                                        <h3>Pengiriman</h3>
                                    </div>
                                    <div class="mb-4">
                                        <div class="custom-control custom-radio">
                                            <input id="shippingOption1" name="shipping_option"  @if($d['jasaPengirim'] === "tiki") checked @endif value="tiki" class="custom-control-input delivery-method" type="radio" disabled>
                                            <label class="custom-control-label" for="shippingOption1">Tiki</label> <span class="float-right font-weight-bold">@if($d['jasaPengirim'] === "tiki") Rp. {{$d['ongkir']}} @endif</span> 
                                        </div>
                                        <div class="ml-4 mb-2 small">(2-4 business days)</div>
                                        <div class="custom-control custom-radio">
                                            <input id="shippingOption2" name="shipping_option" @if($d['jasaPengirim'] === "jne") checked @endif value="jne" class="custom-control-input delivery-method" type="radio" disabled>
                                            <label class="custom-control-label" for="shippingOption2">JNE</label> <span class="float-right font-weight-bold">@if($d['jasaPengirim'] === "jne") Rp. {{$d['ongkir']}} @endif</span> 
                                        </div>
                                        <div class="ml-4 mb-2 small">(2-4 business days)
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input id="shippingOption3" name="shipping_option" @if($d['jasaPengirim'] === "pos") checked @endif value="pos" class="custom-control-input delivery-method" type="radio" disabled>
                                            <label class="custom-control-label" for="shippingOption3">Pos Indonesia</label> <span class="float-right font-weight-bold">@if($d['jasaPengirim'] === "pos") Rp. {{$d['ongkir']}} @endif</span> 
                                        </div>
                                        <div class="ml-4 mb-2 small">(2-4 business days)
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="shipping-method-box">
                                <div class="title-left">
                                    <h3>Pengiriman</h3>
                                </div>
                                <div class="mb-4">
                                    <div class="custom-control custom-radio">
                                        <input id="shippingOption1" name="shipping_option" value="J&T" class="custom-control-input delivery-method" type="radio" disabled>
                                        <label class="custom-control-label" for="shippingOption1">J&T</label> <span class="float-right font-weight-bold">FREE</span> 
                                    </div>
                                    <div class="ml-4 mb-2 small">(2-4 business days)</div>
                                    <div class="custom-control custom-radio">
                                        <input id="shippingOption2" name="shipping_option" value="JNE" class="custom-control-input delivery-method" type="radio" disabled>
                                        <label class="custom-control-label" for="shippingOption2">JNE</label> <span class="float-right font-weight-bold">$10.00</span> 
                                    </div>
                                    <div class="ml-4 mb-2 small">(2-4 business days)
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input id="shippingOption3" name="shipping_option"  value="Sicepat" class="custom-control-input delivery-method" type="radio" disabled>
                                        <label class="custom-control-label" for="shippingOption3">Sicepat</label> <span class="float-right font-weight-bold">$20.00</span> 
                                    </div>
                                    <div class="ml-4 mb-2 small">(2-4 business days)
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-12 col-lg-12">
                        <div class="odr-box">
                            <div class="title-left">
                                <h3>Produk Yang Anda Pesan</h3>
                            </div>
                            <div class="rounded p-2 bg-light">
                                <?php 
                                    use Illuminate\Support\Facades\DB;
                                    $produk =  DB::table('detail_transaksi')->where('nota', $nota)->join('produk', 'produk.id_produk', '=', 'detail_transaksi.id_produk')->get();
                                ?>
                                @foreach($produk as $produk)
                                    <div class="media mb-2 border-bottom">
                                        <div class="media-body"> <a href="detail.html"> {{$produk->nama_produk}}</a>
                                            <div class="small text-muted">Rp.  {{number_format($produk->harga,'0','.','.')}} <span class="mx-2">|</span> Qty: {{$produk->qty}} <span class="mx-2">|</span> Subtotal: Rp. {{number_format($produk->subtotal,'0','.','.')}}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12">
                        <div class="order-box">
                            <div class="title-left">
                                <h3 class="mt-5">Pesanan Anda</h3>
                            </div>
                            <div class="d-flex">
                                <div class="font-weight-bold">Produk</div>
                                <div class="ml-auto font-weight-bold">Total</div>
                            </div>
                            <hr class="my-1">
                            <div class="d-flex">
                                <h4>Sub Total</h4>
                                @php
                                 $ts = DB::table('detail_transaksi')
                                    ->where('nota', $nota)
                                    ->get(); 
                                    $subtotal = 0;
                                    foreach($ts as $item):
                                       $subtotal += (int)$item->subtotal;
                                    endforeach;
                                @endphp
                                <div class="ml-auto font-weight-bold">Rp. {{number_format($subtotal,'0','.','.')}} </div>
                            </div>
                            <div class="d-flex">
                                <h4>Discount</h4>
                                <div class="ml-auto font-weight-bold"> Rp. {{number_format($transaksi->diskon,'0','.','.')}}</div>
                            </div>
                            <?php
                                $jasa = $transaksi->jasa_kirim;
                                $jasa_kirim = json_decode($jasa, true);
                            ?>
                            @foreach ($jasa_kirim as $key => $d)
                                <div class="d-flex">
                                    <h4>Ongkir</h4>
                                    <div class="ml-auto font-weight-bold">Rp. {{$d['ongkir']}}</div>
                                </div>
                            @endforeach
                            <hr class="my-1">
                            <div class="d-flex gr-total">
                                <h5>Total</h5>
                                <div class="ml-auto h5">Rp. {{number_format($transaksi->total,'0','.','.')}}</div>
                            </div>
                            <hr> 
                        </div>
                    </div>
                    <form action="{{route('detail.konfirmasi', $nota)}}" method="POST" enctype="multipart/form-data" class=" col-12 float-right">
                    @csrf
                    @method('patch')
                        <div class="col-12 d-flex shopping-box"> 
                            <input type="submit" class="ml-auto btn hvr-hover text-white float-right" value="Batalkan Pesanan" /> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Cart -->

    <!-- Start Instagram Feed  -->
    @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')

    <script>
        $('#credit').click(function() {
            if($('#credit').is(':checked')) { 
                $('.delivery-method').attr('disabled',true);
             }
        });
         $('#debit').click(function() {
            if($('#debit').is(':checked')) { 
                $('.delivery-method').removeAttr('disabled',true);
             }
        });
    </script>
</body>

</html>