<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Transaksi;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class CheckoutController extends Controller
{
     //tampil checkout
    public function checkout($nota){
        $checkout = DB::table('detail_transaksi')
        ->where('no_telp',Session::get('no_telp'))
        ->where('nota',$nota)
        ->join('produk', 'produk.id_produk', '=', 'detail_transaksi.id_produk')
        ->get();
        $transaksi = Transaksi::where('nota',$nota)->first();

        return view('user.checkout.checkout',compact('checkout','nota','transaksi'));

    }

    public function generateRandomString($length = 5) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function post(Request $request){
        $nota = $this->generateRandomString();
        $id = $request->id;
        $harga = $request->harga;

        Transaksi::create([
            'nota' => $nota, 
            'no_telp'=>Session::get('no_telp'),
            'diskon'=>$request->fixdiskon,
            'jumlah'=>0,
            'total'=>0,
            'status_pesanan' => "unverified" 
        ]);
        
        foreach ($id as $key => $id) {

            DB::table('detail_transaksi')->insert([
            'nota' => $nota, 
            'keranjang_id' => $request->id_keranjang[$key],
            'id_produk' => $request->id[$key],
            'no_telp' => Session::get('no_telp'),
            'harga' => $request->harga[$key],
            'qty' => $request->qty[$key],
            'subtotal' => (int) $request->harga[$key] * (int) $request->qty[$key]
            ]);
        }

         return json_encode(array(
                    "statusCode"=>200,
                    "nota"=>$nota
            ));
    }

    public function postCheckout(Request $req, $nota){
        $ongkir = $result = str_replace(".", "", $req->harga_ongkir);
        $transaksi  = Transaksi::where('nota',$nota)->first();
        

        $qty = DB::table('detail_transaksi')
            ->where('nota', $nota)
            ->sum('qty');

        $subtotal = DB::table('detail_transaksi')
            ->where('nota', $nota)
            ->sum('subtotal');

       $data_pembeli = json_decode($transaksi->data_pembeli, true);
       $data_pembeli[] = array('no'=> $this->generateRandomString(), 'namaLengkap' => $req->nama_lengkap,'noTelp' => $req->no_telp, 'alamat' => $req->alamat);
        
       if($req->has('shipping_option')){
            $data_pengiriman = json_decode($transaksi->jasa_kirim, true);
            $data_pengiriman[] = array('no'=> $this->generateRandomString(), 'jasaPengirim' => $req->shipping_option,'resi' => null, 'ongkir' => $req->harga_ongkir);
            $postTransaksi = Transaksi::where('nota', $nota)->update([
                'jumlah' => $qty,
                'total' => ((int)$subtotal+ (int) $ongkir)  - (int)$transaksi->diskon,
                'status_pesanan' => "unpaid",
                'jasa_kirim' => json_encode($data_pengiriman),
                'data_pembeli' => json_encode($data_pembeli),
            ]);
            foreach($req->id_keranjang as $key => $value) {
                DB::table('keranjang')->where('id_keranjang',$req->id_keranjang[$key])->delete();
            }

            return redirect('checkout/bayar/tf/'. $nota);
        }else{
            $postTransaksi = Transaksi::where('nota', $nota)->update([
                'jumlah' => $qty,
                'total' => $subtotal - $transaksi->diskon,
                'status_pesanan' => "unpaid",
                'jasa_kirim' => null,
                'data_pembeli' => json_encode($data_pembeli),
            ]);
            foreach($req->id_keranjang as $key => $value) {
                DB::table('keranjang')->where('id_keranjang',$req->id_keranjang[$key])->delete();
            }
            return redirect('checkout/bayar/kasir/'. $nota);
        }

        


    }

    //tampil bayar tf
    public function bayarTf($nota){
        return view('user.checkout.bayar-tf', compact('nota'));

    }

    //update bayar tf
    public function simpan(Request $request, $nota){
        // dd($request->all());
        if($request->hasFile('bukti_tf')){
            $resorce = $request->file('bukti_tf');
            
            $name   = time()."_".$resorce->getClientOriginalName();
            // dd($name, $nota);
            // $path = $request->file('bukti_tf')->storeAs('public/bukti-tf', $name);
            // $path = $request->file('bukti_tf')->store('public/bukti-tf');
            $resorce->move(\base_path() ."/public/bukti-tf", $name);
            $save = DB::table('transaksi')->where('nota', $nota)->update(
                [
                    'bukti_tf' => $name,
                    'status_pesanan' => "Menunggu Verifikasi"
                ]);
            // dd($path);
            echo "Foto berhasil di upload";
        }else{
            
            echo "Foto Gagal di upload";
        }
       
        return redirect('pesanan')->with('status', 'Data Berhasil Ditambahkan');
    }

    //tampil bayar kasir
    public function bayarKasir($nota){
        return view('user.checkout.bayar-kasir',compact('nota'));
    }

    //tampil pesanan saya
    public function pesanan(){
        DB::table('transaksi')
                ->where('status_pesanan', 'unverified')
                ->delete();
        $pesanan = DB::table('transaksi')
                ->where('transaksi.no_telp',Session::get('no_telp'))
                ->get();
        return view('user.profil.pesanan', ['pesanan'=> $pesanan]);
    }

    //tampil detail pesanan
    public function Detailpesanan($nota){
        $transaksi = DB::table('transaksi')
        ->where('transaksi.nota',$nota)
        ->first();

        return view('user.profil.detail-pesanan',compact('transaksi','nota'));
        // return view('user.profil.detail-pesanan',compact('nota'));
    }

    //tampil resi
    public function Resi($nota){
        $transaksi = DB::table('transaksi')
        ->where('transaksi.nota',$nota)
        ->first();

        return view('user.profil.resi',compact('transaksi','nota'));
    }

    //update Status Selesai
    public function updateProcess(Request $request, $nota)
    {
        $data = DB::table('transaksi')->where('nota', $nota)->first();
        DB::table('transaksi')->where('nota', $nota)->update([
            'status_pesanan' => "Selesai", 
        ]);
        $member = DB::table('member')
        ->where('no_telp', $data->no_telp)
        ->first();
        if($member != null ){
            DB::table('member')->where('no_telp', $data->no_telp)->update([
            'point' => (int)$member->point + 5, 
            ]);
            
        }
        
        return redirect('pesanan');
    }

    //update Status Dibatalkan
    public function updateProcessBatalkan(Request $request, $nota)
    {
        $data = DB::table('transaksi')->where('nota', $nota)->first();
        DB::table('transaksi')->where('nota', $nota)->update([
            'status_pesanan' => "Dibatalkan", 
        ]);
        
        return redirect('pesanan');

    }
}
