<!DOCTYPE html>
<html lang="en">
<head>
@include('Admin.layouts.header')
</head>

<body>
    <div id="app">
        <section class="section">
        <div class="container mt-5 ">
            <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                <div class="login-brand mt-5">
                    Forgot Password
                {{-- <img src="../assets/img/stisla-fill.svg" alt="logo" width="100" class="shadow-light rounded-circle"> --}}
                </div>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @elseif (session('filed'))
                    <div class="alert alert-danger">
                       {{ session('filed') }}
                    </div>
                @endif
                <div class="card card-primary">
                <div class="card-header"><h4>Forgot Password</h4></div>

                <div class="card-body">
                    <form action="{{url('admin/forgot/noTelp')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('post')
                    <div class="form-group">
                        <label for="no_telp">Nomor Telepon</label>
                        <input  type="no_telp" class="form-control" name="no_telp" tabindex="1" placeholder="Masukan Nomor Telepon Anda" required autofocus >
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                        Lanjut
                        </button>
                    </div>
                    </form>
                </div>
                </div>
            </div>
            </div>
        </div>
        </section>
    </div>
   @include('Admin.layouts.js')
</body>
</html>