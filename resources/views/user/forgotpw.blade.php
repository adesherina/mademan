<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    @include('user.layouts.head')
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Forgot Password</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Account</a></li>
                        <li class="breadcrumb-item active">Forgot Password</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <div class="cart-box-main">
        <div class="container">
            <div class="row new-account-login">
                <div class="col-sm-12 col-lg-12 mb-12">
                    <div class="title-left">
                    <h3>Forgot Password</h3>
                         <!-- Alert  -->
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @elseif (session('filed'))
                            <div class="alert alert-danger">
                                {{ session('filed') }}
                            </div>
                        @endif
                    </div>
                    <h5><a data-toggle="collapse" href="#formLogin" role="button" aria-expanded="false">Nomor Telepon</a></h5>
                    <form class="mt-3 collapse review-form-box" id="formLogin" action="{{route('forgotpw2')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('post')
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" name="no_telp" placeholder="Masukan Nomo Telepon Anda"> 
                            </div>
                        <button type="submit" class="btn hvr-hover">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- Start Instagram Feed  -->@include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
</body>

</html>