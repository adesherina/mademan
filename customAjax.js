// tambah keranjang
$(".tambah-keranjang").on("click", function () {
    event.preventDefault();
    var id_produk = $(this).data("produkid");
    var harga = $(this).data("produkharga");
    var qty = $(this).data("produkqty");
    var csrf = $(this).data("produkcsrf");
    // console.log(id_produk);
    var id_user = $(this).data("id-user");
    // console.log(id_user);
    if (id_user != "") {
        if (id_produk != "") {
            $.ajax({
                url: "keranjang/add", //harus sesuai url di buat di route
                type: "POST",
                data: {
                    _token: csrf,
                    id_produk: id_produk,
                    qty: qty,
                    harga: harga,
                },
                cache: false,
                success: function (dataResult) {
                    var dataResult = JSON.parse(dataResult);
                    console.log(dataResult.produk, dataResult.keranjang);
                    if (dataResult.statusCode == 200) {
                        swal({
                            title: "Produk telah di tambahkan ke keranjang",
                            icon: "success",
                            type: "success",
                        });
                    } else if (dataResult.statusCode == 201) {
                        alert("Error occured !");
                    }
                },
                error: function (error) {
                    console.log(error);
                },
            });
        } else {
            alert("Please fill all the field !");
        }
    } else {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Anda Harus Login Terlebih Dahulu",
        });
    }
});

//tambah keranjang
$(".tambah-keranjang2").each(function (index, value) {
    $(this).on("click", function () {
        var id_produk = $(this).data("produk-id");
        var harga = $(this).data("produkharga");
        var qty = $(this).data("produkqty");
        var csrf = $(this).data("produkcsrf");
        console.log(id_produk);
    });
});

//detail keranjang
$(document).ready(function () {
    $(".detail-keranjang").on("click", function () {
        // agar tidak scrolling ketika klik link
        event.preventDefault();

        var id_produk = $("#id_product").val();
        var qty = $("#quantity").val();
        var harga = String($("#harga").text());
        var hargafix = parseFloat(harga.replace(".", "").replace(".", ""));
        console.log(hargafix);
        if (id_produk != "") {
            //   $("#butsave").attr("disabled", "disabled");
            $.ajax({
                url: "/keranjang/add",
                type: "POST",
                data: {
                    _token: $("#product-csrf").val(),
                    id_produk: id_produk,
                    qty: qty,
                    harga: hargafix,
                },
                cache: false,
                success: function (dataResult) {
                    console.log(dataResult);
                    var dataResult = JSON.parse(dataResult);
                    if (dataResult.statusCode == 200) {
                        swal({
                            title: "Produk telah di tambahkan ke keranjang",
                            icon: "success",
                            type: "success",
                        });
                    } else if (dataResult.statusCode == 201) {
                        alert("Error occured !");
                    }
                },
                error: function (error) {
                    console.log(error);
                },
            });
        } else {
            alert("Please fill all the field !");
        }
    });
});

//alert delete keranjang
$(".deleteKeranjang").each(function (index, value) {
    $(this).on("click", function (event) {
        event.preventDefault();
        const url = $(this).attr("href");
        swal({
            title: "Apakah kamu yakin?",
            text: "Produk ini akan di hapus?",
            icon: "warning",
            buttons: ["Batal", "Ya!"],
        }).then(function (value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
});
