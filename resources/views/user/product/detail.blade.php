
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    @include('user.layouts.head')
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Shop Detail</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Shop</a></li>
                        <li class="breadcrumb-item active">Shop Detail </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start Shop Detail  -->
    <div class="shop-detail-box-main">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-6">
                    <div id="carousel-example-1" class="single-product-slider carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active"> <img class="d-block w-100" src="{{url('/foto-produk/'.$produk->foto)}}" alt="First slide"> </div>
                            <div class="carousel-item"> <img class="d-block w-100" src="{{url('/foto-produk/'.$produk->foto)}}" alt="Second slide"> </div>
                            <div class="carousel-item"> <img class="d-block w-100" src="{{url('/foto-produk/'.$produk->foto)}}" alt="Third slide"> </div>
                        </div>
                        <a class="carousel-control-prev" href="#carousel-example-1" role="button" data-slide="prev"> 
						<i class="fa fa-angle-left" aria-hidden="true"></i>
						<span class="sr-only">Previous</span> 
					</a>
                        <a class="carousel-control-next" href="#carousel-example-1" role="button" data-slide="next"> 
						<i class="fa fa-angle-right" aria-hidden="true"></i> 
						<span class="sr-only">Next</span> 
					</a>
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-1" data-slide-to="0" class="active">
                                <img class="d-block w-100 img-fluid" src="{{url('/foto-produk/'.$produk->foto)}}" alt="" />
                            </li>
                            <li data-target="#carousel-example-1" data-slide-to="1">
                                <img class="d-block w-100 img-fluid" src="{{url('/foto-produk/'.$produk->foto)}}" alt="" />
                            </li>
                            <li data-target="#carousel-example-1" data-slide-to="2">
                                <img class="d-block w-100 img-fluid" src="{{url('/foto-produk/'.$produk->foto)}}" alt="" />
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-7 col-md-6">
                    <div class="single-product-details">
                        <form action="{{route('user.tambahKeranjangDetail')}}" class="d-inline " data-id-user="{{Session::get('no_telp')}}" method="POST">
                        @method('post')
                        @csrf
                            <input type="hidden" name="id_produk" id="id_produk-detail" value="{{$produk->id_produk}}" id="">
                            <h2>{{$produk->nama_produk}}</h2>
                            <h5>Rp. <span id="harga"> {{number_format($produk->harga,'0','.','.')}}</span></h5>
                            <input type="hidden" name="harga" id="harga-detail" value="{{$produk->harga}}" id="">
                            <p class="available-stock"><span> Stok :{{$produk->stok}}</span>
                                <p>
                                    <h4> Description:</h4>
                                    <p>{{$produk->deskripsi}}</p>
                                    <ul>
                                        <li>
                                            <div class="form-group size-st">
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-group quantity-box">
                                                <label class="control-label">Quantity</label>
                                                <input type="hidden" name="id_produk" id="id_product" value="{{$produk->id_produk}}">
                                                <input type="hidden" name="product-csrf" id="product-csrf" value="{{Session::token()}}">
                                                <input class="form-control" id="quantity" value="1" min="0" max="20" type="number" name="qty">
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="price-box-bar">
                                        <div class="cart-and-bay-btn">
                                                <button class="btn hvr-hover text-white">Buy New</button>
                                            <!-- <a class="btn hvr-hover" data-fancybox-close="" href="#" >Buy New</a> -->
                                            <a class="d-inline btn hvr-hover detail-keranjang" data-fancybox-close="" href="#" data-id-user="{{Session::get('no_telp')}}" data-produkid="{{$produk->id_produk}}" data-produkharga="{{$produk->harga}}" data-produkqty="1" data-produkcsrf="{{Session::token()}}">Add to cart</a>
                                        </div>
                                    </div>
                                </p>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Cart -->


    <!-- Start Instagram Feed  -->
    @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
</body>

</html>