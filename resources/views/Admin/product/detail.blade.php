@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Detail Produk</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Detail Produk</a></div>
          <div class="breadcrumb-item">Produk</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Detail Produk</h4>
            <div class="card-header-action">
              <a href="{{url('produk')}}" class="btn btn-success btn-lg">
              kembali
            </a>
            </div>
          </div>

            <div class="col-12 mt-3 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-body">
                      <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Nama Produk</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                              <div class="overflow-auto text-primary" style="max-height: 200px">
                                {{$produk->nama_produk}}
                              </div>
                            </div>
                      </div>
                      <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Merk</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                              <div class="overflow-auto text-primary" style="max-height: 200px">
                                {{$produk->merk}}
                              </div>
                            </div>
                      </div>
                      <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Harga</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                              <div class="overflow-auto text-primary" style="max-height: 200px">
                                Rp. {{number_format($produk->harga,'0','.','.')}}
                              </div>
                            </div>
                      </div>
                      <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Stok</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                              <div class="overflow-auto text-primary" style="max-height: 200px">
                                {{$produk->stok}}
                              </div>
                            </div>
                      </div>
                      <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Deskripsi</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                              <div class="overflow-auto text-primary" style="max-height: 200px">
                                <?= $produk->deskripsi ?>
                              </div>
                            </div>
                      </div>
                      <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Deskripsi</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                              <div class="overflow-auto text-primary" style="max-height: 200px">
                                <img src="{{url('/foto-produk/'.$produk->foto)}}" style="width: 200px;">
                              </div>
                            </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
      </div>
  </section>
</div> 
@endsection