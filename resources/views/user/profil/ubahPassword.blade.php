
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    @include('user.layouts.head')
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>My Account</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <li class="breadcrumb-item active">Ubah Password</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

<!-- Start Contact Us  -->
    <div class="contact-box-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <img src="{{url('user-profil/'. $user->photo)}}" class="img-thumbnail" style="width: 200px;height:200px; border-radius:50%;border:2px solid #ae8547" alt="">
                </div>
                <div class="col-lg-8 col-sm-8">
                    <div class="contact-form-right">
                    <!-- Alert Tambah Data -->
                        @if (session('editpw'))
                        <div class="alert alert-success mt-3">
                        {{ session('editpw') }}
                        </div>
                        @elseif(session('failedpw'))
                        <div class="alert alert-danger mt-3">
                        {{ session('failedpw') }}
                        </div>
                        @endif
                        <h2 class="mb-3">Ubah Password</h2>
                        <form method="post" action="{{route('user.updatePassword', $user->no_telp)}}" enctype="multipart/form-data">
                        @csrf
                        @method('patch')
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="subject" name="newPassword" placeholder="Password" required data-error="Please enter your Password">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="subject" name="confirmPassword" placeholder="Konfirmasi Password" required data-error="Please enter your Password">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="submit-button text-left">
                                        <button class="btn hvr-hover" style="background-color: #ae8547;" id="submit" type="submit">Simpan</button>
                                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Cart -->

    <!-- Start Instagram Feed  -->
    @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
</body>

</html>