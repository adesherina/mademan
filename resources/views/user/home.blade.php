<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
@include('user.layouts.head')
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
        @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Konten -->
    <!-- Start Slider -->
    <div id="slides-shop" class="cover-slides">
        <ul class="slides-container">
            <li class="text-left">
                <img src="{{url('assets/user/images/baner1.png')}}" alt="">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="m-b-20"><strong>Selamat Datang di <br> Mademan Barbershop</strong></h1>
                            <p class="m-b-40">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br> Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <p><a class="btn hvr-hover" href="{{url('product')}}">Shop New</a></p>
                        </div>
                    </div>
                </div>
            </li>
            <li class="text-center">
                <img src="{{url('assets/user/images/baner2.webp')}}" alt="">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="m-b-20"><strong>Selamat Datang di <br> Mademan Barbershop</strong></h1>
                            <p class="m-b-40">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <p><a class="btn hvr-hover" href="{{url('product')}}">Shop New</a></p>
                        </div>
                    </div>
                </div>
            </li>
            <li class="text-right">
                <img src="{{url('assets/user/images/baner3.jpg')}}" alt="">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="m-b-20"><strong>Selamat Datang di <br> Mademan Barbershop</strong></h1>
                            <p class="m-b-40">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <p><a class="btn hvr-hover" href="{{url('product')}}">Shop New</a></p>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <div class="slides-navigation">
            <a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
            <a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
        </div>
    </div>
    <!-- End Slider -->

    <!-- Start Categories  -->
    <div class="categories-shop">
        <div class="container">
            <div class="row">
                @foreach($produk as $produk)
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="shop-cat-box">
                        <img class="img-fluid" src="{{url('/foto-produk/'.$produk->foto)}}" alt="" />
                        <a class="btn hvr-hover" href="{{route('detail.keranjang', $produk->id_produk)}}">{{$produk->nama_produk}}</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- End Categories -->

    <!-- Start Instagram Feed  -->
    
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
    
</body>

</html>