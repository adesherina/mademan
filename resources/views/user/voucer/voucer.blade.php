<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
     <link rel="stylesheet" href="{{url('assets/user/coupon.css')}}">
    @include('user.layouts.head')
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Voucer</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Voucer</a></li>
                        <li class="breadcrumb-item active">Voucer</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start Content -->
        <div class="container mt-5 mb-5">
            <div class="row">
                @foreach($voucer as $item)
                <div class="col-lg-3 col-md-2 col-sm-12 col-12 mb-3 mt-4">
                    <div class='coupon_box mx-auto'>
                        <div class='body'>
                            <h4 class='title'> {{$item->nama_voucer}}</h4>
                            <h2 class='how_much'> <b>{{$item->diskon}}%</b></h2>
                            <h3>OFF</h3>
                        </div>
                        
                        <a href="{{route('detail.voucer', $item->id_voucer)}}" class="btn" style="background-color: #fff;">Detail</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>


    <!-- Start Instagram Feed  -->
    @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
</body>

</html>