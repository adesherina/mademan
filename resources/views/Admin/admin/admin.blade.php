@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
        <section class="section">
          <div class="section-header">
              <h1>Admin</h1>
              <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data Admin</a></div>
                <div class="breadcrumb-item">Admin</div>
              </div>
          </div>
          <div class="section-body">
            <!-- Alert  -->
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card card-primary">
              <div class="card-header">
                <h4>Data Admin</h4>
                <div class="card-header-action">
                  <a href="dataadmin/add" class="btn btn-success btn-lg">
                    Add
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-md" id="tabel-data">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($admins as $admin)
                      <tr>
                        <td>{{$loop->iteration}}</td>  
                        <td>{{$admin->nama}}</td>
                        <td>{{$admin->username}}</td>
                        <td>
                          <a href="{{route('admin.update', $admin->id_admin)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                          </a>
                          <form action="{{url('dataadmin', $admin->id_admin)}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                              @method('delete')
                              <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                              @csrf
                          </form></td>
                      </tr>    
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div> 
        </section>
      </div> 
@endsection
 