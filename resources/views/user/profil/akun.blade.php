
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    @include('user.layouts.head')
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>My Account</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <li class="breadcrumb-item active">Ubah Profil</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

<!-- Start Contact Us  -->
    <div class="contact-box-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <img src="{{url('user-profil/'. $user->photo)}}" class="img-thumbnail" style="width: 200px;height:200px; border-radius:50%;border:2px solid #ae8547" alt="">
                </div>
                <div class="col-lg-8 col-sm-8">
                    <div class="contact-form-right">
                        <!-- Alert Update Data -->
                        @if (session('statusSuccess1'))
                            <div class="alert alert-success mt-3">
                                {{ session('statusSuccess1') }}
                            </div>
                        @elseif(session('statusSuccess2'))
                            <div class="alert alert-success mt-3">
                                {{ session('statusSuccess2') }}
                            </div>
                        @endif
                        <h2>Profil</h2>
                        <form method="post" action="{{route('user.updateProcess', $user->no_telp)}}" enctype="multipart/form-data">
                        @csrf
                        @method('patch')
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="nama_lengkap" value="{{$user->nama_lengkap}}" required data-error="Please enter your name">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="subject" name="no_telp" value="{{$user->no_telp}}" required data-error="Please enter your Subject">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="subject" name="email" value="{{$user->email}}" required data-error="Please enter your Subject">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" name="jenis_kelamin" class="custom-control-input" <?php if($user->jenis_kelamin == "Laki-laki"){ ?> checked <?php } ?>  value="Laki-laki"><span class="custom-control-label">Laki - Laki</span>
                                    </label>
                                    <label class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" name="jenis_kelamin" class="custom-control-input" <?php if($user->jenis_kelamin == "Perempuan"){ ?> checked <?php } ?>  value="Perempuan"><span class="custom-control-label">Perempuan</span>
                                    </label>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" id="message" name="alamat" value="{{$user->alamat}}" rows="4" data-error="Write your message" required>{{$user->alamat}}</textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="file" class="form-control" id="subject" name="photo" data-error="Please enter your Subject">
                                        <input type="hidden" name="oldPhoto" value="{{$user->photo}}">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="submit-button text-left">
                                        <button class="btn hvr-hover" id="submit" type="submit">Simpan</button>
                                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Cart -->

    <!-- Start Instagram Feed  -->
    @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
</body>

</html>