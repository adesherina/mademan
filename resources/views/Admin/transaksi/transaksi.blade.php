@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Transaksi Penjualan</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Data Transaksi Penjualan</a></div>
          <div class="breadcrumb-item">Transaksi Penjualan</div>
        </div>
      </div>
      <div class="section-body">
          <!-- Alert Tambah Data -->
          @if (session('status'))
          <div class="alert alert-success">
            {{ session('status') }}
          </div>
          @endif
          <div class="card card-primary">
              <div class="card-header">
                <h4>Data Transaksi Penjualan</h4>
                <div class="card-header-action">
                  <a href="#" class="btn btn-success btn-lg">
                  View All
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="tabel-data" class="table table-bordered table-md">
                    <thead>
                      <tr>
                        <th class="text-center">
                          No
                        </th>
                        <th>Nota</th>
                        <th>Nama Pengguna</th>
                        <th>No Telepon</th>
                        <th>Qty</th>
                        <th>Total Bayar</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                       @foreach ($ts as $ts)
                      <tr>
                        <td>{{$loop->iteration}}</td>  
                        <td>{{$ts->nota}}</td>
                        <td>{{$ts->nama_lengkap}}</td>
                        <td>{{$ts->no_telp}}</td>
                        <td>{{$ts->jumlah}}</td>
                        <td>Rp.  {{number_format($ts->total,'0','.','.')}}</td>
                        <td>
                          @if($ts->status_pesanan == "unpaid")
                            <div class="badge badge-warning">{{$ts->status_pesanan}}</div>
                           @elseif($ts->status_pesanan == "Menunggu Verifikasi")
                            <div class="badge badge-warning">{{$ts->status_pesanan}}</div>
                          @elseif($ts->status_pesanan == "Dikemas")
                            <div class="badge badge-info">{{$ts->status_pesanan}}</div>
                          @elseif($ts->status_pesanan == "Dikirim")
                            <div class="badge badge-primary">{{$ts->status_pesanan}}</div>
                          @elseif($ts->status_pesanan == "Selesai")
                            <div class="badge badge-success">{{$ts->status_pesanan}}</div>
                          @elseif($ts->status_pesanan == "Dibatalkan")
                            <div class="badge badge-danger">{{$ts->status_pesanan}}</div>
                          @endif
                        </td>
                        <td>
                          <a href="{{route('transaksi.update', $ts->nota)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i></a>
                          <form action="{{url('transaksi/delete', $ts->nota)}}" id="delete{{$ts->nota}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                              @method('delete')
                              <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                              @csrf
                          </form>
                          <a href="{{route('transaksi.detaildata', $ts->nota)}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-eye"></i></a>
                      </td>
                      </tr>    
                      @endforeach
                      </tbody>
                    </table>
                  </div>
              </div>
        </div>
      </div>
  </section>
</div>
    
@endsection