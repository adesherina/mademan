@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Transaksi Reedem Poin</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Data Transaksi Reedem Poin</a></div>
          <div class="breadcrumb-item">Transaksi Reedem Poin</div>
        </div>
      </div>
      <div class="section-body">
          <!-- Alert Tambah Data -->
          @if (session('status'))
          <div class="alert alert-success">
            {{ session('status') }}
          </div>
          @endif
          <div class="card card-primary">
              <div class="card-header">
                <h4>Data Transaksi Reedem Poin</h4>
                <div class="card-header-action">
                  <a href="#" class="btn btn-success btn-lg">
                  View All
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="tabel-data" class="table table-bordered table-md">
                    <thead>
                      <tr>
                        <th class="text-center">
                          No
                        </th>
                        <th>Kode Reedem</th>
                        <th>Nama Pengguna</th>
                        <th>No Telepon</th>
                        <th>Point yang harus ditukar</th>
                        <th>Point yang dipunyai</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                       @foreach ($tsr as $ts)
                      <tr>
                        <td>{{$loop->iteration}}</td>  
                        <td>{{$ts->id_transaksi}}</td>
                        <td>{{$ts->nama_lengkap}}</td>
                        <td>{{$ts->no_telp}}</td>
                        <td>{{$ts->point_reedem}}</td>
                        <td>{{$ts->point_member}}</td>
                        <td>
                          @if($ts->status == "Belum Dikonfirmasi")
                          <div class="badge badge-warning">{{$ts->status}}</div>
                          @elseif($ts->status == "Selesai")
                          <div class="badge badge-success">{{$ts->status}}</div>
                          @endif
                        </td>
                        <td>
                          <a href="{{route('transaksiReedem.update', $ts->id_transaksi)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i></a>
                          <form action="{{url('transaksiReedem/delete', $ts->id_transaksi)}}" id="delete{{$ts->id_transaksi}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                              @method('delete')
                              <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                              @csrf
                          </form>
                          {{-- <a href="{{route('transaksiReedem.detaildata', $ts->id_transaksi)}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-eye"></i></a> --}}
                      </td>
                      </tr>    
                      @endforeach
                      </tbody>
                    </table>
                  </div>
              </div>
        </div>
      </div>
  </section>
</div>
    
@endsection