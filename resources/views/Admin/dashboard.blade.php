@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Dashboard</h1>
    </div>
    
    <div class="section-body">
      <div class="container-fluid"> 
        <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-warning">
                <i class="far fa-user"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  use Illuminate\Support\Facades\DB;
                  $admin = DB::table('admin')->get();
                  $count = $admin->count();
                ?>
                  <div class="card-header">
                    <h4>Total admin</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>  

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-info">
                <i class="fas fa-user-friends"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $users = DB::table('users')->get();
                  $count = $users->count();
                ?>
                  <div class="card-header">
                    <h4>Total Pengguna</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-success">
                <i class="fas fa-users"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $member = DB::table('member')->get();
                  $count = $member->count();
                ?>
                  <div class="card-header">
                    <h4>Total Member</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-success">
                <i class="fab fa-product-hunt"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $produk = DB::table('produk')->get();
                  $count = $produk->count();
                ?>
                  <div class="card-header">
                    <h4>Total Produk</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-warning">
                <img src="{{url('assets/admin/img/coupon2.svg')}}" style="height: 30px; width:25px;" alt="" class="mb-3 text-center">
              </div>
              <div class="card-wrap">
                <?php 
                  $voucer = DB::table('voucer')->get();
                  $count = $voucer->count();
                ?>
                  <div class="card-header">
                    <h4>Total Voucer</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-info">
                <i class="fas fa-money-check-alt"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $transaksi = DB::table('transaksi')->get();
                  $count = $transaksi->count();
                ?>
                  <div class="card-header">
                    <h4>Total Transaksi Penjualan</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-info">
                <i class="fas fa-coins"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $reedem = DB::table('reedem_point')->get();
                  $count = $reedem->count();
                ?>
                  <div class="card-header">
                    <h4>Total Reedem</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-success">
                <i class="fas fa-money-check-alt"></i>
              </div>
              <div class="card-wrap">
                <?php 
                  $tsr = DB::table('transaksi_reedem')->get();
                  $count = $tsr->count();
                ?>
                  <div class="card-header">
                    <h4>Total Transaksi Reedem</h4>
                  </div>
                  <div class="card-body">
                    {{$count}}
                  </div>
              </div>
            </div>
          </div>

        </div> 
      </div>
    </div>
  </section>
</div> 
@endsection
 