<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{
    public function data()
    {
        $member = DB::table('member')
        ->join('users', 'users.no_telp', '=', 'member.no_telp')
        ->get();

        return view('Admin.member.member', ['member' => $member]);
    }

    //edit data
    public function update($id_member){
        $member = DB::table('member')->where('id_member', $id_member)
        ->join('users', 'users.no_telp', '=', 'member.no_telp')
        ->first();
        return view('Admin.member.memberEdit', ['member'=> $member]);

    }

    //update
    public function updateProcess(Request $request, $id_member)
    {
        $data = DB::table('member')->where('id_member', $id_member)->first();
        DB::table('member')->where('id_member', $id_member)->update([
            'status' => $request->status,
            'point' => $data->point + $request->point,    
        ]);
        
        return redirect('datamember')->with('status', 'Data Berhasil Diedit');

    }

    //detail data
    public function detail($id_member){
        $member = DB::table('member')->where('id_member', $id_member)
        ->join('users', 'users.no_telp', '=', 'member.no_telp')
        ->first();
        return view('Admin.member.detail', ['member'=> $member]);

    }

    //hapus 
    public function delete($id)
    {
        DB::table('member')->where('id_member', $id)->delete();
        return redirect('datamember')->with('status', 'Data Berhasil Dihapus');
    }
}
