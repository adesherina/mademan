@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Produk</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Produk</a></div>
        <div class="breadcrumb-item">Form Tambah Produk</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('produk.simpandata')}}" method="POST" enctype="multipart/form-data">
        @method('post')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Tambah Produk</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                    <label>Brand</label>
                    @php
                    $merk = DB::table('merk')->get();    
                    @endphp
                        <select class="form-control selectric" name="merk">
                          @foreach ($merk as $item)
                          <option value="{{$item->id_merk}}">{{$item->nama_merk}}</option>
                          @endforeach
                        </select>
                </div>
                <div class="form-group">
                  <label>Nama Produk</label>
                  <input type="text" class="form-control" name="nama_produk">
                </div>
                <div class="form-group">
                    <label >Kategori</label>
                    @php
                    $kategori = DB::table('kategori')->get();    
                    @endphp
                        <select class="form-control selectric" name="kategori">
                          @foreach ($kategori as $item)
                            <option value="{{$item->id_kategori}}">{{$item->nama_kategori}}</option>
                          @endforeach
                        </select>
                </div>
                <div class="form-group">
                  <label>Harga </label>
                  <input type="text" class="form-control" name="harga">
                </div>
                <div class="form-group">
                  <label>Stok</label>
                  <input type="text" class="form-control" name="stok">
                </div>
                <div class="form-group"> 
                  <label>Deskripsi</label>
                 <textarea class="form-control" name="deskripsi" id="editor2"></textarea>
                </div>
                <div class="form-group">
                  <label>Foto</label>
                  <input type="file" class="form-control" name="foto">
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection

@push('script')
    <script>
      $(document).ready(function() {
          $('#bidang').select2();
      });

      CKEDITOR.replace( 'deskripsi' );
    </script>
@endpush
 