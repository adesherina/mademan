<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProdukController extends Controller
{
    //tampil data
    public function index(){
        $produk = DB::table('produk')
        ->join('merk', 'merk.id_merk', '=', 'produk.merk')
        ->join('kategori', 'kategori.id_kategori', '=', 'produk.kategori')
        ->get();
        return view('Admin.product.product', ['produk'=> $produk]);
    }

    //tambah data
    public function tambah(){
        return view('Admin.product.productTambah');
    }

    //simpan data
    public function simpan(Request $request){
        if($request->hasFile('foto')){
            $resorce = $request->file('foto');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/foto-produk", $name);
            $save = DB::table('produk')->insert(
                [
                    'merk'=>$request->merk,
                    'kategori'=>$request->kategori,
                    'nama_produk' => $request->nama_produk,
                    'harga' => $request->harga,
                    'stok' => $request->stok,
                    'deskripsi' => $request->deskripsi,
                    'foto' => $name
                ]);
            echo "Foto berhasil di upload";
        }else{
            echo "Gagal upload Foto";
        }
        return redirect('produk')->with('status', 'Data Berhasil Ditambahkan');
    }

    //edit data
    public function edit($id){
        $produk = DB::table('produk')->where('id_produk', $id)
        ->join('merk', 'merk.id_merk', '=', 'produk.merk')
        ->join('kategori', 'kategori.id_kategori', '=', 'produk.kategori')
        ->first();
        return view('Admin.product.productEdit', ['produk'=> $produk]);

    }

    //update
    public function update(Request $request, $id)
    {
        if($request->hasFile('foto')){
            $resorce = $request->file('foto');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/foto-produk", $name);
             
            DB::table('produk')
            ->where('id_produk', $id)
            ->update(
                [
                'merk'=>$request->merk,
                'kategori'=>$request->kategori,
                'nama_produk' => $request->nama_produk,
                'harga' => $request->harga,
                'stok' => $request->stok,
                'deskripsi' => $request->deskripsi,
                'foto' => $name
                ]);
        }else{
            DB::table('produk')
            ->where('id_produk', $id)
            ->update(
                [
                'merk'=>$request->merk,
                'kategori'=>$request->kategori,
                'nama_produk' => $request->nama_produk,
                'harga' => $request->harga,
                'stok' => $request->stok,
                'deskripsi' => $request->deskripsi,
                'foto' => $request->oldPhoto
                ]);
        }
        return redirect('produk')->with('status', 'Data Berhasil Diedit');

    }

    //detail data
    public function detail($id){
        $produk = DB::table('produk')
        ->where('id_produk', $id)
        ->join('merk', 'merk.id_merk', '=', 'produk.merk')
        ->join('kategori', 'kategori.id_kategori', '=', 'produk.kategori')
        ->first();
        return view('Admin.product.detail', ['produk'=> $produk]);

    }

    //hapus data
    public function delete($id)
    {
        DB::table('produk')->where('id_produk', $id)->delete();
        
        return redirect('produk')->with('status', 'Data Berhasil Dihapus');
    }
}
