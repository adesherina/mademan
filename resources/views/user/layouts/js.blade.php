    <script src="{{url('assets/user/js/jquery-3.2.1.min.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
   
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    <!-- ALL PLUGINS -->
    <script src="{{url('assets/user/js/jquery.superslides.min.js')}}"></script>
    <script src="{{url('assets/user/js/bootstrap-select.js')}}"></script>
    <script src="{{url('assets/user/js/inewsticker.js')}}"></script>
    <script src="{{url('assets/user/js/bootsnav.js')}}"></script>
    <script src="{{url('assets/user/js/images-loded.min.js')}}"></script>
    <script src="{{url('assets/user/js/isotope.min.js')}}"></script>
    <script src="{{url('assets/user/js/owl.carousel.min.js')}}"></script>
    <script src="{{url('assets/user/js/baguetteBox.min.js')}}"></script>
    <script src="{{url('assets/user/js/form-validator.min.js')}}"></script>
    <script src="{{url('assets/user/js/contact-form-script.js')}}"></script>
    <script src="{{url('assets/user/js/custom.js')}}"></script>
    <script src="https://use.fontawesome.com/97cea03af9.js"></script>
    <script src="{{url('assets/user/customAjax.js')}}"></script>

    <!-- Check All -->
<script>
    $(document).ready(function () {
        $("#checkAll").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
            calcTotal()
        });
        $('#deleteAll').on('click', function(e) {
            var allVals = [];
            $(".checkedItem:checked").each(function() {
                allVals.push($(this).attr('data-id'));
            });
            if(allVals.length <=0)
            {
                alert("Please select row.");
            }  else { 
                var check = confirm("Are you sure you want to delete this row?");
                if(check == true){
                    var join_selected_values = allVals.join(",");
                    $.ajax({
                        url: $(this).data('url'),
                        type: 'DELETE',
                        headers: {'X-CSRF-TOKEN': $(this).data("csrf")},
                        data: 'id='+join_selected_values,
                        success: function (data) {
                            if (data['success']) {
                                $(".checkedItem:checked").each(function() {  
                                    $(this).parents("tr").remove();
                                });
                                alert(data['success']);
                            } else if (data['error']) {
                                alert(data['error']);
                            } else {
                                alert('Whoops Something went wrong!!');
                            }
                        },
                        error: function (data) {
                            alert(data.responseText);
                        }
                    });
                  $.each(allVals, function( index, value ) {
                      $('table tr').filter("[data-row-id='" + value + "']").remove();
                  });
                }  
            }
        });
    });

    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');    
        jQuery('.quantity').each(function() {
            var spinner = jQuery(this),
                input = spinner.find('input[type="number"]'),
                btnUp = spinner.find('.quantity-up'),
                btnDown = spinner.find('.quantity-down'),
                min = input.attr('min'),
                max = input.attr('max');
                
            btnUp.click(function() {
                var oldValue = parseFloat(input.val());
                
                if (oldValue >= max) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue + 1;
                }
                spinner.find("input").val(newVal);
                
                spinner.find("input").trigger("change");
                
            });

            btnDown.click(function() {
                var oldValue = parseFloat(input.val());
                if (oldValue <= min) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue - 1;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });

        });
</script>

<!-- hitung sub total dan total harga-->
 <script>
      $('.penjualan').each(function() {
            $(this).parent().find('.qty').keyup(function(){
            var value1 = parseInt($(this).val()) ||  0;
        
            var value2 = String($(this).parent().prev().children().find('span.harga').text()) || 0; // ubah ke tring supaya bisa di hitung
            valuefix = parseFloat(value2.replace(".", "").replace(".",""));
            
            // console.log(valuefix*value1);
            //change to format rupiah
            var jumlahsubtotal = valuefix*value1;
                var	number_string = jumlahsubtotal.toString(),
                sisa 	= number_string.length % 3,
                rupiah 	= number_string.substr(0, sisa),
                ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
                    
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
            
            $(this).parent().next().children().find('span.subtotal').text(rupiah);
            let voucherPrice = parseFloat($('#voucher-span').text().replace(".", "").replace(".",""));
            let totalPrice = total-jumlahsubtotal;

                var number_string2 = totalPrice.toString(),
                sisa2 	= number_string2.length % 3,
                rupiah2 	= number_string2.substr(0, sisa2),
                ribuan2 	= number_string2.substr(sisa2).match(/\d{3}/g);
                    
                if (ribuan2) {
                    separator2 = sisa2 ? '.' : '';
                    rupiah2 += separator2 + ribuan2.join('.');
                }
                $('#total').text(rupiah2);
                if($(this).parent().prev().prev().prev().prev().find('.checkedItem').is(':checked') === true)
                {
                calcTotal();   
                }
            
            });
         });

        //subtotal hitung
        $('#subtotal').text('0');
        $(".checkedItem").click(function(event) {
            var total = 0;
            var total_produk = 0;
            $(".checkedItem:checked").each(function() {
                // total += parseInt($(this).parent().next().next().next().next().next().find('.subtotal').text());
                var subtotal = String($(this).parent().next().next().next().next().next().find('.subtotal').text());
                var jumlah_produk = parseInt($(this).parent().next().next().next().next().find('.qty').val());
                var intsubtotal = parseFloat(subtotal.replace(".", "").replace(".",""));
                // console.log(intsubtotal);
                total += intsubtotal;
                total_produk += jumlah_produk;
            });
            
             var number_string2 = total.toString(),
                sisa2 	= number_string2.length % 3,
                rupiah2 	= number_string2.substr(0, sisa2),
                ribuan2 	= number_string2.substr(sisa2).match(/\d{3}/g);
                    
            if (ribuan2) {
                separator2 = sisa2 ? '.' : '';
                rupiah2 += separator2 + ribuan2.join('.');
            }
            
            if (total == 0) {
                   
                // console.log(rupiah2);
                $('#subtotal').text('0');
                $('#total').text('0');
                $('span#jml_produk').text('0');
            } else {
                                
                $('#subtotal').text(rupiah2);
                let voucherPrice = parseFloat($('#voucher-span').text().replace(".", "").replace(".",""));
                let totalPrice = total-voucherPrice;

                var number_string2 = totalPrice.toString(),
                sisa2 	= number_string2.length % 3,
                rupiah2 	= number_string2.substr(0, sisa2),
                ribuan2 	= number_string2.substr(sisa2).match(/\d{3}/g);
                    
                if (ribuan2) {
                    separator2 = sisa2 ? '.' : '';
                    rupiah2 += separator2 + ribuan2.join('.');
                }
                $('#total').text(rupiah2);
                $('span#jml_produk').text(total_produk);
                
            }
        });

         $('#jml_produk').text('0');
         function calcTotal(){
            var sum = "";
            var qty = 0;
            var totalsum = 0;
            var total_produk= 0;
            $('.subtotal').each(function(){
                if($(this).parent().parent().prev().prev().prev().prev().prev().find('.checkedItem').is(':checked') === true){
                    sum = String($(this).text());
                    jumlah_produk = parseInt($(this).parent().parent().prev().find('.qty').val()) || 0;
                    
                    var intsum = parseFloat(sum.replace(".", "").replace(".",""));  
                    totalsum += intsum;
                    total_produk += jumlah_produk;
                }
            });

            
            //change to tupiah
            var number_string3 = totalsum.toString(),
                sisa3 	= number_string3.length % 3,
                rupiah3 	= number_string3.substr(0, sisa3),
                ribuan3 	= number_string3.substr(sisa3).match(/\d{3}/g);
                    
                if (ribuan3) {
                    separator3 = sisa3 ? '.' : '';
                    rupiah3 += separator3 + ribuan3.join('.');
                }
                $('#subtotal').text(rupiah3);
                let voucherPrice = parseFloat($('#voucher-span').text().replace(".", "").replace(".",""));
                let totalPrice = totalsum-voucherPrice;

                var number_string2 = totalPrice.toString(),
                sisa2 	= number_string2.length % 3,
                rupiah2 	= number_string2.substr(0, sisa2),
                ribuan2 	= number_string2.substr(sisa2).match(/\d{3}/g);
                    
                if (ribuan2) {
                    separator2 = sisa2 ? '.' : '';
                    rupiah2 += separator2 + ribuan2.join('.');
                }
                $('#total').text(rupiah2);
                $('span#jml_produk').text(total_produk);
         }
        
         $("#checkout").click(function(event) {
                var harga= [];
                var id=[];
                var qty=[];
                var id_keranjang=[];
                let diskon = $('#voucher-span').text();
                var fixdiskon = parseFloat(diskon.replace(".", "").replace(".",""));

                $(".checkedItem:checked").each(function() {
                    harga.push(parseFloat($(this).parent().find('.harga-produk').val())) || 0;
                    id.push($(this).parent().find('.id-produk').val()) || 0;
                    id_keranjang.push($(this).parent().find('.id_keranjang').val()) || 0;
                    qty.push($(this).parent().next().next().next().next().find('.qty').val())
                    
                });
                var csrf = $('#csrf').val();
                console.log(fixdiskon);

                 $.ajax({
                        url: "checkout/add", //harus sesuai url di buat di route
                        type: "POST",
                        data: {
                            _token: csrf,
                            harga: harga,
                            id:id,
                            qty:qty,
                            id_keranjang:id_keranjang,
                            fixdiskon : fixdiskon
                        },
                        cache: false,
                        success: function (dataResult) {
                            console.log(dataResult);
                            var dataResult = JSON.parse(dataResult);
                            if(dataResult.statusCode==200){
                                swal({
                                    title: "Berhasil",
                                    text: "Produk telah di checkout",
                                    icon: "success",
                                }).then(function (value) {
                                    if (value) {
                                        window.location.href = "checkout/"+ dataResult.nota;
                                    }
                                });	
                            }
                            else if(dataResult.statusCode==201){
                                alert("Error occured !");
                            }
                        },
                        error: function (error) {
                            console.log(error);
                        },
                });    
         });
</script>

