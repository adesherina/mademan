<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{

    public function data()
    {
        $admin = DB::table('admin')->get();

        return view('Admin.admin.admin', ['admins' => $admin]);
    }

    public function add()
    {
        return view('Admin.admin.adminTambah');
    }

    public function addProcess(Request $request)
    {
        $timestamps = true;
        //query builder insert
        DB::table('admin')->insert(
            [
                'nama' => $request->nama,
                'username' => $request->username,
                'password' => Hash::make($request->password),
            ]
        );
        //Redirect dengan status 
        return redirect('dataadmin')->with('status', 'Data Berhasil Ditambahkan');
    }

    //View Update Form
    public function update($id)
    {
        $admin = DB::table('admin')->where('id_admin', $id)->first();
        return view('Admin.admin.adminEdit', ['admins' => $admin]);
    }

    //Update Process
    public function updateProcess(Request $request, $id)
    {
        $newPassword = $request->newPassword;
        if ($newPassword != null) {
            DB::table('admin')->where('id_admin', $id)->update([
                'nama' => $request->nama,
                'username' => $request->username,
                'password' => Hash::make($request->newPassword),
            ]);
        } else {
            DB::table('admin')->where('id_admin', $id)->update([
                'nama' => $request->nama,
                'username' => $request->username,
                'password' => $request->oldPassword
            ]);
        }
        return redirect('dataadmin')->with('status', 'Data Berhasil Diedit');
    }

    //delete data
    public function delete($id)
    {
        DB::table('admin')->where('id_admin', $id)->delete();
        return redirect('dataadmin')->with('status', 'Data Berhasil Dihapus');
    }
}
