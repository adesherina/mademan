<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Model\DataUser;


class UserForgotPW extends Controller
{
    //tampil forgot pw
    public function index(){
        return view('user.forgotpw');
    }

    //forgot pw dengan mencari no_telp
    public function tampil(Request $request)
    {
        $user = DataUser::whereno_telp($request->no_telp)->first();

        if ($user != null){
            return view('user.forgotpw2', ['no_telp' =>$request->no_telp]);
        } else {
            return view('user.forgotpw')->with('failed', 'Nomor Telepon Tidak Terdaftar');
        }

        
    }

    //forgot pw
    public function forgotPw(Request $request)
    {
        $newPassword = $request->newPassword;
        $no_telp = $request->no_telp;
        $confirmPassword = $request->confirmPassword;
        //    dd($newPassword, $confirmPassword);
        if ($newPassword === $confirmPassword){
            $change = DB::table('users')
            ->where('no_telp', $no_telp)
            ->update(['password' => Hash::make($newPassword)]);
            
            return redirect('user/register')->with('success', 'Berhasil Mengganti Password');
        } elseif($newPassword != $confirmPassword){

            return view('user.forgotpw', ['no_telp'=>$no_telp])->with('failedPass', 'Password Konfirmasi Tidak Sama');
        }
    }
}
