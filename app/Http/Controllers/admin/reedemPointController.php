<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class reedemPointController extends Controller
{
    
    //tampil data
    public function index(){
        $reedem = DB::table('reedem_point')->get();
        return view('Admin.reedem.list', ['reedem'=> $reedem]);
    }

    //tambah data
    public function tambah(){
        return view('Admin.reedem.tambah');
    }

    //simpan data
    public function simpan(Request $request){
        if($request->hasFile('foto')){
            $resorce = $request->file('foto');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/foto-reedem", $name);
            $save = DB::table('reedem_point')->insert(
                [
                    'judul'=>$request->judul,
                    'point' => $request->point,
                    'deskripsi' => $request->deskripsi,
                    'foto' => $name
                ]);
            echo "Foto berhasil di upload";
        }else{
            echo "Gagal upload Foto";
        }
        return redirect('reedem')->with('status', 'Data Berhasil Ditambahkan');
    }

    //edit data
    public function edit($id){
        $reedem = DB::table('reedem_point')->where('id_reedem', $id)->first();
        return view('Admin.reedem.edit', ['reedem'=> $reedem]);

    }

    //update
    public function update(Request $request, $id)
    {
        if($request->hasFile('foto')){
            $resorce = $request->file('foto');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/foto-reedem", $name);
            DB::table('reedem_point')
            ->where('id_reedem', $id)
            ->update(
                [
                'judul'=>$request->judul,
                'point' => $request->point,
                'deskripsi' => $request->deskripsi,
                'foto' => $name
                ]);
        }else{
            DB::table('reedem_point')
            ->where('id_reedem', $id)
            ->update(
                [
                'judul'=>$request->judul,
                'point' => $request->point,
                'deskripsi' => $request->deskripsi,
                'foto' => $request->oldPhoto
                ]);
        }
        return redirect('reedem')->with('status', 'Data Berhasil Diedit');

    }

    //detail data
    public function detail($id){
        $reedem = DB::table('reedem_point')->where('id_reedem', $id)->first();
        return view('Admin.reedem.detail', ['reedem'=> $reedem]);

    }

    //hapus data
    public function delete($id)
    {
        DB::table('reedem_point')->where('id_reedem', $id)->delete();
        
        return redirect('reedem')->with('status', 'Data Berhasil Dihapus');
    }

}
