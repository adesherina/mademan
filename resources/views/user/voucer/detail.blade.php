<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    @include('user.layouts.head')
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Detail Voucer</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Voucer</a></li>
                        <li class="breadcrumb-item active">Detail Voucer</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <div class="my-account-box-main">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 col-sm-12 mr-2 mt-3 mx-auto">
                <div class="card detail-voucher-card shadow-sm">
                    <div class="card-header text-center" style="background-color: #ae8547">
                    <h2 style="font-family: 'B612', sans-serif; color:#fff;" class="mt-2 font-weight-bold">{{$voucer->nama_voucer}}</h2>
                    </div>
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-lg-3 col-4">
                                <p class="font-weight-bold">Diskon</p>
                            </div>
                            <div class="col-lg-1 col-1 ">
                                :
                            </div>
                            <div class="col-lg-8 col-6">
                                <p class>{{$voucer->diskon}}%</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-3 col-4">
                                <p class="font-weight-bold">Deskripsi</p>
                            </div>
                            <div class="col-lg-1 col-1 ">
                                :
                            </div>
                            <div class="col-lg-8 col-6">
                                <p class>{{$voucer->deskripsi}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-3 col-4">
                                <p class="font-weight-bold">Kode Voucer</p>
                            </div>
                            <div class="col-lg-1 col-1 ">
                                :
                            </div>
                            <div class="col-lg-8 col-6">
                                <p class>{{$voucer->kode_voucer}}</p>
                            </div>
                        </div>
                        @php
                            setlocale(LC_TIME, 'id_ID');
                            \Carbon\Carbon::setLocale('id');
                           $exp =  \Carbon\Carbon::parse($voucer->expaired_voucer)->toDatetimeString();
                        @endphp
                        <p>Expaired {{Carbon\Carbon::parse($exp)->format('d, M Y')}}</p>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>


    <!-- Start Instagram Feed  -->
    @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
    <script>
        $(document).ready(function(){
        $(".detail-voucher-card").hover(function(){
            $(this).removeClass("shadow-sm");
            $(this).addClass("shadow");
            }, function(){
            $(this).removeClass("shadow");
            $(this).addClass("shadow-sm");
        });
        });
    </script>
</body>

</html>