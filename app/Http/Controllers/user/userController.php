<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class userController extends Controller
{
    public function index(){
        $produk = DB::table('produk')->get();
        
        return view('user.home', compact('produk'));

    }
    
    public function about(){
        return view('user.about');

    }
}
