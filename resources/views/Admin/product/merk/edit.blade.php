@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Brand Produk</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Brand Produk</a></div>
        <div class="breadcrumb-item">Form Update Brand Produk</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('merk.update', $merk->id_merk)}}" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Edit Brand Produk</h4>
                <div class="card-header-action">
                  <a href="{{url('merk')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
                <div class="card-body">
                    <div class="form-group">
                      <label>Brand Produk</label>
                      <input type="text" class="form-control" name="nama_merk" value="{{$merk->nama_merk}}">
                    </div>
                    <div class="card-footer text-right">
                      <button class="btn btn-primary mr-1" type="submit">Submit</button>
                    </div>
                </div>
            </div> 
          </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 