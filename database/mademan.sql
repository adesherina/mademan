-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 17, 2020 at 05:23 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mademan`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 'ade sherina', 'ades', '$2y$10$dIs9DDlLQt94vXJN3ArNPOHovEbE0SHBln4r0RtdGKZGsDG17o85a', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

CREATE TABLE `keranjang` (
  `id_keranjang` bigint(20) UNSIGNED NOT NULL,
  `no_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_produk` bigint(20) UNSIGNED NOT NULL,
  `harga` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `keranjang`
--

INSERT INTO `keranjang` (`id_keranjang`, `no_telp`, `id_produk`, `harga`, `qty`, `subtotal`, `created_at`, `updated_at`) VALUES
(12, '087838384632', 14, 40000, 1, 40000, NULL, NULL),
(13, '087838384632', 14, 40000, 1, 40000, NULL, NULL),
(14, '087838384632', 1, 100000, 0, 0, NULL, NULL),
(31, '082115806156', 2, 100000, 1, 100000, NULL, NULL),
(32, '082115806156', 5, 50000, 3, 150000, NULL, NULL),
(33, '082115806156', 2, 100000, 4, 400000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id_member` bigint(20) UNSIGNED NOT NULL,
  `no_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `point` int(11) NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referal_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `success_invite` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `no_telp`, `point`, `status`, `referal_code`, `success_invite`, `created_at`, `updated_at`) VALUES
(1, '082115806156', 7, 'Aktif', '1640', 1, NULL, NULL),
(2, '087838384632', 1, 'Aktif', '5263', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(20, '2020_12_08_035917_create_users', 2),
(21, '2020_12_08_053303_create_admin', 0),
(23, '2020_12_11_030110_create_produk', 1),
(24, '2020_12_08_053325_create_member', 3),
(25, '2020_12_15_061122_create_keranjang', 4),
(26, '2020_12_16_045811_create_voucer', 5);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` bigint(20) UNSIGNED NOT NULL,
  `nama_produk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_produk`, `merk`, `harga`, `stok`, `deskripsi`, `foto`, `created_at`, `updated_at`) VALUES
(1, 'CAPTAIN Clay Pomade', 'CAPTAIN', 100000, 2, 'Pomade terbaik bagi Anda yang ingin rambutnya terlihat lebih ber-volume dan tampak mengembang. Berfungsi baik dalam menjaga bentuk rambut agar bertahan lama seharian. Menghasilkan kilau yang natural pada rambut. Mudah dibilas dan cocok untuk segala jenis rambut.', 'CAPTAIN Kids Clay Pomade.jpeg', NULL, NULL),
(2, 'CAPTAIN Black Pomade', 'CAPTAIN', 100000, 5, 'Pomade yang dapat membuat warna rambut tampak hitam seketika dan berkilau alami. Mengandung Argan & Jojoba Oil untuk membantu mengurangi kerontokan dan membantu merangsang pertumbuhan rambut hitam. Cocok untuk segala jenis rambut.', 'CAPTAIN Black Pomade.jpeg', NULL, NULL),
(3, 'CAPTAIN Water Based Pomade', 'CAPTAIN', 90000, 5, 'CAPTAIN Water Based Pomade\r\nPomade terbaik bagi Anda yang ingin memiliki fleksibilitas dalam mengatur gaya rambut sepanjang hari. Menghasilkan kilau yang tinggi pada rambut. Mudah dibilas dan cocok untuk segala jenis rambut.', 'CAPTAIN Water Based Pomade.jpeg', NULL, NULL),
(4, 'CAPTAIN Kids Clay Pomade', 'CAPTAIN', 90000, 5, 'CAPTAIN Kids Clay Pomade\r\nPomade terbaik untuk si kecil yang ingin tampil dengan rambut stylish. Dibuat dengan formula yang lembut dan tanpa pewarna buatan sehingga aman bagi rambut anak-anak. Menghasilkan kilau rambut yang natural. Mudah dibilas dan cocok untuk segala jenis rambut.', 'CAPTAIN Kids Clay Pomade.jpeg', NULL, NULL),
(5, 'Pomade Chief Red Oil Based Travel Pack Original 35Gr', 'Pomade Chief', 50000, 5, 'Travel pack size CHIEF OIL BASED cocok untuk bepergian dan travelling\r\nKunci Gaya Rambutmu Dengan Product Original CHIEF !', '2373806293ce8ead93a383e9d2bec674.jpeg', NULL, NULL),
(6, 'Pomade Chief Space Clay Original 120gr', 'Pomade Chief', 131000, 2, 'CHIEF POMADE SPACE CLAY BARBERNAUT\r\nWater Based Clay\r\nNetto : 120 gr\r\nGlass Jar Ekslusif', 'Pomade Chief Space Clay Original 120gr.jpg', NULL, NULL),
(7, 'Pomade Chief Water Based Original 120gr', 'Pomade Chief', 132000, 4, 'Aktifitas: INDOOR (Daya tahan berkurang jika terkena sinar matahari & keringat)\r\nTampilan akhir:  Strong Hold, Wet, Slick & Shine (Klimis & Basah)', 'Pomade Chief Water Based Original 120gr.jpeg', NULL, NULL),
(8, 'Pomade Chief Hybrid Travel Pack (Unorthodox) Original 28,5Gr', 'Pomade Chief', 75000, 3, 'Aktifitas: INDOOR & OUTDOOR\r\nTampilan akhir: Highest Shine Finish, Firm Hold, Natural Dry (jika digunakan pada rambut kering); Natural Slick Wet (jika digunakan pada rambut basah)', 'Pomade Chief Hybrid Travel Pack (Unorthodox) Original 28,5Gr.jpeg', NULL, NULL),
(9, 'Pomade Chief Junior Glass Jar Original 120gr', 'Pomade Chief', 120000, 4, 'Water Based Pomade\r\nCocok untuk Anak usia 3-10 tahun (Mild Hold)', 'Pomade Chief Junior Glass Jar Original 120gr.jpeg', NULL, NULL),
(10, 'Pomade Chief Junior Travel Pack 35Gr Glass jar Original', 'Pomade Chief', 75000, 2, 'Water Based Pomade\r\nCocok untuk Anak usia 3-10 tahun (Mild Hold)\r\nParfum: Arizona Honeydew (Melon Segar)', 'Pomade Chief Junior Travel Pack 35Gr Glass jar Original.jpg', NULL, NULL),
(11, 'BAD LAB Supersonik Water-Based Pomade Medium & Glossy 80g', 'BAD LAB', 99000, 6, 'Pomade berbahan dasar air, ditambahkan dengan Purifying FluidiPure™ 8G untuk membantu mengurangi iritasi dan menjaga wangi kulit kepala Anda. Dengan hasil akhir yang licin, sangat mengkilap untuk mengunci gaya rambut quaff, pompadour atau ducktail. So just slick back and relax!', 'BAD LAB Supersonik Water-Based Pomade Medium & Glossy 80g.png', NULL, NULL),
(12, 'BAD LAB Matte Max Strong & Matte Water-Based Pomade 40g', 'BAD LAB', 75000, 7, 'Pomade berbahan dasar air-namun tetap kuat dengan Purifying FluidiPureTM 8G dan charcoal untuk membantu Anda melawan bau tak sedap dan iritasi pada kulit kepala. Kencan, olahraga di gym, atau rencana mendominasi dunia, rambut Anda tidak akan bergerak kecuali jika Anda menyuruhnya melakukannya.\r\nOleskan secara merata pada rambut yang kering, setengah kering atau lembab. Bentuk rambut seperti yang diinginkan. Ideal untuk semua jenis rambut. Bersihkan dengan cuci rambut biasa.', 'BAD LAB Matte Max Strong & Matte Water-Based Pomade 40g.jpeg', NULL, NULL),
(13, 'BAD LAB Like A Boss Sculpting Hair Clay 80g', 'BAD LAB', 98000, 4, 'Daya tahan yang kuat dengan formula yang ringan. Menjinakkan dan membentuk rambut Anda dengan baik.  Mengandung Purifying Fluidipure ™ 8G untuk membantu menjaga kulit kepala dari iritasi dan tetap wangi. Taklukan hari, jalani peran, dan kuasai hutanmu.', 'BAD LAB Like A Boss Sculpting Hair Clay 80g.jpeg', NULL, NULL),
(14, 'Bellagio Homme High Shine & Strong Hold Pomade 80gr', 'Bellagio', 40000, 10, 'Dari produsen parfum BELLAGIO yang aromanya sudah tidak diragukan lagi, kini hadir BELLAGIO Pomade water-based untuk gaya rambut yang trendy, edgy dan mengkilap.\r\nBellagio Homme High Shine & Strong Hold Hair Pomade mengandung bahan-bahan yang yang berkualitas tinggi untuk menghasilkan rambut yang mengkilap dan terbentuk sepanjang hari.', 'Bellagio Homme High Shine & Strong Hold Pomade 80gr.jpeg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `no_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_lengkap` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`no_telp`, `code`, `nama_lengkap`, `email`, `alamat`, `jenis_kelamin`, `photo`, `password`, `created_at`, `updated_at`) VALUES
('082115806156', NULL, 'ade sherina', 'adesherin00@gmail.com', 'Indramayu', 'Perempuan', '49583.jpg', '$2y$10$IGoqNffbP9sT7VQ6owG7aOzSIZkvMhZVEgx1asgEK6Lx9dnFvifUa', '2020-12-14 23:35:20', '2020-12-14 23:35:20'),
('087838384632', NULL, 'Yuni', 'yuni@gmail.com', 'Indramayu', NULL, NULL, '$2y$10$RFjx9l04NtrlynVb3D5QbOXrPU5VHwKoDm/V47aetWbza8x2V8b6W', '2020-12-15 02:52:58', '2020-12-15 02:52:58');

-- --------------------------------------------------------

--
-- Table structure for table `voucer`
--

CREATE TABLE `voucer` (
  `id_voucer` bigint(20) UNSIGNED NOT NULL,
  `nama_voucer` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_voucer` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expaired_voucer` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `voucer`
--

INSERT INTO `voucer` (`id_voucer`, `nama_voucer`, `kode_voucer`, `deskripsi`, `expaired_voucer`, `created_at`, `updated_at`) VALUES
(1, 'Pengguna Baru', 'PB123', 'Voucer ini hanya bisa digunakan untuk pengguna baru saja', '2020-12-19', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD PRIMARY KEY (`id_keranjang`),
  ADD KEY `keranjang_no_telp_foreign` (`no_telp`),
  ADD KEY `keranjang_id_produk_foreign` (`id_produk`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_member`),
  ADD KEY `member_no_telp_foreign` (`no_telp`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`no_telp`);

--
-- Indexes for table `voucer`
--
ALTER TABLE `voucer`
  ADD PRIMARY KEY (`id_voucer`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `keranjang`
--
ALTER TABLE `keranjang`
  MODIFY `id_keranjang` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id_member` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `voucer`
--
ALTER TABLE `voucer`
  MODIFY `id_voucer` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD CONSTRAINT `keranjang_id_produk_foreign` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `keranjang_no_telp_foreign` FOREIGN KEY (`no_telp`) REFERENCES `users` (`no_telp`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `member`
--
ALTER TABLE `member`
  ADD CONSTRAINT `member_no_telp_foreign` FOREIGN KEY (`no_telp`) REFERENCES `users` (`no_telp`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
