<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    public $table = 'transaksi';
    public $fillable = ['nota','no_telp','jumlah','total','status_pesanan','diskon'];
}
