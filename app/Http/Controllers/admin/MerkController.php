<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MerkController extends Controller
{
    //tampil data
    public function index(){
        $merk = DB::table('merk')->get();
        return view('Admin.product.merk.list', ['merk'=> $merk]);
    }

    //tambah data
    public function tambah(){
        return view('Admin.product.merk.tambah');
    }

    //simpan data
    public function simpan(Request $request){
        $save = DB::table('merk')->insert(
           [
                'nama_merk'=>$request->nama_merk,
           ]);
        return redirect('merk')->with('status', 'Data Berhasil Ditambahkan');
    }

    //edit data
    public function edit($id){
        $merk = DB::table('merk')->where('id_merk', $id)->first();
        return view('Admin.product.merk.edit', ['merk'=> $merk]);

    }

    //update
    public function update(Request $request, $id)
    {
        DB::table('merk')
        ->where('id_merk', $id)->update([
                'nama_merk'=>$request->nama_merk,
                ]);
        return redirect('merk')->with('status', 'Data Berhasil Diedit');

    }

    //hapus data
    public function delete($id)
    {
        DB::table('merk')->where('id_merk', $id)->delete();
        
        return redirect('merk')->with('status', 'Data Berhasil Dihapus');
    }

}
