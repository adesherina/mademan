@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Admin</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Admin</a></div>
        <div class="breadcrumb-item">Form Update Admin</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('admin.updateProcess', $admins->id_admin)}}" method="POST">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Update Admin</h4>
                <div class="card-header-action">
                  <a href="{{url('dataadmin')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" class="form-control" name="nama" value="{{$admins->nama}}">
                </div>
                <div class="form-group"> 
                  <label>Username</label>
                  <input type="text" class="form-control" name="username" value="{{$admins->username}}">
                </div>
                <div class="form-group"> 
                  <label>Password</label>
                  <input type="hidden" class="form-control" name="oldPassword" value="{{$admins->password}}">
                  <input type="text" class="form-control" name="newPassword" placeholder="Jika ingin update...">
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Update</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 