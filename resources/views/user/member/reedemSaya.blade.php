
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    @include('user.layouts.head')
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Reedem Saya</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Reedem</a></li>
                        <li class="breadcrumb-item active">Riwayat Reedem</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <div class="products-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Reedem Saya</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet lacus enim.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="special-menu text-center">
                        <div class="button-group filter-button-group">
                            <button class="active mb-3" data-filter="*">Semua</button>
                            <button data-filter=".menunggu-konfirmasi">Menunggu Konfirmasi</button>
                            <button data-filter=".selesai">Selesai</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row special-list">
                @foreach($reedem as $reedem)
                <div class="col-lg-6 col-md-6 special-grid @if($reedem->status === 'Belum Dikonfirmasi') menunggu-konfirmasi @elseif($reedem->status === 'Selesai') selesai @endif">
                    <div class="row">
                        <div class="col-12">
                            <div class="card mt-5" style="min-height: 400px;">
                                <div class="card-header" style="background-color: #ae8547;">
                                   <h4 class="text-center mt-4 font-weight-bold" style="font-size: 1.2em;font-family: 'B612', sans-serif; color:#ffff;">Kode Reedem : {{$reedem->id_transaksi}}</h4>
                                </div>
                                <div class="card-body" >
                                    <div class="content" >
                                        <div class="row mb-3">
                                            <div class="col-4">
                                                <p>Produk</p>
                                            </div>
                                            <div class="col-1">
                                            :
                                            </div>
                                            <div class="col-6">
                                            <p class>{{$reedem->judul}}</p>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-4">
                                                <p>Point</p>
                                            </div>
                                            <div class="col-1">
                                            :
                                            </div>
                                            <div class="col-6">
                                            <p class>{{$reedem->point}}</p>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-4">
                                                <p>Status Reedem</p>
                                            </div>
                                            <div class="col-1">
                                            :
                                            </div>
                                            <div class="col-6">
                                                @if($reedem->status == "Belum Dikonfirmasi")
                                                <div class="badge badge-pill  badge-warning">{{$reedem->status}}</div>
                                                @elseif($reedem->status == "Selesai")
                                                <div class="badge badge-pill  badge-success">{{$reedem->status}}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                        @if($reedem->status == "Belum Dikonfirmasi")
                                            <div class="submit-button text-right">
                                                <a href="{{route('reedem.kode', $reedem->id_transaksi)}}" class="btn hvr-hover text-white">Cek Kode Reedem</a>
                                            </div>
                                        @elseif($reedem->status == "Selesai")
                                        <div class="submit-button text-right">
                                            <a href="{{url('product')}}" class="btn hvr-hover text-white">Beli Lagi</a>
                                        </div>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- End Products  -->


    <!-- Start Instagram Feed  -->
    @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
</body>

</html>