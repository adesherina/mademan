<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Model\DataAdmin;

class forgotPassword extends Controller
{
    //tampil nomor telpon
    public function index(){
        return view('Admin.forgotPassword.noTelp');
    }

    //forgot pw dengan mencari no telp
    public function tampil(Request $request)
    {
        $admin = DataAdmin::whereno_telp($request->no_telp)->first();

        if ($admin != null){
            return view('Admin.forgotPassword.forgot', ['no_telp' =>$request->no_telp]);
        } else {
            return view('Admin.forgotPassword.noTelp')->with('failed', 'Nomor Telepon Tidak Terdaftar');
        }
    }

    //forgot pw
    public function forgotPw(Request $request)
    {
        $newPassword = $request->newPassword;
        $no_telp = $request->no_telp;
        $confirmPassword = $request->confirmPassword;
        //    dd($newPassword, $confirmPassword);
        if ($newPassword === $confirmPassword){
            $change = DB::table('admin')
            ->where('no_telp', $no_telp)
            ->update(['password' => Hash::make($newPassword)]);
            
            return redirect('/login')->with('success', 'Berhasil Mengganti Password');
        } elseif($newPassword != $confirmPassword){

            return view('Admin.forgotPassword.forgot', ['no_telp'=>$no_telp])->with('failedPass', 'Password Konfirmasi Tidak Sama');
        }
    }
}
