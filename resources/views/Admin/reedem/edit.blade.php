@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Reedem Point</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Reedem Point</a></div>
        <div class="breadcrumb-item">Form Update Reedem Point</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('reedem.update', $reedem->id_reedem)}}" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Edit Reedem Point</h4>
                <div class="card-header-action">
                  <a href="{{url('reedem')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
                <div class="card-body">
                    <div class="form-group">
                      <label>Nama Produk</label>
                      <input type="text" class="form-control" name="judul" value="{{$reedem->judul}}">
                    </div>
                    <div class="form-group">
                      <label>Point</label>
                      <input type="text" class="form-control" name="point" value="{{$reedem->point}}">
                    </div>
                    <div class="form-group"> 
                      <label>Deskripsi</label>
                    <textarea class="form-control" name="deskripsi">{{$reedem->deskripsi}}</textarea>
                    </div>
                    <div class="form-group">
                      <label>Foto</label>
                      <input type="file" class="form-control" name="foto" >
                      <input type="hidden" name="oldPhoto" value="{{$reedem->foto}}">
                    </div>
                    <div class="card-footer text-right">
                      <button class="btn btn-primary mr-1" type="submit">Submit</button>
                    </div>
                </div>
            </div> 
          </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 
@push('script')
    <script>
      $(document).ready(function() {
          $('#bidang').select2();
      });

      CKEDITOR.replace( 'deskripsi' );
    </script>
@endpush