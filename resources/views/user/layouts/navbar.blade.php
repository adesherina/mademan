        <!-- Start Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light navbar-default bootsnav" style="background-color: #212832;">
            <div class="container">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                    <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('assets/user/images/mademan.png')}}" style="width: 120px; height:40px"  class="logo" alt=""></a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                        <li class="nav-item"><a class="nav-link text-white" href="{{url('/')}}">Home</a></li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{url('about')}}">About Us</a></li>
                        <li class="nav-item">
                            <a href="{{url('product')}}" class="nav-link text-white">Product</a>
                        </li>
                        
                        @if(session('berhasil_login'))
                        <li class="dropdown" >
                            <a href="#" class="nav-link dropdown-toggle arrow text-white" data-toggle="dropdown">My Account</a>
                            <ul class="dropdown-menu" style="background-color:#212832; ">
                                <a class="dropdown-item text-white" style="background-color: #212832" href="{{route('user.update',Session::get('no_telp'))}}">Account</a>
                                <a class="dropdown-item text-white" style="background-color: #212832" href="{{route('user.update.pw',Session::get('no_telp'))}}">Ganti Password</a>
                                <a class="dropdown-item text-white" style="background-color: #212832" href="{{url('pesanan')}}">Pesanan Saya</a>
                                <a class="dropdown-item text-white" style="background-color: #212832" href="{{url('favourite')}}">Produk Favorite Saya</a>
                                <a class="dropdown-item text-white" style="background-color: #212832" href="{{url('user/logout')}}">Logout</a>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{url('user/voucer')}}">Voucer</a>
                        </li>
                        
                        <li class="dropdown">
                            <a href="#" class="nav-link dropdown-toggle arrow text-white" data-toggle="dropdown">Reedem</a>
                            <ul class="dropdown-menu" style="background-color:#212832; ">
                                 <a href="{{url('reedemPoint')}}" style="background-color: #212832" class="dropdown-item text-white">Reedem Point</a>
                                 <a href="{{url('reedemSaya')}}" style="background-color: #212832" class="dropdown-item text-white">Reedem Saya</a>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="{{route('status.member')}}" class="nav-link">
						        <i class="fa fa-credit-card-alt text-white"></i>
					        </a>
                        </li>
                        <li class="nav-item ml">
                            <a href="{{url('keranjang')}}" class="nav-link">
                                <i class="fa fa-shopping-cart text-white"></i>
                            </a>
                        </li>
                        @else
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{url('user/register')}}">Account</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{url('user/voucer')}}">Voucer</a>
                        </li>
                       
                        @endif
                         
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
                
            <!-- End Side Menu -->
        </nav>
        <!-- End Navigation -->