@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Reedem Point</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Reedem Point</a></div>
        <div class="breadcrumb-item">Form Tambah Reedem Point</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('reedem.simpandata')}}" method="POST" enctype="multipart/form-data">
        @method('post')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Tambah Reedem Point</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Produk</label>
                  <input type="text" class="form-control" name="judul">
                </div>
                <div class="form-group">
                  <label>Point</label>
                  <input type="text" class="form-control" name="point">
                </div>
                <div class="form-group"> 
                  <label>Deskripsi</label>
                 <textarea class="form-control" name="deskripsi" id="editor2"></textarea>
                </div>
                <div class="form-group">
                  <label>Foto</label>
                  <input type="file" class="form-control" name="foto">
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection

@push('script')
    <script>
      $(document).ready(function() {
          $('#bidang').select2();
      });

      CKEDITOR.replace( 'deskripsi' );
    </script>
@endpush
 