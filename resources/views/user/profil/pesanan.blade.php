
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    @include('user.layouts.head')
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>My Account</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <li class="breadcrumb-item active">Riwayat Pesanan</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <div class="products-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Pesanan Saya</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet lacus enim.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="special-menu text-center">
                        <div class="button-group filter-button-group">
                            <button class="active mb-3" data-filter="*">Semua</button>
                            <button data-filter=".belum-bayar">Belum Bayar</button>
                            <button data-filter=".dikemas">Dikemas</button>
                            <button data-filter=".dikirim">Dikirim</button>
                            <button data-filter=".selesai">Selesai</button>
                            <button data-filter=".dibatalkan">Dibatalkan</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row special-list">
                @foreach($pesanan as $pesanan)
                <div class="col-lg-6 col-md-6 special-grid @if($pesanan->status_pesanan === 'Dikemas') dikemas  @elseif($pesanan->status_pesanan === 'unpaid') belum-bayar @elseif($pesanan->status_pesanan === 'Dikirim') dikirim @elseif($pesanan->status_pesanan === 'Selesai') selesai @elseif($pesanan->status_pesanan === 'Dibatalkan') dibatalkan @endif">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-voucher mt-5" style="min-height: 360px;">
                                <div class="card-header" style="background-color: #ae8547;">
                                   <h4 class="text-center mt-4 font-weight-bold" style="font-size: 1.2em;font-family: 'B612', sans-serif; color:#ffff;">Nota : {{$pesanan->nota}}</h4>
                                </div>
                                <div class="card-body" style="min-height: 300px;">
                                    <div class="content" >
                                        <?php
                                            $produk = DB::table('detail_transaksi')
                                                ->where('nota',$pesanan->nota)
                                                ->join('produk', 'produk.id_produk', '=', 'detail_transaksi.id_produk','left')
                                                ->first();
                                        ?>
                                        <div class="row mb-3">
                                            <div class="col-lg-3 col-4">
                                                <p class="font-weight-bold">Produk</p>
                                            </div>
                                            <div class="col-lg-1 col-1 ">
                                            :
                                            </div>
                                            <div class="col-lg-8 col-6">
                                            <p class>{{$produk->nama_produk}}</p>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-lg-3 col-4">
                                                <p class="font-weight-bold">QTY</p>
                                            </div>
                                            <div class="col-lg-1 col-1">
                                            :
                                            </div>
                                            <div class="col-lg-8 col-6">
                                            <p class>{{$pesanan->jumlah}}</p>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-lg-3 col-4">
                                                <p class="font-weight-bold">Total Bayar</p>
                                            </div>
                                            <div class="col-lg-1 col-1">
                                            :
                                            </div>
                                            <div class="col-lg-8 col-6">
                                            <p class>Rp.  {{number_format($pesanan->total,'0','.','.')}}</p>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-lg-3 col-4">
                                                <p class="font-weight-bold">Status </p>
                                            </div>
                                            <div class="col-lg-1 col-1">
                                            :
                                            </div>
                                            <div class="col-lg-8 col-5">
                                                @if($pesanan->status_pesanan == "unpaid")
                                                    <div class="badge badge-pill  badge-warning">{{$pesanan->status_pesanan}}</div>
                                                @elseif($pesanan->status_pesanan == "Menunggu Verifikasi")
                                                    <div class="badge badge-pill  badge-success">{{$pesanan->status_pesanan}}</div>
                                                @elseif($pesanan->status_pesanan == "Dikemas")
                                                    <div class="badge badge-pill  badge-info">{{$pesanan->status_pesanan}}</div>
                                                @elseif($pesanan->status_pesanan == "Dikirim")
                                                    <div class="badge badge-pill  badge-primary">{{$pesanan->status_pesanan}}</div>
                                                @elseif($pesanan->status_pesanan == "Selesai")
                                                    <div class="badge badge-pill  badge-success">{{$pesanan->status_pesanan}}</div>
                                                @elseif($pesanan->status_pesanan == "Dibatalkan")
                                                    <div class="badge badge-pill  badge-danger">{{$pesanan->status_pesanan}}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                        @if($pesanan->status_pesanan == "unpaid")
                                            @if($pesanan->jasa_kirim != null)
                                                <div class="submit-button text-right mt-2">
                                                    <a href="{{route('checkout.bayar.tf', $pesanan->nota)}}" class="btn hvr-hover text-white">Bayar Sekarang</a>
                                                </div>
                                            @elseif($pesanan->jasa_kirim == null)
                                                <div class="submit-button text-right mt-2">
                                                    <a href="{{route('checkout.bayar.kasir',$pesanan->nota)}}" class="btn hvr-hover text-white">Bayar Sekarang</a>
                                                </div>
                                            @endif
                                        @elseif($pesanan->status_pesanan == "Menunggu Verifikasi")
                                        <div class="submit-button text-right mt-2">
                                            <a href="{{route('detail', $pesanan->nota)}}" class="btn hvr-hover text-white">Lihat Detail Pesanan</a>
                                        </div>
                                        @elseif($pesanan->status_pesanan == "Dikemas")
                                        <div class="submit-button text-right mt-2">
                                            <a href="{{route('detail', $pesanan->nota)}}" class="btn hvr-hover text-white">Lihat Detail Pesanan</a>
                                        </div>
                                        @elseif($pesanan->status_pesanan == "Dikirim")
                                        <div class="submit-button text-right mt-2">
                                            <a href="{{route('resi', $pesanan->nota)}}" class="btn hvr-hover text-white">Cek Resi</a>
                                        </div>
                                        @elseif($pesanan->status_pesanan == "Selesai")
                                        <div class="submit-button text-right mt-2">
                                            <a href="{{url('product')}}" class="btn hvr-hover text-white">Beli Lagi</a>
                                        </div>
                                        @elseif($pesanan->status_pesanan == "Dibatalkan")
                                        <div class="submit-button text-right mt-2">
                                            <a href="{{url('product')}}" class="btn hvr-hover text-white">Beli Lagi</a>
                                        </div>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- End Products  -->


    <!-- Start Instagram Feed  -->
    @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
</body>

</html>