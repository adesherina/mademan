<!-- Start Instagram Feed  -->
    <div class="instagram-box">
        @php
            $produk2 = DB::table('produk')->get();
        @endphp
        <div class="main-instagram owl-carousel owl-theme">
            @foreach($produk2 as $produk2)
            <div class="item">
                <div class="ins-inner-box">
                    <img src="{{url('/foto-produk/'.$produk2->foto)}}" alt="" />
                    <div class="hov-in">
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
<!-- End Instagram Feed  -->
