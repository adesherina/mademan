<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DataUser extends Model
{
        protected $table = "users";

    public $timestamp = true;

    protected $guarded = ['update_at'];
}
