@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
   <section class="section">
      <div class="section-header">
        <h1>Kategori Produk</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Kategori Produk</a></div>
          <div class="breadcrumb-item">Kategori Produk</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Kategori Produk</h4>
            <div class="card-header-action">
              <a href="{{route('kategori.tambahdata')}}" class="btn btn-success btn-lg">
              Add
              </a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="tabel-data" class="table table-bordered table-md">
                <thead>
                  <tr>
                    <th class="text-center">
                       No
                    </th>
                    <th>Kategori Produk</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach ($kategori as $kategori)
                 <tr>
                   <td class="text-center">
                     {{ $loop->iteration }}
                    </td>
                    <td>{{$kategori->nama_kategori}}</td>
                    <td>
                      <a href="{{route('kategori.editdata', $kategori->id_kategori)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                      </a>
                      <form action="{{route('kategori.deletedata', $kategori->id_kategori)}}" id="delete{{$kategori->id_kategori}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                      @method('delete')
                      <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                      @csrf
                      </form>
                    </td>
                 </tr>
                @endforeach
                </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</section>
</div> 
@endsection