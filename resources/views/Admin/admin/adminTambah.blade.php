@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Admin</h1>
      
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Admin</a></div>
        <div class="breadcrumb-item">Tambah Admin</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('admin.simpandata')}}" method="POST">
        @method('post')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Tambah Admin</h4>
                <div class="card-header-action">
                    <a href="{{url('dataadmin')}}" class="btn btn-success btn-lg">
                      kembali
                    </a>
                  </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" class="form-control" name="nama">
                </div>
                <div class="form-group"> 
                  <label>Username</label>
                 <input type="text" class="form-control" name="username">
                </div>
                <div class="form-group"> 
                    <label>Password</label>
                   <input type="text" class="form-control" name="password">
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 