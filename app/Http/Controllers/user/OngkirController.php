<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OngkirController extends Controller
{
    public function get_Kota(){
        $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "key: 2b9e297b4630a6a7da27b23a044febef"
                    ),
                ));
                $response = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);
                $data = json_decode($response, true);
                for ($i = 0; $i < count($data['rajaongkir']['results']); $i++) {
                    echo "<option></option>";
                    echo "<option value='" . $data['rajaongkir']['results'][$i]['city_id'] . "'>" . $data['rajaongkir']['results'][$i]['city_name'] ." </option>";
                }
    }

    public function cekOngkir(Request $request){
        $kota_tujuan = $request->kota_tujuan;
        $kurir = $request->kurir;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "origin=" . 398 . "&destination=" . $kota_tujuan . "&weight=1000&courier=" . $kurir. "",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: 2b9e297b4630a6a7da27b23a044febef"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $data = json_decode($response, true);

         $harga = $data['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value'];
         $estimasi = $data['rajaongkir']['results'][0]['costs'][0]['cost'][0]['etd'];
         $hargaRupiah =  number_format($harga, 0, '.','.');      

         return json_encode(array(
                    "statusCode"=>200,
                    "harga"=>$hargaRupiah,
                    'estimasi'=> $estimasi
            ));
    }
}
