<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    @include('user.layouts.head')
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Verifikasi OTP</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Account</a></li>
                        <li class="breadcrumb-item active">Verifikasi Otp</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <div class="cart-box-main">
        <div class="container">
            <div class="row new-account-login">
                <div class="col-sm-12 col-lg-12 mb-12">
                    <div class="title-left">
                         @if (session('uploadSuccess'))
                            <div class="alert alert-success">
                                {{ session('uploadSuccess') }}
                            </div>
                        @endif
                    </div>
                    <h5><a data-toggle="collapse" href="#formLogin" role="button" aria-expanded="false">Verifikasi OTP</a></h5>
                    <form class="mt-3 collapse review-form-box" id="formLogin" action="{{route('verify')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('post')
                            <div class="form-group col-md-6">
                                <label for="InputEmail" class="mb-0">Verifikasi OTP</label>
                                <input type="hidden" class="form-control" value="{{$no_telp}}" name="no_telp">
                                <input type="hidden" class="form-control" value="{{$kode_referal}}" name="kode_referal">
                                <input type="text" class="form-control" name="code"> 
                            </div>
                        <button type="submit" class="btn hvr-hover">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- Start Instagram Feed  -->
    @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
</body>

</html>