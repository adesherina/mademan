@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Voucer</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Voucer</a></div>
        <div class="breadcrumb-item">Form Update Voucer</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('voucer.update', $voucer->id_voucer)}}" method="POST">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Edit Voucer</h4>
                <div class="card-header-action">
                  <a href="{{url('voucer')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
                <div class="card-body">
                    <div class="form-group">
                      <label>Nama Voucer</label>
                      <input type="text" class="form-control" name="nama_voucer" value="{{$voucer->nama_voucer}}">
                    </div>
                    <div class="form-group">
                      <label>Kode Voucer</label>
                      <input type="text" class="form-control" name="kode_voucer" value="{{$voucer->kode_voucer}}">
                    </div>
                    <div class="form-group">
                      <label>Kode Voucer</label>
                      <input type="text" class="form-control" name="diskon" value="{{$voucer->diskon}}">
                    </div>
                    <div class="form-group">
                      <label>Expaired Voucer</label>
                      <input type="date" class="form-control" name="expaired_voucer" value="{{$voucer->expaired_voucer}}">
                    </div>
                    <div class="form-group"> 
                      <label>Deskripsi</label>
                    <textarea class="form-control" name="deskripsi">{{$voucer->deskripsi}}</textarea>
                    </div>
                    <div class="card-footer text-right">
                      <button class="btn btn-primary mr-1" type="submit">Submit</button>
                    </div>
                </div>
            </div> 
          </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 
@push('script')
    <script>
      $(document).ready(function() {
          $('#bidang').select2();
      });

      CKEDITOR.replace( 'deskripsi' );
    </script>
@endpush