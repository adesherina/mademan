@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
   <section class="section">
      <div class="section-header">
        <h1>Voucer</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Data Voucer</a></div>
          <div class="breadcrumb-item">Voucer</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Data Voucer</h4>
            <div class="card-header-action">
              <a href="{{route('voucer.tambahdata')}}" class="btn btn-success btn-lg">
              Add
              </a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="tabel-data" class="table table-bordered table-md">
                <thead>
                  <tr>
                    <th class="text-center">
                       No
                    </th>
                    <th>Nama Voucer</th>
                    <th>Kode Voucer</th>
                    <th>Diskon</th>
                    <th>Expaired Vocer</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach ($voucer as $voucer)
                 <tr>
                   <td class="text-center">
                     {{ $loop->iteration }}
                    </td>
                    <td>{{$voucer->nama_voucer}}</td>
                    <td>{{$voucer->kode_voucer}}</td>
                    <td>{{$voucer->diskon}}</td>
                    <td>{{$voucer->expaired_voucer}}</td>
                    <td>
                      <a href="{{route('voucer.editdata', $voucer->id_voucer)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                      </a>
                      <form action="{{route('voucer.deletedata', $voucer->id_voucer)}}" id="delete{{$voucer->id_voucer}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                      @method('delete')
                      <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                      @csrf
                      </form>
                      <a href="{{route('voucer.detaildata', $voucer->id_voucer)}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-eye"></i></a>
                    </td>
                 </tr>
                @endforeach
                </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</section>
</div> 
@endsection