<!DOCTYPE html>
<html lang="en">
<head>
@include('Admin.layouts.header')
</head>

<body>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="login-brand">
              Login Admin
              <!-- <img src="{{url('assets/user/img/stisla-fill.svg')}}" alt="logo" width="100" class="shadow-light rounded-circle"> -->
            </div>
             <!-- Alert  -->
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <div class="card card-primary">
              <div class="card-header"><h4>Login</h4></div>

              <div class="card-body">
                <form method="POST" action="{{url('adminlogin')}}" class="needs-validation" novalidate="">
                @csrf
                @method('post')
                  <div class="form-group">
                    <label for="email">Username</label>
                    <input type="text" class="form-control" name="username">
                    <div class="invalid-feedback">
                      Masukan Username
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="d-block">
                      <label for="password" class="control-label">Password</label>
                      <div class="float-right">
                        <a href="{{url('admin/forgot')}}" class="text-small">
                          Forgot Password?
                        </a>
                      </div>
                    </div>
                    <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
                    <div class="invalid-feedback">
                      please fill in your password
                    </div>
                  </div>

                  @if(session('message'))
                    <div class="alert alert-warning alert-dismissible show fade">
                      <div class="alert-body">
                        <button class="close" data-dismiss="alert">
                          <span>&times;</span>
                        </button>
                        {{session('message')}}
                      </div>
                    </div>
                  @endif
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                      Login
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div class="mt-5 text-muted text-center">
              Tidak Punya Akun? <a href="{{url('register')}}">Buat Akun</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

   @include('Admin.layouts.js')
</body>
</html>
