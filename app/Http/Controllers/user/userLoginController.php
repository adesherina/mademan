<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Model\UserLogin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class userLoginController extends Controller
{
    public function login(Request $request){
        // dd($request->all());

        $data = UserLogin::where('no_telp', $request->no_telp)->first();
        
        if (!$data){
            return redirect('user/register')->with('message2', 'Nomor Telepon salah');
        }else{
            if(Hash::check($request->password, $data->password)){
                Session::put('nama_lengkap',$data->nama_lengkap);
                Session::put('no_telp',$data->no_telp);

                DB::table('transaksi')
                ->where('status_pesanan', 'unverified')
                ->delete();

                session(['berhasil_login' => true]);
                return redirect('/');
            }
            return redirect('user/register')->with('message2', 'password salah');
        }
        
    }

    public function logout(Request $request){
        $request->session()->flush();
        return redirect('user/register');
    }
}
