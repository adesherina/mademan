@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Kategori Produk</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Kategori Produk</a></div>
        <div class="breadcrumb-item">Form Tambah Kategori Produk</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('kategori.simpandata')}}" method="POST" enctype="multipart/form-data">
        @method('post')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Tambah Kategori Produk</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Kategori Produk</label>
                  <input type="text" class="form-control" name="nama_kategori">
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 