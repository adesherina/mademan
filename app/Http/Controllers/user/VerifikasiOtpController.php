<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use App\User;

class VerifikasiOtpController extends Controller
{
    //tampil verifikasi OTP
    public function index(){
        return view('user.verifikasiOtp');

    }

    //proses Verifikasi OTP
    protected function verify(Request $request)
    {
        $data = $request->validate([
            'code' => ['required', 'numeric'],
            'no_telp' => ['required', 'string'],
        ]);
        $nohp = $request->no_telp;
        // kadang ada penulisan no hp 0811 239 345
        $nohp = str_replace(" ","",$nohp);
        // kadang ada penulisan no hp (0274) 778787
        $nohp = str_replace("(","",$nohp);
        // kadang ada penulisan no hp (0274) 778787
        $nohp = str_replace(")","",$nohp);
        // kadang ada penulisan no hp 0811.239.345
        $nohp = str_replace(".","",$nohp);

        // cek apakah no hp mengandung karakter + dan 0-9
        if(!preg_match('/[^+0-9]/',trim($nohp))){
            // cek apakah no hp karakter 1-3 adalah +62
            if(substr(trim($nohp), 0, 3)=='+62'){
                $hp = trim($nohp);
            }
            // cek apakah no hp karakter 1 adalah 0
            elseif(substr(trim($nohp), 0, 1)=='0'){
                $hp = '+62'.substr(trim($nohp), 1);
            }
        }
        $token = getenv("TWILIO_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
         $verification = $twilio->verify->v2->services($twilio_verify_sid)
            ->verificationChecks
            ->create($request->code, array('to' => $hp));
          $referal = rand(1111, 9999);
          if ($verification->valid) {
            
            $user = tap(User::where('no_telp', $request->no_telp))->update(['isVerified' => true]);
            /* Authenticate user */
            if($request->kode_referal === null){
                 $save = DB::table('member')->insert(
                    [
                        'no_telp' => $request->no_telp,
                        'point' => 0,
                        'status' => "Menunggu Verifikasi",
                        'referal_code' => $referal ,
                        'success_invite' =>0
                    ]);
            }else{
                $save = DB::table('member')->insert(
                    [
                        'no_telp' => $request->no_telp,
                        'point' => 0,
                        'status' => "Menunggu Verifikasi",
                        'referal_code' => $referal ,
                        'success_invite' =>0
                    ]);
                $data = DB::table('member')->where('referal_code', $request->kode_referal)->first();
                if($data === null) return redirect('user/register')->with('statusRegister', 'Berhasil Register Namun Kode Referal Tidak Ada');
                
                DB::table('member')
                    ->where('referal_code', $request->kode_referal)
                    ->update([
                        'success_invite' => (int)$data->success_invite+1,
                        'point' => (int)$data->point + 5
                    ]);  
            }
            return redirect('user/register')->with('statusRegister', 'Register berhasil silahkan bayar di kasir dengan memberikan nomor telepon anda');
        }else{
             return view('user.verifikasiOtp')->with('statusFailed', 'Verify code is not corret, Please try again');
        }
        
               
    }



    public function postVerify(Request $request)
    {
        $referal = rand(1111, 9999);
        if($request->kode_referal === null){
            
            if ($user = User::where('code', $request->code)->first()) {
                
                DB::table('users')
                ->where('code', $request->code)
                ->update(['code' => null]);
                $save = DB::table('member')->insert(
                    [
                        'no_telp' => $request->no_telp,
                        'point' => 0,
                        'status' => "Menunggu Verifikasi",
                        'referal_code' => $referal ,
                        'success_invite' =>0
                    ]);
                return redirect('user/register')->with('statusRegister', 'Register berhasil silahkan bayar di kasir dengan memberikan nomor telepon anda');
            } else {
                return view('user.verifikasiOtp')->with('statusFailed', 'Verify code is not corret, Please try again');
            }
        }elseif($request->kode_referal != null){
            if ($user = User::where('code', $request->code)->first()) {
                
                DB::table('users')
                ->where('code', $request->code)
                ->update(['code' => null,
                ]);
                $save = DB::table('member')->insert(
                    [
                        'no_telp' => $request->no_telp,
                        'point' => 0,
                        'status' => "Menunggu Verifikasi",
                        'referal_code' => $referal ,
                        'success_invite' =>0
                    ]);
                $data = DB::table('member')->where('referal_code', $request->kode_referal)->first();
                
                DB::table('member')
                    ->where('referal_code', $request->kode_referal)
                    ->update([
                        'success_invite' => (int)$data->success_invite+1,
                        'point' => (int)$data->point + 5
                    ]);  

                return redirect('user/register')->with('statusRegister', 'Register berhasil silahkan bayar di kasir dengan memberikan nomor telepon anda');
            } else {
                return view('user.verifikasiOtp')->with('statusFailed', 'Verify code is not corret, Please try again');
            }
        }
    }

}
