<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoucer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucer', function (Blueprint $table) {
            $table->id('id_voucer');
            $table->string('nama_voucer', 15);
            $table->string('kode_voucer', 15);
            $table->float('diskon', 15);
            $table->text('deskripsi');
            $table->date('expaired_voucer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucer');
    }
}
