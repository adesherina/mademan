<div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.html">Admin</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">Admin</a>
          </div>
          <ul class="sidebar-menu">
              <li class="menu-header">Dashboard</li>
              <li class="nav-item ">
                <a href="{{url('/dashboard')}}" class="nav-link "><i class="fas fa-fire"></i><span>Dashboard</span></a>
              </li>
              <li class="menu-header">Pengguna</li>
              <li class="nav-item ">
                <a href="#" class="nav-link has-dropdown" ><i class="fas fa-user"></i><span>User</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="{{url('dataadmin')}}">Admin</a></li>
                  <li><a class="nav-link" href="{{url('datauser')}}">Pengguna</a></li>
                </ul>
              </li>

              <li class="menu-header">Member</li>
              <li class="nav-item ">
                <a class="nav-link" href="{{url('datamember')}}"><i class="fas fa-users"></i><span>Member</span></a>
              </li>

              <li class="menu-header">Produk</li>
              <li class="nav-item ">
                <a class="nav-link" href="{{url('merk')}}"><i class="fas fa-tags"></i><span>Brand</span></a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="{{url('kategori')}}"><i class="fas fa-filter"></i><span>Kategori</span></a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="{{url('produk')}}"><i class="fab fa-product-hunt"></i><span>Produk</span></a>
              </li>

              <li class="menu-header">Voucer</li>
              <li class="nav-item ">
                <a class="nav-link" href="{{url('voucer')}}"><img src="{{url('assets/admin/img/coupon2.svg')}}" style="height: 18px; width:18px;" alt="" class="float-rigt ml-2 mr-3"><span>Voucer</span></a>
              </li>

              <li class="menu-header">Transaksi Penjualan</li>
              <li class="nav-item ">
                <a href="{{url('transaksi')}}" class="nav-link "><i class="fas fa-money-check-alt"></i><span>Transaksi</span></a>
              </li>

              <li class="menu-header">Reedem Point</li>
              <li class="nav-item ">
                <a href="{{url('reedem')}}" class="nav-link "><i class="fas fa-coins"></i><span>Reedem</span></a>
              </li>
              <li class="nav-item ">
                <a href="{{url('transaksiReedem')}}" class="nav-link "><i class="fas fa-money-check-alt"></i><span>Transaksi</span></a>
              </li>
              
              
        </aside>
      </div>