<!DOCTYPE html>
<html lang="en">

    <head>
        @include('user.layouts.head')
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.1/css/select2.min.css">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Checkout</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Shop</a></li>
                        <li class="breadcrumb-item active">Checkout</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

<!-- Start Conten -->
<div class="cart-box-main">
    <form action="{{route('checkoutPesanan', $nota)}}" method="post">
    @csrf
    @method('post')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-6 mb-3">
                <div class="checkout-address">
                    <div class="title-left">
                        <h3>Alamat</h3>
                    </div>
                    <form class="needs-validation" novalidate>
                        <div class="mb-3">
                            <label for="username">Nama Lengkap *</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="username" placeholder="" name="nama_lengkap" required>
                                    <div class="invalid-feedback" style="width: 100%;"> Your username is required. </div>
                                </div>
                        </div>
                        <div class="mb-3">
                            <label for="noTelp">No Telepon *</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="noTelp" placeholder="" name="no_telp" required>
                                    <div class="invalid-feedback" style="width: 100%;"> Your username is required. </div>
                                </div>
                        </div>
                        <div class="mb-3">
                            <label for="kota">Kota *</label>
                                <div class="input-group">
                                <select class="form-control" id="kota_tujuan" name="kota_tujuan" required>
                                    <option value=""> </option>
                                </select>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="email">Alamat *</label>
                            <div class="input-group">
                                <textarea class="form-control" id="message" name="alamat" rows="4" data-error="Write your message" required></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>  
                        <hr class="mb-4">
                        <div class="title"> <span>Metode Pembayaran *</span> 
                        </div>
                        <div class="d-block my-3">
                            <div class="custom-control custom-radio">
                                <input id="credit" name="paymentMethod" type="radio" value="Bayar di kasir" class="custom-control-input " required>
                                <label class="custom-control-label" for="credit">Bayar di kasir</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input id="debit" name="paymentMethod" type="radio" value="Bayar Via Transfer" class="custom-control-input" required>
                                <label class="custom-control-label" for="debit">Bayar Via Transfer</label>
                            </div>
                        </div>
                        <hr class="mb-1"> 
                    </form>
                </div>
            </div>
            <div class="col-sm-6 col-lg-6 mb-3">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="shipping-method-box">
                            <div class="title-left">
                                <h3>Pengiriman</h3>
                            </div>
                            <div class="mb-4">
                                <div class="custom-control custom-radio">
                                    <input id="shippingOption1" name="shipping_option" disabled data-etd="etd-tiki" value="tiki" class="custom-control-input delivery-method" type="radio">
                                    <label class="custom-control-label" for="shippingOption1">Tiki</label> <span class="float-right font-weight-bold shippingOption1"  ></span> 
                                </div>
                                <div class="ml-4 mb-2 small" id="etd-tiki"></div>
                                <div class="custom-control custom-radio">
                                    <input id="shippingOption2" name="shipping_option" disabled data-etd="etd-jne" value="jne" class="custom-control-input delivery-method" type="radio">
                                    <label class="custom-control-label" for="shippingOption2">JNE</label> <span class="float-right font-weight-bold shippingOption2" ></span> 
                                </div>
                                <div class="ml-4 mb-2 small" id="etd-jne">
                                </div>
                                <div class="custom-control custom-radio">
                                    <input id="shippingOption3" name="shipping_option" disabled value="pos" data-etd="etd-pos" class="custom-control-input delivery-method" type="radio">
                                    <label class="custom-control-label" for="shippingOption3">POS Indonesia</label> <span class="float-right font-weight-bold shippingOption3" ></span> 
                                </div>
                                <div class="ml-4 mb-2 small" id="etd-pos">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12">
                        <div class="odr-box">
                            <div class="title-left">
                                <h3>Produk Yang Anda Pesan</h3>
                            </div>
                            <div class="rounded p-2 bg-light">
                                @foreach($checkout as $item)
                                <div class="media mb-2 border-bottom">
                                    <div class="media-body"> <a href="#"> {{$item->nama_produk}}</a>
                                        <input type="hidden" name="id_keranjang[]" value="{{$item->keranjang_id}}">
                                        <div class="small text-muted">Rp.  {{number_format($item->harga,'0','.','.')}} <span class="mx-2">|</span> Qty: {{$item->qty}} <span class="mx-2">|</span> Subtotal: Rp. {{number_format($item->subtotal,'0','.','.')}}
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12">
                        <div class="order-box">
                            <div class="title-left">
                                <h3 class="mt-5">Pesanan Anda</h3>
                            </div>
                            <div class="d-flex">
                                <div class="font-weight-bold">Produk</div>
                                <div class="ml-auto font-weight-bold">Total</div>
                            </div>
                            <hr class="my-1">
                            <div class="d-flex">
                                <h4>Sub Total</h4>
                                <?php 
                                    $total = 0;
                                    foreach($checkout as $item):
                                       $total += (int)$item->subtotal;
                                    endforeach;
                                ?>
                                <div class="ml-auto font-weight-bold"> Rp. {{number_format($total,'0','.','.')}}</div>
                            </div>
                            <div class="d-flex">
                                <h4>Biaya Pengiriman</h4>
                                <div class="ml-auto font-weight-bold"> Rp. <span class="harga-ongkir">0</span> </div>
                                <input type="hidden" name="harga_ongkir" id="inp-harga-ongkir">
                            </div>
                            <div class="d-flex">
                                <h4>Discount</h4>
                                <div class="ml-auto font-weight-bold"> Rp.{{number_format($transaksi->diskon,'0','.','.')}}</div>
                            </div>
                            <hr class="my-1">
                            <div class="d-flex gr-total">
                                <h5>Total</h5>
                                <div class="ml-auto h5"> Rp. <span id="total-semua">{{number_format($total - $transaksi->diskon,'0','.','.')}}</span></div>
                            </div>
                            <hr> 
                        </div>
                    </div>
                    <div class="col-12 d-flex shopping-box"> <input type="submit" class="ml-auto btn hvr-hover text-white" value="Place Order" /> </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<!-- End Cart -->

    <!-- Start Instagram Feed  -->
    @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.1/js/select2.min.js"></script>

    <script>
        //ongkos kirim
        var prevOngkir =  0;
        var prevTotal = 0;

        // if in cashier
        $('#credit').click(function() {
            if($('#credit').is(':checked')) { 
                $('.delivery-method').attr('disabled',true);
                $('input:radio[name=shipping_option]').each(function(){
                    $(this).attr('disabled',true);     
                });
                if(prevOngkir != 0){
                    let total = $('span#total-semua').text();
                    total = parseFloat(total.replace(".", "").replace(".", ""));
                    let cutOngkir = total - prevOngkir;
                    // console.log(total, prevOngkir);
                    
                    var jumlah = cutOngkir.toString(),
                        sisa 	= jumlah.length % 3,
                        rupiah 	= jumlah.substr(0, sisa),
                        ribuan 	= jumlah.substr(sisa).match(/\d{3}/g);
                            
                        if (ribuan) {
                            separator = sisa ? '.' : '';
                            rupiah += separator + ribuan.join('.');
                    }
                    $('#inp-harga-ongkir').val(0);
                    $('span.harga-ongkir').text(0);
                    $('span#total-semua').text(rupiah);
                }
             }
        });

        // if delivery
         $('#debit').click(function() {
            if($('#debit').is(':checked')) { 
                $('.delivery-method').removeAttr('disabled',true);
                $('input:radio[name=shipping_option]').each(function(){
                    $(this).removeAttr('disabled',true);
                });
                let total = $('span#total-semua').text();
                    total = parseFloat(total.replace(".", "").replace(".", ""));
                if(prevOngkir != 0){
                    let nextOngkir = total + prevOngkir;
                    // console.log(total, prevOngkir);
                    
                    var jumlah = nextOngkir.toString(),
                        sisa 	= jumlah.length % 3,
                        rupiah 	= jumlah.substr(0, sisa),
                        ribuan 	= jumlah.substr(sisa).match(/\d{3}/g);
                            
                        if (ribuan) {
                            separator = sisa ? '.' : '';
                            rupiah += separator + ribuan.join('.');
                    }
                    var jumlah2 = prevOngkir.toString(),
                        sisa2 	= jumlah2.length % 3,
                        rupiah2 	= jumlah2.substr(0, sisa2),
                        ribuan2 	= jumlah2.substr(sisa2).match(/\d{3}/g);
                            
                        if (ribuan2) {
                            separator2 = sisa2 ? '.' : '';
                            rupiah2 += separator2 + ribuan2.join('.');
                    }
                    $('#inp-harga-ongkir').val(prevOngkir);
                    $('span.harga-ongkir').text(rupiah2);
                    $('span#total-semua').text(rupiah);
                }
               
             }
        });

        $('#kota_tujuan').select2({
            placeholder: '--Pilih kota/kabupaten asal--',
        });

        $.ajax({
            type: "GET",
            dataType: "html",
            url: "{{ route('get.kota') }}",
            success: function (msg) {
                $("select#kota_tujuan").html(msg);
            }
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let subtotal = "{{$total}}";
        let diskon = "{{$transaksi->diskon}}" || 0;

        // $('.custom-radio').each(function(){
                $('.delivery-method').change(function(){
                    let kurir = $(this).val();
                    let kota = $('#kota_tujuan').val();
                    let id = $(this).attr("id");
                    let etd = $(this).data("etd");
                    console.log(etd);

                    $.ajax({
                        //url: 'cek_ongkir.php',
                        url: "{{ route('cek.ongkir') }}",
                        type: 'post',
                        data: {
                            kurir: kurir,
                            kota_tujuan: kota,
                        },
                        success: function (data) {
                            var data = JSON.parse(data);
                            
                            $('span.harga-ongkir').text(data.harga);
                            let nonfix = data.harga;
                            let harga = parseFloat(nonfix.replace(".", "").replace(".", ""));
                            prevOngkir = harga;
                            $('#inp-harga-ongkir').val(data.harga);
                            $('span.'+id).text("Rp. "+data.harga);
                            $('div#'+etd).text(data.estimasi + " Hari");

                            let allTotal = (parseFloat(subtotal) + harga) - diskon; 
                            prevTotal = allTotal;
                            var jumlah = allTotal.toString(),
                            sisa 	= jumlah.length % 3,
                            rupiah 	= jumlah.substr(0, sisa),
                            ribuan 	= jumlah.substr(sisa).match(/\d{3}/g);
                                
                            if (ribuan) {
                                separator = sisa ? '.' : '';
                                rupiah += separator + ribuan.join('.');
                            }
                            $('span#total-semua').text(rupiah);
                        },
                        error: function (error) {
                            swal("Oopps!", "Pilih Kota Tujuan Terlebih Dahulu!", "error");
                            
                        },
                    });
                });
        // });

        $('#kota_tujuan').on('change', function() {
            let id_kota = $(this).val();
            let kurir = $("input[name='shipping_option']:checked").val();
            let id = $("input[name='shipping_option']:checked").attr("id");
            let etd = $("input[name='shipping_option']:checked").data("etd");
           
            $.ajax({
                url: "{{ route('cek.ongkir') }}",
                type: 'post',
                data: {
                    kurir: kurir,
                    kota_tujuan: id_kota,
                },
                success: function (data) {
                    var data = JSON.parse(data);
                    $('span.harga-ongkir').text(data.harga);
                    let nonfix = data.harga;
                    let harga = parseFloat(nonfix.replace(".", "").replace(".", ""));
                    prevOngkir = harga;
                    $('#inp-harga-ongkir').val(data.harga);
                    $('span.'+id).text("Rp. "+data.harga);
                    $('div#'+etd).text(data.estimasi + " Hari");

                    let allTotal = (parseFloat(subtotal) + harga) - diskon;
                    prevTotal = allTotal; 
                    var jumlah = allTotal.toString(),
                    sisa 	= jumlah.length % 3,
                    rupiah 	= jumlah.substr(0, sisa),
                    ribuan 	= jumlah.substr(sisa).match(/\d{3}/g);
                        
                    if (ribuan) {
                        separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                    }
                    $('span#total-semua').text(rupiah);
                },
                error: function (error) {
                    
                },
            });

        });
        // 
    </script>
</body>

</html>