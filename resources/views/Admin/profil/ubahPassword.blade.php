@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Ganti Password</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Ganti Password</div>
        </div>
      </div>
      <div class="section-body">
        <h2 class="section-title">Hi, {{Session::get('name')}}</h2>
          <p class="section-lead">
            Change information about yourself on this page.
          </p>
          <!-- Alert Update Data -->
          @if (session('editpw'))
            <div class="alert alert-success mt-3">
                {{ session('editpw') }}
            </div>
          @elseif(session('failedpw'))
            <div class="alert alert-danger mt-3">
                {{ session('failedpw') }}
            </div>
          @endif
          <div class="row mt-sm-4">
            <div class="col-12 col-md-12 col-lg-5">
              <div class="card profile-widget">
                <div class="profile-widget-header">
                  <img alt="image" src="{{url('admin-profil/'. $admin->photo)}}" class="rounded-circle profile-widget-picture" style="width: 200px;height:200px; border-radius:50%;border:2px">
                </div>
              </div>
            </div>
              <div class="col-12 col-md-12 col-lg-7">
                <div class="card">
                  <form action="{{url('admin/pw/ubah/'. $admin->id_admin)}}" method="post" class="needs-validation" novalidate="" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <div class="card-header">
                      <h4>Ganti Password</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                          <label>Password Baru</label>
                          <input type="password" class="form-control" name="newPassword">
                        </div>
                        <div class="form-group">
                          <label>Konfirmasi Password</label>
                          <input type="password" class="form-control" name="confirmPassword">
                        </div>
                    </div>
                    <div class="card-footer text-right">
                      <button class="btn btn-primary">Save Changes</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
@endsection