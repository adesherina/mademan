<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Model\Favourite;
use Illuminate\Support\Facades\DB;
use Throwable;

class ProductFavouriteController extends Controller
{
    //tampil 
    public function index(){
        $favourite = Favourite::with('productRef')->with('userRef')->where('no_telp',Session::get('no_telp'))->get();
        return view('user.profil.produkFavorite', compact('favourite'));
    }

    //tambah favorit
    public function addFavourite(Request $request)
    {
        $no_telp = $request->no_telp;
        $product_id = $request->product_id;

        try {
            $favourite = new Favourite();
            $favourite->no_telp = $no_telp;
            $favourite->product_id = $product_id;
            $favourite->save();

            $success['status'] = 'success';
            $success['data'] = 'Config save success';
            $success['html'] = '<a href="#" onclick="removeFavourite(' . $product_id . ')" id="favourite_' . $product_id . '" data-toggle="tooltip" data-placement="right" data-id-user=" ' . Session::get('no_telp') . '" title="Remove from Wishlist"><i class="fas fa-heart"></i></a>';
            return response()->json($success, 200);
        } catch (Throwable $e) {
            $success['status'] = 'error';
            $success['data'] = $e;
            return response()->json($success, 500);
        }
    }

    public function removeFavourite(Request $request)
    {
        $no_telp = $request->no_telp;
        $product_id = $request->product_id;

        try {
            Favourite::where('product_id', $product_id)->where('no_telp', $no_telp)->delete();
            $success['status'] = 'success';
            $success['data'] = 'Config save success';
            $success['html'] = '<a href="#" onclick="addFavourite(' . $product_id . ')" id="favourite_' . $product_id . '" data-toggle="tooltip" data-placement="right" data-id-user=" ' . Session::get('no_telp') . '" title="Add to Wishlist"><i class="far fa-heart"></i></a>';
            return response()->json($success, 200);
        } catch (Throwable $e) {
            $success['status'] = 'error';
            $success['data'] = $e;
            return response()->json($success, 500);
        }
    }
}
