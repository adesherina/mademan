<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class userProfilController extends Controller
{
    //tampil update profil
    public function update($no_telp){
        $user = DB::table('users')->where('no_telp', $no_telp)->first();
        return view('user.profil.akun', ['user' => $user]);
      

    }

    //simpan update profil
    public function updateProcess(Request $request, $no_telp){
        if($request->hasFile('photo')){
            $resorce = $request->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/user-profil", $name);
            DB::table('users')->where('no_telp', Session::get('no_telp') )->update(
                [
                    'no_telp' => $request->no_telp,
                    'nama_lengkap' => $request->nama_lengkap,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'alamat' => $request->alamat,
                    'photo' => $name
                ]);
            return redirect()->route('user.update',$no_telp)->with('statusSuccess1', 'Data Berhasil Di Update');
        }else{
            DB::table('users')->where('no_telp', Session::get('no_telp') )->update(
                [
                    'no_telp' => $request->no_telp,
                    'nama_lengkap' => $request->nama_lengkap,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'alamat' => $request->alamat,
                    'photo' => $request->oldPhoto
                ]);
            return redirect()->route('user.update',$no_telp) ->with('statusSuccess2', 'Data Berhasil Di update');
        }
        
    }

    //tampil update Password
    public function updatePw($no_telp){
        $user = DB::table('users')->where('no_telp', $no_telp)->first();
        return view('user.profil.ubahPassword', ['user' => $user]);
        // return view('user.profil.akun');

    }

    //Update password
    public function updatePassword(Request $request, $no_telp)
    {
        
        $newPassword = $request->newPassword;
        if ($newPassword === $request->confirmPassword) {
            DB::table('users')->where('no_telp', $no_telp)->update([
                'password' => Hash::make($request->newPassword),
            ]);
            return redirect()->back()->with("editpw", "Berhasil Ganti Password");
        } else {
           return redirect()->back()->with("failedpw", "Gagal Ganti Password");
        }
    }
}
