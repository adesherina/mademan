// tambah keranjang
// $(".tambah-keranjang").each(function () {
//     $(this).on("click", function () {
//         event.preventDefault();
//         var id_produk = $(this).data("produkid");
//         var harga = $(this).data("produkharga");
//         var qty = $(this).data("produkqty");
//         var csrf = $(this).data("produkcsrf");
//         // console.log(id_produk);
//         var id_user = $(this).data("id-user");
//         console.log(id_user, harga, csrf);
//         if (id_user != "") {
//             if (id_produk != "") {
//                 $.ajax({
//                     url: "keranjang/add", //harus sesuai url di buat di route
//                     type: "POST",
//                     data: {
//                         _token: csrf,
//                         id_produk: id_produk,
//                         qty: qty,
//                         harga: harga,
//                     },
//                     cache: false,
//                     success: function (dataResult) {
//                         var dataResult = JSON.parse(dataResult);
//                         console.log(dataResult.produk, dataResult.keranjang);
//                         if (dataResult.statusCode == 200) {
//                             swal({
//                                 title: "Produk telah di tambahkan ke keranjang",
//                                 icon: "success",
//                                 type: "success",
//                             });
//                         } else if (dataResult.statusCode == 201) {
//                             alert("Error occured !");
//                         }
//                     },
//                     error: function (error) {
//                         swal({
//                             title: "Opps!",
//                             text: "Anda Harus login Terlebih Dahulu!",
//                             icon: "error",
//                         });
//                     },
//                 });
//             } else {
//                 alert("Please fill all the field !");
//             }
//         } else {
//             swal({
//                 icon: "error",
//                 title: "Oops...",
//                 text: "Anda Harus Login Terlebih Dahulu",
//             });
//         }
//     });
// });

//detail keranjang
$(document).ready(function () {
    $(".detail-keranjang").on("click", function () {
        // agar tidak scrolling ketika klik link
        event.preventDefault();
        let id_user = $(this).data("id-user");
        var id_produk = $("#id_product").val();
        var qty = $("#quantity").val();
        var harga = String($("#harga").text());
        var hargafix = parseFloat(harga.replace(".", "").replace(".", ""));

        if (id_produk != "") {
            //   $("#butsave").attr("disabled", "disabled");
            $.ajax({
                url: "/mademan/public/keranjang/add",
                type: "POST",
                data: {
                    _token: $("#product-csrf").val(),
                    id_produk: id_produk,
                    qty: qty,
                    harga: hargafix,
                },
                cache: false,
                success: function (dataResult) {
                    var dataResult = JSON.parse(dataResult);
                    if (dataResult.statusCode == 200) {
                        swal({
                            title: "Produk telah di tambahkan ke keranjang",
                            icon: "success",
                            type: "success",
                        });
                    } else if (dataResult.statusCode == 201) {
                        alert("Error occured !");
                    }
                },
                error: function (error) {
                    swal({
                        title: "Opps!",
                        text: "Anda Harus login Terlebih Dahulu!",
                        icon: "error",
                    });
                },
            });
        } else {
            alert("Please fill all the field !");
        }
    });
});

//alert delete keranjang
$(".deleteKeranjang").each(function (index, value) {
    $(this).on("click", function (event) {
        event.preventDefault();
        const url = $(this).attr("href");
        swal({
            title: "Apakah kamu yakin?",
            text: "Produk ini akan di hapus?",
            icon: "warning",
            buttons: ["Batal", "Ya!"],
        }).then(function (value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
});
