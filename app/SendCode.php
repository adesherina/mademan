<?php

namespace App;

class SendCode
{
    public static function SendCode($phone)
    {
        $code = rand(1111, 9999);
        $nexmo = app('Nexmo\Client');

        $nexmo->message()->send([
            'to'   => '+62' . (int) $phone,
            'from' => 'mademan',
            'text' => 'Verify code : ' . $code,
        ]);
        return $code;
    }
}
