<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class userVoucerController extends Controller
{
    //tampil voucer
    public function index(){
        $voucer = DB::table('voucer')->get();
        return view('user.voucer.voucer', ['voucer'=> $voucer]);

    }

    //detail data
    public function detail($id){
        $voucer = DB::table('voucer')->where('id_voucer', $id)->first();
        return view('user.voucer.detail', ['voucer'=> $voucer]);

    }
}
