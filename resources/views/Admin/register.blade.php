<!DOCTYPE html>
<html lang="en">
<head>
    @include('Admin.layouts.header')
</head>

<body>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
            <div class="login-brand">
              Register Admin
              <!-- <img src="../assets/img/stisla-fill.svg" alt="logo" width="100" class="shadow-light rounded-circle"> -->
            </div>

            <div class="card card-primary">
              <div class="card-header"><h4>Register</h4></div>

              <div class="card-body">
                <form method="POST" action="{{route('register')}}">
                  @csrf
                  @method('post')

                  <div class="form-group">
                    <label for="email">Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama">
                    
                    {{-- <div class="invalid-feedback">
                    </div> --}}
                    @if ($errors->has('nama'))
                      <span class="text-danger">{{ $errors->first('nama') }}</span>
                    @endif
                  </div>

                  <div class="form-group">
                    <label for="email">Username</label>
                    <input type="text" class="form-control" name="username">
                    
                    {{-- <div class="invalid-feedback">
                    </div> --}}
                    @if ($errors->has('username'))
                      <span class="text-danger">{{ $errors->first('username') }}</span>
                    @endif
                  </div>

                  <div class="form-group">
                    <label for="email">Password</label>
                    <input type="password" class="form-control" name="password">
                    
                    {{-- <div class="invalid-feedback">
                    </div> --}}
                    @if ($errors->has('password'))
                      <span class="text-danger">{{ $errors->first('password') }}</span>
                    @endif
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                      Register
                    </button>
                  </div>
                </form>
              </div>
          </div>
          <div class="simple-footer">
              Copyright &copy; Mademan Barbershop 2020
          </div>
        </div>
            
          </div>
        </div>
      </div>
    </section>
  </div>

   @include('Admin.layouts.js')
</body>
</html>
