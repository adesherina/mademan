@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
   <section class="section">
      <div class="section-header">
        <h1>Reedem Point</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Data Reedem Point</a></div>
          <div class="breadcrumb-item">Reedem Point</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Data Reedem Point</h4>
            <div class="card-header-action">
              <a href="{{route('reedem.tambahdata')}}" class="btn btn-success btn-lg">
              Add
              </a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="tabel-data" class="table table-bordered table-md">
                <thead>
                  <tr>
                    <th class="text-center">
                       No
                    </th>
                    <th>Nama Produk</th>
                    <th>Point</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach ($reedem as $reedem)
                 <tr>
                   <td class="text-center">
                     {{ $loop->iteration }}
                    </td>
                    <td>{{$reedem->judul}}</td>
                    <td>{{$reedem->point}}</td>
                    <td>
                      <a href="{{route('reedem.editdata', $reedem->id_reedem)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                      </a>
                      <form action="{{route('reedem.deletedata', $reedem->id_reedem)}}" id="delete{{$reedem->id_reedem}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                      @method('delete')
                      <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                      @csrf
                      </form>
                      <a href="{{route('reedem.detaildata', $reedem->id_reedem)}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-eye"></i></a>
                    </td>
                 </tr>
                @endforeach
                </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</section>
</div> 
@endsection