<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('no_telp', 15)->primary();
            $table->string('code')->nullable();
            $table->string('nama_lengkap', 50);
            $table->string('email');
            $table->text('alamat')->nullable();
            $table->string('jenis_kelamin', 10)->nullable();
            $table->string('photo', 255)->nullable();
            $table->string('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
