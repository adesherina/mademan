<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiReedem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_reedem', function (Blueprint $table) {
            $table->id('id_transaksi');
            $table->unsignedBigInteger('reedem_id');
            $table->unsignedBigInteger('member_id');
            $table->timestamps();

            $table->foreign('reedem_id')->references('id_reedem')->on('reedem_point')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('member_id')->references('id_member')->on('member')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_reedem');
    }
}
