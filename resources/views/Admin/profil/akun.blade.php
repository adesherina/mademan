@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Profil</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Profil</div>
        </div>
      </div>
      <div class="section-body">
        <h2 class="section-title">Hi, {{Session::get('name')}}</h2>
          <p class="section-lead">
            Change information about yourself on this page.
          </p>
          <!-- Alert Update Data -->
          @if (session('statusSuccess1'))
            <div class="alert alert-success mt-3">
              {{ session('statusSuccess1') }}
            </div>
          @elseif(session('statusSuccess2'))
            <div class="alert alert-success mt-3">
              {{ session('statusSuccess2') }}
            </div>
          @endif
          <div class="row mt-sm-4">
            <div class="col-12 col-md-12 col-lg-5">
              <div class="card profile-widget">
                <div class="profile-widget-header">
                  <img alt="image" src="{{url('admin-profil/'. $admin->photo)}}" class="rounded-circle profile-widget-picture" style="width: 200px;height:200px; border-radius:50%;border:2px">
                </div>
              </div>
            </div>
              <div class="col-12 col-md-12 col-lg-7">
                <div class="card">
                  <form action="{{url('admin/'. $admin->id_admin)}}" method="post" class="needs-validation" novalidate="" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <div class="card-header">
                      <h4>Edit Profil</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                          <label>Nama Lengkap</label>
                          <input type="text" class="form-control" value="{{$admin->nama}}" name="nama">
                        </div>
                        <div class="form-group">
                          <label>Username</label>
                          <input type="text" class="form-control" value="{{$admin->username}}" name="username">
                        </div>
                        <div class="form-group">
                          <label>Nomor Telepon</label>
                          <input type="text" class="form-control" value="{{$admin->no_telp}}" name=" no_telp">
                        </div>
                        <div class="form-group">
                          <label class="d-block">Jenis Kelamin</label>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="jenis_kelamin" id="exampleRadios1" <?php if($admin->jenis_kelamin == "Laki-laki"){ ?> checked <?php } ?>  value="Laki-laki">
                            <label class="form-check-label" for="exampleRadios1">
                              Laki - Laki
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="jenis_kelamin" id="exampleRadios2" <?php if($admin->jenis_kelamin == "Perempuan"){ ?> checked <?php } ?>  value="Perempuan">
                            <label class="form-check-label" for="exampleRadios2">
                              Perempuan
                            </label>
                          </div>
                        </div>
                        <div class="form-group">
                          <label>Alamat</label>
                          <textarea class="form-control summernote-simple" name="alamat">{{$admin->alamat}}</textarea>
                        </div>
                        <div class="form-group">
                          <label>Photo</label>
                          <input type="file" class="form-control" value="" name="photo">
                          <input type="hidden" name="oldPhoto" value="{{$admin->photo}}">
                        </div>
                    </div>
                    <div class="card-footer text-right">
                      <button class="btn btn-primary">Save Changes</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
@endsection