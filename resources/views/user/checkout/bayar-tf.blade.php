<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    @include('user.layouts.head')
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Pembayaran</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Checkout</a></li>
                        <li class="breadcrumb-item active">Pembayaran</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <div class="cart-box-main">
        <div class="container">
            <div class="row new-account-login">
                <div class="col-sm-12 col-lg-12 mb-12">
                    <div class="title-left">
                        <h3>Pembayaran Via Transfer</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                    <div class="card" style=" box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
                    transition: all 0.3s cubic-bezier(.25,.8,.25,1);">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <p class="h5"><b>
                                        Nomor Rekening
                                    </b></p>
                                    <hr class="text-dark mt-2">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="row mb-3">
                                        <div class="col-lg-2">
                                            <img src="{{url('assets/user/images/payment-icon/bri.png')}}" alt="Logo BRI" style="width: 45px; height:45px;">
                                        </div>
                                        <div class="col-6 col-lg-4 pt-3">
                                            <h4 class=""> 6212982790</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <img src="{{url('assets/user/images/payment-icon/bni.png')}}" alt="Logo BNI" style="width: 75px; height:45px;">
                                        </div>
                                        <div class="col-lg-4 col-6 pt-3">
                                            <h4 class=""> 6212982790</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                <form action="{{route('bayarTf.simpandata', $nota)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <label for="username" class="mt-5">Upload Bukti Transfer</label>
                    <div class="input-group">
                        <input type="file" class="form-control" id="username" placeholder="" name="bukti_tf" required>
                    </div>
                    <input class="btn hvr-hover mt-3" id="submit" type="submit" value="Upload">
                </form>
                </div>
            </div>
        </div>
    </div>


       <!-- Start Instagram Feed  -->
        @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
    <script type="text/javascript">
    function copy_text() {
        document.getElementById("referal").select();
        document.execCommand("copy");
        alert("Kode Referal berhasil dicopy");
    }
</script>
</body>

</html>