@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Detail Member</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Detail Member</a></div>
          <div class="breadcrumb-item">Member</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Detail Member</h4>
            <div class="card-header-action">
              <a href="{{url('datamember')}}" class="btn btn-success btn-lg">
              kembali
            </a>
            </div>
          </div>

            <div class="col-12 mt-3 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Nama Member : {{$member->nama_lengkap}}</h4>
                    </div>
                    <div class="card-body">
                        <p class="card-text text-primary">No Telepon : {{$member->no_telp}}</p>
                    </div>
                    <div class="card-body">
                        <p class="card-text text-primary">Point : {{$member->point}}</p>
                    </div>
                    <div class="card-body">
                        <p class="card-text text-primary">Success Invite : {{$member->success_invite}}</p>
                    </div>
                </div>
            </div>

        </div>
      </div>
  </section>
</div> 
@endsection