<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Product;

class Favourite extends Model
{
    //
    public $timestamps = true;

    public function userRef(){
        return $this->belongsTo(User::class, 'no_telp','no_telp');
    }

    public function productRef(){
        return $this->belongsTo(Product::class, 'product_id','id_produk');
    }
}
