
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    @include('user.layouts.head')
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>My Account</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Account</a></li>
                        <li class="breadcrumb-item active">My Account</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

<!-- Start Contact Us  -->
    <div class="contact-box-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-12">
                    <div class="contact-info-left">
                        <div class="col-sm-12 col-lg-12 mb-3">
                            <div class="title-left">
                                <h3>Login Member</h3>
                                @if (session('message'))
                                <div class="alert alert-success">
                                    {{ session('message') }}
                                </div>
                                @elseif(session('message2'))
                                <div class="alert alert-danger">
                                    {{ session('message2') }}
                                </div>
                                @endif
                            </div>
                            <h5><a data-toggle="collapse" href="#formLogin" role="button" aria-expanded="false">Click here to Login</a></h5>
                            <form class="mt-3 collapse review-form-box" id="formLogin" action="{{url('user/login')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('post')
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="InputEmail" class="mb-0">Nomor Telepon</label>
                                        <input type="text" class="form-control" id="InputEmail" name="no_telp"> </div>
                                    <div class="form-group col-md-6">
                                         <label for="InputEmail" class="mb-0">Password</label>
                                        <input type="password" class="form-control" id="InputPassword" name="password"> </div>
                                </div>
                                    <button type="submit" class="btn hvr-hover mr-3">Login</button>
                                    <a href="{{route('forgotpw')}}">Lupa Password ? </a>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-sm-12">
                    <div class="contact-form-right">
                        
                        <h2>Register</h2>
                        @if (session('statusRegister'))
                            <div class="alert alert-success">
                                {{ session('statusRegister') }}
                            </div>
                        @elseif(session('statusFailed'))
                         <div class="alert alert-danger">
                                {{ session('statusFailed') }}
                            </div>
                        @endif
                        <p>Belum Jadi Member? Register Sekarang</p>
                        <p>Price : Rp. 300.000</p>
                        <form method="post" action="{{route('user.register')}}" enctype="multipart/form-data">
                        @csrf
                        @method('post')
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" value="{{old('nama_lengkap')}}" name="nama_lengkap" placeholder="Nama Lengkap"  data-error="Please enter your name">
                                        @if ($errors->has('nama_lengkap'))
                                            <span class="text-danger">{{ $errors->first('nama_lengkap') }}</span>
                                        @endif

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="subject" name="no_telp" value="{{old('no_telp')}}" placeholder="Nomor Telepon"  data-error="Please enter your Subject">
                                        @if ($errors->has('no_telp'))
                                            <span class="text-danger">{{ $errors->first('no_telp') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="subject" value="{{old('email')}}"  name="email" placeholder="Email"  data-error="Please enter your Subject">
                                        @if ($errors->has('email'))
                                            <span class="text-danger">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" id="message" name="alamat"    placeholder="Alamat" rows="4" data-error="Write your message" >{{old('alamat')}}</textarea>
                                        @if ($errors->has('alamat'))
                                            <span class="text-danger">{{ $errors->first('alamat') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="subject" name="password"  placeholder="Password"  data-error="Please enter your Subject">
                                        @if ($errors->has('password'))
                                            <span class="text-danger">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="subject" name="confirmPassword" placeholder="Konfirmasi Password"  data-error="Please enter your Subject">
                                        @if ($errors->has('confirmPassword'))
                                            <span class="text-danger">{{ $errors->first('confirmPassword') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="subject" name="kode_referal" placeholder="Masukan Kode Referal(optional)..."  data-error="Please enter your Subject">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="submit-button text-left">
                                        <button class="btn hvr-hover" id="submit" type="submit">Sign Up</button>
                                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Cart -->

       <!-- Start Instagram Feed  -->
       @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
</body>

</html>