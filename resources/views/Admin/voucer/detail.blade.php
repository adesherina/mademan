@extends('Admin.master')
@section('title', 'Mademan Barbershop')



@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Detail Voucer</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Detail Voucer</a></div>
          <div class="breadcrumb-item">Voucer</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Detail Voucer</h4>
            <div class="card-header-action">
              <a href="{{url('voucer')}}" class="btn btn-success btn-lg">
              kembali
            </a>
            </div>
          </div>
            <div class="col-12 mt-3 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Nama Voucer</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                              <div class="overflow-auto text-primary" style="max-height: 200px">
                                {{$voucer->nama_voucer}}
                              </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Kode Voucer</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                              <div class="overflow-auto text-primary" style="max-height: 200px">
                                {{$voucer->kode_voucer}}
                              </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Diskon</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                              <div class="overflow-auto text-primary" style="max-height: 200px">
                                {{$voucer->diskon}}
                              </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Expired Voucer</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                              <div class="overflow-auto text-primary" style="max-height: 200px">
                                {{$voucer->expaired_voucer}}
                              </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Deskripsi</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                              <div class="overflow-auto text-primary" style="max-height: 200px">
                                <?= $voucer->deskripsi ?>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
  </section>
</div> 
@endsection