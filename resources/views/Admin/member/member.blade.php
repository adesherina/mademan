@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
        <section class="section">
          <div class="section-header">
              <h1>Member</h1>
              <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data Member</a></div>
                <div class="breadcrumb-item">Member</div>
              </div>
          </div>
          <div class="section-body">
            <!-- Alert  -->
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card card-primary">
              <div class="card-header">
                <h4>Data Member</h4>
                <div class="card-header-action">
                  <a href="#" class="btn btn-primary">
                    View All
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive" >
                  <table id="tabel-data" class="table table-bordered table-md">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>No Telp</th>
                        <th>Point</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($member as $member)
                      <tr>
                        <td>{{$loop->iteration}}</td>  
                        <td>{{$member->nama_lengkap}}</td>
                        <td>{{$member->no_telp}}</td>
                        <td>{{$member->point}}</td>
                        <td>
                          @if($member->status == "Menunggu Verifikasi")
                          <div class="badge badge-warning">{{$member->status}}</div>
                          @elseif($member->status == "Aktif")
                          <div class="badge badge-success">{{$member->status}}</div>
                          @else
                          <div class="badge badge-danger">{{$member->status}}</div>
                          @endif
                        </td>
                        <td>
                          <a href="{{route('member.update', $member->id_member)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i></a>
                          <form action="{{url('datamember/delete', $member->id_member)}}" id="delete{{$member->id_member}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                              @method('delete')
                              <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                              @csrf
                          </form>
                          <a href="{{route('member.detaildata', $member->id_member)}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-eye"></i></a>
                      </td>
                      </tr>    
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div> 
        </section>
      </div> 
@endsection
 