-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2021 at 02:34 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `idam_mademan`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_telp` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `username`, `password`, `jenis_kelamin`, `no_telp`, `alamat`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'Ade Sherina', 'ades', '$2y$10$A7v62yAQBQVUivASdroheeMrPWYUbQ7ywnOinMRfp7ecuq4TSFfyq', 'Perempuan', '0895379282402', 'Indramayu', '49583.jpg', NULL, NULL),
(2, 'Yuni', 'yuni', '$2y$10$k33UjIBjt59ohH6vQAUajuFVIthgHGR86djxkHGLRpAvcpZskMqEK', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Siti Sholeha', 'sitisole', '$2y$10$cvbypfrh.9W4BMXMMsKTZu8tC.nzXV.ZoO.qcxP22.B7A5xpK/zMS', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `id` bigint(20) NOT NULL,
  `nota` varchar(15) NOT NULL,
  `keranjang_id` bigint(20) UNSIGNED DEFAULT NULL,
  `id_produk` bigint(20) UNSIGNED DEFAULT NULL,
  `no_telp` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `detail_transaksi`
--

INSERT INTO `detail_transaksi` (`id`, `nota`, `keranjang_id`, `id_produk`, `no_telp`, `harga`, `qty`, `subtotal`, `create_at`, `update_at`) VALUES
(50, 'DE3dM', 54, 19, '082115806156', 10000, 1, 10000, '2021-01-19 12:46:56', '2021-01-19 12:46:56'),
(51, 'UeV4c', 56, 18, '082115806156', 49900, 1, 49900, '2021-01-19 13:46:28', '2021-01-19 13:46:28'),
(52, 'UeV4c', 57, 19, '082115806156', 10000, 1, 10000, '2021-01-19 13:46:28', '2021-01-19 13:46:28'),
(54, 'Kpr26', 60, 31, '082115806156', 98500, 1, 98500, '2021-01-22 04:37:28', '2021-01-22 04:37:28'),
(55, 'Kpr26', 61, 30, '082115806156', 34500, 1, 34500, '2021-01-22 04:37:28', '2021-01-22 04:37:28'),
(57, 'R06Qw', 82, 32, '082115806156', 165500, 1, 165500, '2021-02-04 12:00:08', '2021-02-04 12:00:08'),
(60, 'c7icm', 84, 32, '082115806156', 165500, 1, 165500, '2021-02-18 13:12:15', '2021-02-18 13:12:15'),
(61, 'AmghO', 83, 18, '082115806156', 59900, 1, 59900, '2021-02-19 12:59:21', '2021-02-19 12:59:21'),
(67, 'OgdAB', 85, 19, '082115806156', 120000, 1, 120000, '2021-02-24 02:35:01', '2021-02-24 02:35:01'),
(78, 'VixOg', 89, 20, '082115806156', 120000, 4, 480000, '2021-03-08 03:46:40', '2021-03-08 03:46:40');

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE `favourites` (
  `id` int(11) NOT NULL,
  `no_telp` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `favourites`
--

INSERT INTO `favourites` (`id`, `no_telp`, `product_id`, `created_at`, `updated_at`) VALUES
(11, '082115806156', 20, '2021-03-02 14:21:52', '2021-03-02 14:21:52');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(10) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Oil Based'),
(2, 'Water Based'),
(3, 'Mix Baseed');

-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

CREATE TABLE `keranjang` (
  `id_keranjang` bigint(20) UNSIGNED NOT NULL,
  `no_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_produk` bigint(20) UNSIGNED NOT NULL,
  `harga` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `keranjang`
--

INSERT INTO `keranjang` (`id_keranjang`, `no_telp`, `id_produk`, `harga`, `qty`, `subtotal`, `created_at`, `updated_at`) VALUES
(86, '082115806156', 18, 59900, 1, 59900, NULL, NULL),
(87, '082115806156', 19, 120000, 2, 240000, NULL, NULL),
(88, '082115806156', 22, 38900, 2, 77800, NULL, NULL),
(89, '082115806156', 20, 120000, 4, 480000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id_member` bigint(20) UNSIGNED NOT NULL,
  `no_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `point` int(11) NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referal_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `success_invite` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `no_telp`, `point`, `status`, `referal_code`, `success_invite`, `created_at`, `updated_at`) VALUES
(1, '082115806156', 9015, 'Aktif', '1640', 4, NULL, NULL),
(2, '087838384632', 1410, 'Aktif', '5263', 1, NULL, NULL),
(6, '081295491852', 0, 'Menunggu Verifikasi', '3535', 0, NULL, NULL),
(8, '087822814574', 0, 'Menunggu Verifikasi', '4414', 0, NULL, NULL),
(9, '089668058996', 5, 'Aktif', '7067', 0, NULL, NULL),
(10, '085839950597', 5, 'Aktif', '3060', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `merk`
--

CREATE TABLE `merk` (
  `id_merk` int(10) NOT NULL,
  `nama_merk` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `merk`
--

INSERT INTO `merk` (`id_merk`, `nama_merk`) VALUES
(1, 'Chief Pomade'),
(2, 'Belagio Pomade'),
(3, 'Captain Pomade'),
(4, 'Bad Lab Pomade'),
(5, 'Axe pomade'),
(6, 'Marlboro Pomade'),
(7, 'Tezzen Pomade'),
(8, 'Gatsby Pomade');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(20, '2020_12_08_035917_create_users', 2),
(21, '2020_12_08_053303_create_admin', 0),
(23, '2020_12_11_030110_create_produk', 1),
(24, '2020_12_08_053325_create_member', 3),
(31, '2020_12_15_061122_create_keranjang', 4),
(38, '2020_12_16_045811_create_voucer', 5),
(48, '2020_12_22_121205_create_transaksi', 6),
(54, '2021_01_06_083147_create_reedem_point', 7),
(55, '2021_01_06_095952_create_transaksi_reedem', 8),
(59, '2020_12_28_031650_create_detail_transaksi', 9);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` bigint(20) UNSIGNED NOT NULL,
  `nama_produk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` int(10) NOT NULL,
  `merk` int(10) NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_produk`, `kategori`, `merk`, `harga`, `stok`, `deskripsi`, `foto`, `created_at`, `updated_at`) VALUES
(18, 'BAD LAB Supersonik Water-Based Pomade Medium & Glossy 40g', 2, 4, 59900, 7, 'PARFUM: American Pear\r\nKelebihan: Mengandung Vitamin E untuk menutrisi rambut\r\nJenis Rambut: Lurus & Ikal', 'BAD LAB Supersonik Water-Based Pomade Medium & Glossy 80g.png', NULL, NULL),
(19, 'CAPTAIN Clay Pomade', 2, 3, 120000, 20, 'CAPTAIN Clay Pomade\r\nPomade terbaik bagi Anda yang ingin rambutnya terlihat lebih ber-volume dan tampak mengembang. Berfungsi baik dalam menjaga bentuk rambut agar bertahan lama seharian. Menghasilkan kilau yang natural pada rambut. Mudah dibilas dan cocok untuk segala jenis rambut.', 'CAPTAIN Clay Pomade.jpeg', '2021-01-19 12:03:28', NULL),
(20, 'CAPTAIN Black Pomade', 1, 3, 120000, 16, 'CAPTAIN Black Pomade\r\nPomade yang dapat membuat warna rambut tampak hitam seketika dan berkilau alami. Mengandung Argan & Jojoba Oil untuk membantu mengurangi kerontokan dan membantu merangsang pertumbuhan rambut hitam. Cocok untuk segala jenis rambut.', 'CAPTAIN Black Pomade.jpeg', '2021-01-19 12:05:22', NULL),
(21, 'Pomade Chief Junior Glass Jar Original 120gr', 2, 1, 74500, 8, 'Water Based Pomade\r\nCocok untuk Anak usia 3-10 tahun (Mild Hold)\r\nAroma: Arizona Honeydew (Melon Segar)\r\nWater based (sangat mudah dibilas) ', 'Pomade Chief Junior Glass Jar Original 120gr.jpeg', '2021-01-19 12:08:09', NULL),
(22, 'Bellagio Homme High Shine & Strong Hold Pomade 80gr', 1, 2, 38900, 9, 'Bellagio Homme Pomade High Shine and Strong Hold (Red) Water Based 80gr  Dari produsen parfum BELLAGIO yang aromanya sudah tidak diragukan lagi, kini hadir BELLAGIO Pomade oil-based untuk gaya rambut yang trendy dan mengkilap. Mengandung bahan-bahan yang yang berkualitas tinggi untuk menghasilkan rambut yang mudah diatur dan restyle sepanjang hari.', 'Bellagio Homme High Shine & Strong Hold Pomade 80gr.jpeg', '2021-01-19 12:10:01', NULL),
(29, 'BAD LAB Matte Max Strong & Matte Water-Based Pomade', 2, 4, 120000, 20, 'omade berbahan dasar air-namun tetap kuat dengan Purifying FluidiPureTM 8G dan charcoal untuk membantu Anda melawan bau tak sedap dan iritasi pada kulit kepala. Kencan, olahraga di gym, atau rencana mendominasi dunia, rambut Anda tidak akan bergerak kecuali jika Anda menyuruhnya melakukannya.', 'BAD LAB Matte Max Strong & Matte Water-Based Pomade 40g.jpeg', '2021-01-19 12:25:51', NULL),
(30, 'Axe Hair Styling Perfect Control Pomade 75 gr', 2, 5, 34500, 10, 'Merupakan pomade berbahan dasar air yang mudah dicuci. Cocok untuk Anda yang memiliki panjang rambut medium atau pendek. Memberikan tampilan clean cut yang rapi dan sempurna.', '4de5d68d435f34b4346dcba750863c4c.jpeg', NULL, NULL),
(31, 'Pomade Marlboro Waterbased Strong + EDT Napoleon Bleu - Free Tas', 2, 6, 98500, 10, 'Tak perlu malu dengan bau tubuh yang kurang sedap, Napoleon Bleu hadir dengan wangi yang memberi kesan casual dan berkelas. Napoleon Bleu dengan tipe Eau De Toilette merupakan parfum dengan grade super, hadir dengan wangi yang menawan, mampu bertahan di tengah teriknya matahari dan aktivitas seharian.', 'marlboro_marlboro_full01.jpg', NULL, NULL),
(32, 'Tezzen Pomade Delta Charlie Activated Charcoal Clay Free Sisir Carbon Pouch', 3, 7, 165500, 2, 'Tezzen Delta Activated Charcoal Clay merupakan sebuah persembahan istimewa yang diperuntukan bagi pria energik yang penuh semangat akan pembaharuan namun tetap tampil elegan.', 'd6c7b7a9474f2ed2fc8a7a81b3890957.jpeg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reedem_point`
--

CREATE TABLE `reedem_point` (
  `id_reedem` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `point` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reedem_point`
--

INSERT INTO `reedem_point` (`id_reedem`, `judul`, `foto`, `point`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'Pomade Chief Red Oil Based Travel Pack Original 35Gr', '2373806293ce8ead93a383e9d2bec674.jpeg', '600', 'Tukar Point anda dengan pomade', NULL, NULL),
(6, 'Pomade Belagio', 'Bellagio Homme High Shine & Strong Hold Pomade 80gr.jpeg', '300', 'tukar point 1000 dapatkan pomade belagio', NULL, NULL),
(7, 'Pomade Marlboro Waterbased Strong', '795688_strong.jpg', '450', 'Some quick example text to build on the card title and make up the bulk of the card\'s content.', NULL, NULL),
(8, 'CAPTAIN Black Pomade', 'CAPTAIN Black Pomade.jpeg', '1000', 'Some quick example text to build on the card title and make up the bulk of the card\'s content.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `nota` varchar(15) NOT NULL,
  `no_telp` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `diskon` int(11) DEFAULT NULL,
  `total` int(11) NOT NULL,
  `status_pesanan` varchar(50) DEFAULT NULL,
  `bukti_tf` varchar(255) DEFAULT NULL,
  `jasa_kirim` longtext DEFAULT NULL,
  `data_pembeli` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`nota`, `no_telp`, `jumlah`, `diskon`, `total`, `status_pesanan`, `bukti_tf`, `jasa_kirim`, `data_pembeli`, `created_at`, `updated_at`) VALUES
('AmghO', '082115806156', 1, 0, 75900, 'unpaid', NULL, '[{\"no\":\"s5RMD\",\"jasaPengirim\":\"tiki\",\"resi\":null,\"ongkir\":\"16.000\"}]', '[{\"no\":\"CAKvC\",\"namaLengkap\":\"Sherin\",\"noTelp\":\"082115806156\",\"alamat\":\"Indramayu\"}]', '2021-02-19 05:59:21', '2021-02-19 06:00:08'),
('c7icm', '082115806156', 1, 0, 184500, 'Selesai', '1613654419_848 (2).jpg', '[{\"no\":\"Bfc95\",\"jasaPengirim\":\"jne\",\"resi\":\"J0091982\",\"ongkir\":\"19.000\"}]', '[{\"no\":\"C4A4a\",\"namaLengkap\":\"Ade Sherina\",\"noTelp\":\"082115806156\",\"alamat\":\"Indramayu\"}]', '2021-02-18 06:12:15', '2021-02-18 06:14:23'),
('DE3dM', '082115806156', 1, 0, 29000, 'Menunggu Verifikasi', '1611060504_848.jpg', '[{\"no\":\"i5pyM\",\"jasaPengirim\":\"jne\",\"resi\":null,\"ongkir\":\"19.000\"}]', '[{\"no\":\"UrVNS\",\"namaLengkap\":\"Ade Sherina\",\"noTelp\":\"082115806156\",\"alamat\":\"kandanghaur\"}]', '2021-01-19 05:46:56', '2021-01-19 05:48:08'),
('Kpr26', '082115806156', 2, 10640, 122360, 'unpaid', NULL, NULL, '[{\"no\":\"DjVnh\",\"namaLengkap\":\"Ade Sherina\",\"noTelp\":\"082115806156\",\"alamat\":\"Indramayu\"}]', '2021-01-21 21:37:28', '2021-01-21 21:38:16'),
('OgdAB', '082115806156', 1, 0, 120000, 'unpaid', NULL, '[{\"no\":\"wzKCe\",\"jasaPengirim\":\"jne\",\"resi\":null,\"ongkir\":\"0\"}]', '[{\"no\":\"Ao4Mx\",\"namaLengkap\":\"Wantrisnadi Gusti\",\"noTelp\":\"082115806156\",\"alamat\":\"Ujung Harapan\"}]', '2021-02-23 19:35:01', '2021-02-23 19:40:18'),
('R06Qw', '082115806156', 1, 0, 165500, 'unpaid', NULL, NULL, '[{\"no\":\"wmgR0\",\"namaLengkap\":\"Ade Sherina\",\"noTelp\":\"082115806156\",\"alamat\":\"Indramayu\"}]', '2021-02-04 05:00:08', '2021-02-04 05:06:20'),
('UeV4c', '082115806156', 2, 4792, 76108, 'Selesai', '1611064122_848.jpg', '[{\"no\":\"WX0Ib\",\"jasaPengirim\":\"jne\",\"resi\":null,\"ongkir\":\"19.000\"},{\"no\":\"abzhd\",\"jasaPengirim\":\"pos\",\"resi\":null,\"ongkir\":\"21.000\"}]', '[{\"no\":\"WkzaR\",\"namaLengkap\":\"Ade Sherina\",\"noTelp\":\"082115806156\",\"alamat\":\"Indramayu\"},{\"no\":\"MhYKP\",\"namaLengkap\":\"Ade Sherina\",\"noTelp\":\"082115806156\",\"alamat\":\"Indramayu\"}]', '2021-01-19 06:46:28', '2021-01-19 06:48:29'),
('VixOg', '082115806156', 0, 0, 0, 'unverified', NULL, NULL, NULL, '2021-03-08 03:46:40', '2021-03-08 03:46:40');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_reedem`
--

CREATE TABLE `transaksi_reedem` (
  `id_transaksi` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reedem_id` bigint(20) UNSIGNED NOT NULL,
  `member_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksi_reedem`
--

INSERT INTO `transaksi_reedem` (`id_transaksi`, `reedem_id`, `member_id`, `status`, `created_at`, `updated_at`) VALUES
('0038256', 1, 1, 'Selesai', '2021-01-12 00:05:34', NULL),
('0179717', 6, 1, 'Belum Dikonfirmasi', '2021-01-16 07:53:49', NULL),
('0520222', 1, 1, 'Belum Dikonfirmasi', '2021-01-15 02:43:02', NULL),
('2175640', 1, 1, 'Belum Dikonfirmasi', '2021-01-15 02:45:56', NULL),
('3223220', 1, 1, 'Belum Dikonfirmasi', '2021-01-19 06:26:37', NULL),
('3466133', 1, 1, 'Belum Dikonfirmasi', '2021-01-16 07:41:59', NULL),
('3708092', 6, 1, 'Belum Dikonfirmasi', '2021-01-15 02:49:47', NULL),
('5473189', 1, 1, 'Belum Dikonfirmasi', '2021-01-19 06:51:55', NULL),
('6343566', 6, 1, 'Selesai', '2021-01-15 00:40:48', NULL),
('6349007', 1, 1, 'Belum Dikonfirmasi', '2021-02-16 07:11:24', NULL),
('8844777', 1, 2, 'Selesai', '2021-01-12 06:42:15', NULL),
('9186244', 1, 1, 'Selesai', '2021-01-15 00:36:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `no_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isVerified` tinyint(1) DEFAULT NULL,
  `nama_lengkap` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`no_telp`, `isVerified`, `nama_lengkap`, `email`, `alamat`, `jenis_kelamin`, `photo`, `password`, `created_at`, `updated_at`) VALUES
('081295491852', 1, 'nam do', 'namdo@gmail.com', 'kandang haur', NULL, NULL, '$2y$10$xZR/44iXfsDaqu3tFp5bg.9SdDhiq5J74udNDmd7S6GysXTQhDPq2', '2021-01-13 21:39:02', '2021-01-13 21:39:24'),
('082115806156', NULL, 'ade sherina', 'adesherin00@gmail.com', 'Jl Jendral Sudirman Indramayu', 'Perempuan', '803494a5a348dc9d2eb57dee9353bc82.jpg', '$2y$10$XA62TEt537j2JHtaoO/cg.fyWQcRDJXhCfaNr1ERn8eKTjxMB6KE2', '2020-12-14 23:35:20', '2020-12-14 23:35:20'),
('085839950597', 1, 'Bagu bragowo', 'bagubragowo3@gmail.com', 'Jalan sembung', NULL, NULL, '$2y$10$PFDBXEL44/9Hfo8wakHQDOrAMip9RTD/bAHM/DNzKigEEv9YlAQcq', '2021-01-19 06:43:49', '2021-01-19 06:44:25'),
('087822814574', 1, 'Monika Dedeh Wahyuningsih', 'monika@gmail.com', 'Indramayu', NULL, NULL, '$2y$10$ijjAEOmdkcRIFtHaJNYi2OOCHDX3OP60D8xR4f0irplpQxE6ut9VO', '2021-01-14 03:16:15', '2021-01-14 03:16:41'),
('087838384632', NULL, 'Yuni', 'yuni@gmail.com', 'Indramayu', NULL, NULL, '$2y$10$RFjx9l04NtrlynVb3D5QbOXrPU5VHwKoDm/V47aetWbza8x2V8b6W', '2020-12-15 02:52:58', '2020-12-15 02:52:58'),
('089668058996', 1, 'Indrawan Dwi Prasetyo', 'indrawandwiprasetyo@gmail.com', 'Sepanjang Jalan Kenangan', NULL, NULL, '$2y$10$telVvW8txV1l.WAjCRITLuWtKKveHcoi.Y6UTLClY1DzSH32w98Ai', '2021-01-19 06:17:02', '2021-01-19 06:17:27');

-- --------------------------------------------------------

--
-- Table structure for table `voucer`
--

CREATE TABLE `voucer` (
  `id_voucer` bigint(20) UNSIGNED NOT NULL,
  `nama_voucer` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_voucer` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diskon` double(15,2) NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expaired_voucer` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `voucer`
--

INSERT INTO `voucer` (`id_voucer`, `nama_voucer`, `kode_voucer`, `diskon`, `deskripsi`, `expaired_voucer`, `created_at`, `updated_at`) VALUES
(3, 'Awal Bulan', 'AB123', 8.00, 'Diskon gajian', '2020-12-16', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_produk` (`id_produk`),
  ADD KEY `no_telp` (`no_telp`),
  ADD KEY `detail_transaksi_ibfk_1` (`nota`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `no_telp` (`no_telp`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD PRIMARY KEY (`id_keranjang`),
  ADD KEY `keranjang_no_telp_foreign` (`no_telp`),
  ADD KEY `keranjang_id_produk_foreign` (`id_produk`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_member`),
  ADD KEY `member_no_telp_foreign` (`no_telp`);

--
-- Indexes for table `merk`
--
ALTER TABLE `merk`
  ADD PRIMARY KEY (`id_merk`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`),
  ADD KEY `kategori` (`kategori`),
  ADD KEY `merk` (`merk`);

--
-- Indexes for table `reedem_point`
--
ALTER TABLE `reedem_point`
  ADD PRIMARY KEY (`id_reedem`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`nota`),
  ADD KEY `no_telp` (`no_telp`),
  ADD KEY `no_telp_2` (`no_telp`);

--
-- Indexes for table `transaksi_reedem`
--
ALTER TABLE `transaksi_reedem`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `transaksi_reedem_reedem_id_foreign` (`reedem_id`),
  ADD KEY `transaksi_reedem_member_id_foreign` (`member_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`no_telp`);

--
-- Indexes for table `voucer`
--
ALTER TABLE `voucer`
  ADD PRIMARY KEY (`id_voucer`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `keranjang`
--
ALTER TABLE `keranjang`
  MODIFY `id_keranjang` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id_member` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `merk`
--
ALTER TABLE `merk`
  MODIFY `id_merk` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `reedem_point`
--
ALTER TABLE `reedem_point`
  MODIFY `id_reedem` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `voucer`
--
ALTER TABLE `voucer`
  MODIFY `id_voucer` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD CONSTRAINT `detail_transaksi_ibfk_1` FOREIGN KEY (`nota`) REFERENCES `transaksi` (`nota`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_transaksi_ibfk_2` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`),
  ADD CONSTRAINT `detail_transaksi_ibfk_4` FOREIGN KEY (`no_telp`) REFERENCES `users` (`no_telp`);

--
-- Constraints for table `favourites`
--
ALTER TABLE `favourites`
  ADD CONSTRAINT `favourites_ibfk_1` FOREIGN KEY (`no_telp`) REFERENCES `users` (`no_telp`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `favourites_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `produk` (`id_produk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD CONSTRAINT `keranjang_id_produk_foreign` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `keranjang_no_telp_foreign` FOREIGN KEY (`no_telp`) REFERENCES `users` (`no_telp`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `member`
--
ALTER TABLE `member`
  ADD CONSTRAINT `member_no_telp_foreign` FOREIGN KEY (`no_telp`) REFERENCES `users` (`no_telp`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `produk_ibfk_1` FOREIGN KEY (`kategori`) REFERENCES `kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `produk_ibfk_2` FOREIGN KEY (`merk`) REFERENCES `merk` (`id_merk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`no_telp`) REFERENCES `users` (`no_telp`);

--
-- Constraints for table `transaksi_reedem`
--
ALTER TABLE `transaksi_reedem`
  ADD CONSTRAINT `transaksi_reedem_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `member` (`id_member`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_reedem_reedem_id_foreign` FOREIGN KEY (`reedem_id`) REFERENCES `reedem_point` (`id_reedem`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
