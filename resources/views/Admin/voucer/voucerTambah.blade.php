@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Voucer</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Voucer</a></div>
        <div class="breadcrumb-item">Form Tambah Voucer</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('voucer.simpandata')}}" method="POST" enctype="multipart/form-data">
        @method('post')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Tambah Voucer</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Voucer</label>
                  <input type="text" class="form-control" name="nama_voucer">
                </div>
                <div class="form-group">
                  <label>Kode Voucer</label>
                  <input type="text" class="form-control" name="kode_voucer">
                </div>
                <div class="form-group">
                  <label>Diskon</label>
                  <input type="text" class="form-control" name="diskon">
                </div>
                <div class="form-group">
                  <label>Expaired Voucer</label>
                  <input type="date" class="form-control" name="expaired_voucer">
                </div>
                <div class="form-group"> 
                  <label>Deskripsi</label>
                 <textarea class="form-control" name="deskripsi" id="editor2"></textarea>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>      
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection

@push('script')
    <script>
      $(document).ready(function() {
          $('#bidang').select2();
      });

      CKEDITOR.replace( 'deskripsi' );
    </script>
@endpush
 