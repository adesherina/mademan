<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    @include('user.layouts.head')
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
         @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Cek Status Member</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Member</a></li>
                        <li class="breadcrumb-item active">Cek Status Member</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <div class="cart-box-main">
        <div class="container">
            <div class="row new-account-login">
                <div class="col-sm-12 col-lg-12 mb-12">
                    <div class="title-left">
                        <h3>Status Member</h3>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-lg-8 col-md-8 col-sm-12 col-12 mb-3">
                    <div class="card" style=" box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  transition: all 0.3s cubic-bezier(.25,.8,.25,1);">
                        <div class="card-body">
                            @if($member === null)
                              <div class="row">
                                <div class="col-12">
                                    <p class="h5"><b>
                                       Belum Ada Member
                                    </b></p>
                                    <hr class="text-dark mt-2">
                                </div>
                            </div> 
                            @else
                            <div class="row">
                                <div class="col-12">
                                    <p class="h5"><b>
                                        @if($member->status == "Aktif")
                                        {{$member->status}}
                                        @else($$member->status == "Menunggu Verifikasi")
                                        {{$member->status}}
                                        @endif
                                    </b></p>
                                    <hr class="text-dark mt-2">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-lg-4 col-md-4">
                                   <p><i class="fa fa-user mr-2" aria-hidden="true"></i>{{$member->nama_lengkap}}</p>
                                   <p><i class="fa fa-phone mr-2 mt-3" aria-hidden="true"></i>{{$member->no_telp}}</p>
                                </div>
                                <div class="col-12 col-lg-4 col-md-4">
                                   <p><i class="fa fa-envelope-o mr-2" aria-hidden="true"></i>{{$member->email}}</p>
                                </div>
                                <div class="col-lg-4 col-sm-4 col-12 col-md-4 text-center mt-3">
                                   <p class="h1" style="color: #ae8547"><b>{{$member->point}}</b></p>
                                   <p class="h3" style="color: #ae8547"><b>Point</b></p>
                                </div>
                                <div class="col-3">
                                    <hr class="text-secondary">
                                </div>
                                <div class="col-12">
                                    <p><i class="fa fa-clock-o mr-2 mt-1" aria-hidden="true"></i>13 Desember 2020</p>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-12 mt-5">
                                        <p class="h5 text-center">Undang Teman Anda dan Dapatkan 5 Point Gratis</p>
                                        <div class="referal-code mt-5">
                                            <input type="text" class="form-control d-inline" id="referal" style="width: 40%;" value="{{$member->referal_code}}" readonly>
                                            <button class="d-inline btn btn-primary" style="background-color: #ae8547 !important;border:none !important" onclick="copy_text()"><i class="fa fa-clone" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-12 mt-5">
                                        <img src="{{url('assets/user/images/refer.png')}}" class="float-right" style="width:300px;height:240px" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


       <!-- Start Instagram Feed  -->
       @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
    <script type="text/javascript">
        function copy_text() {
            document.getElementById("referal").select();
            document.execCommand("copy");
            alert("Kode Referal berhasil dicopy");
        }
    </script>
</body>

</html>