@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Produk</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Produk</a></div>
        <div class="breadcrumb-item">Form Update Produk</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('produk.update', $produk->id_produk)}}" method="POST" enctype="multipart/form-data">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Edit Produk</h4>
                <div class="card-header-action">
                  <a href="{{url('produk')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
                <div class="card-body">
                    <div class="card-body">
                    <div class="form-group">
                        <label>Brand</label>
                        @php
                        $merk = DB::table('merk')->get();    
                        @endphp
                          <select class="form-control selectric" name="merk">
                            <option value="{{$produk->id_merk}}" selected>{{$produk->nama_merk}}</option>
                            @foreach ($merk as $item)
                              <option value="{{$item->id_merk}}">{{$item->nama_merk}}</option>
                            @endforeach
                          </select>
                    </div>
                    <div class="form-group">
                      <label>Nama Produk</label>
                      <input type="text" class="form-control" name="nama_produk" value="{{$produk->nama_produk}}">
                    </div>
                    <div class="form-group">
                      <label >Kategori</label>
                      @php
                      $kategori = DB::table('kategori')->get();    
                      @endphp
                        <select class="form-control selectric" name="kategori">
                          <option value="{{$produk->id_kategori}}" selected>{{$produk->nama_kategori}}</option>
                          @foreach ($kategori as $item)
                            <option value="{{$item->id_kategori}}">{{$item->nama_kategori}}</option>
                          @endforeach
                        </select>
                  </div>
                    <div class="form-group">
                      <label>Harga</label>
                      <input type="text" class="form-control" name="harga" value="{{$produk->harga}}">
                    </div>
                    <div class="form-group">
                      <label>Stok</label>
                      <input type="text" class="form-control" name="stok" value="{{$produk->stok}}">
                    </div>
                    <div class="form-group"> 
                      <label>Deskripsi</label>
                    <textarea class="form-control" name="deskripsi">{{$produk->deskripsi}}</textarea>
                    </div>
                    <div class="form-group">
                      <label>Foto</label>
                      <input type="file" class="form-control" name="foto" >
                      <input type="hidden" class="form-control" name="oldPhoto" value="{{$produk->foto}}">
                    </div>
                    <div class="card-footer text-right">
                      <button class="btn btn-primary mr-1" type="submit">Submit</button>
                    </div>
                </div>
            </div> 
          </div>
      </form>
    </div>
  </section>
</div> 
@endsection

@push('script')
    <script>
      $(document).ready(function() {
          $('#bidang').select2();
      });

      CKEDITOR.replace( 'deskripsi' );
    </script>
@endpush
 