@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Transaksi Reedem Poin</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Transaksi Reedem Poin</a></div>
        <div class="breadcrumb-item">Transaksi Reedem Poin</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('transaksiReedem.updateProcess', $tsr->id_transaksi)}}" method="POST">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Edit Transaksi Reedem Poin</h4>
                <div class="card-header-action">
                  <a href="{{url('transaksiReedem')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Lengkap</label>
                  <input type="text" class="form-control" name="nama_lengkap" value="{{$tsr->nama_lengkap}}" disabled>
                </div>
                <div class="form-group">
                  <label>No Telepon</label>
                  <input type="text" class="form-control" name="no_telp" value="{{$tsr->no_telp}}" disabled>
                </div>
                <div class="form-group">
                  <label>Point yang dipunyai</label>
                  <input type="text" class="form-control" name="jumlah" value="{{$tsr->point_member}}" disabled>
                </div>
                <div class="form-group">
                  <label>Point Yang harus ditukar</label>
                  <input type="text" class="form-control" name="jumlah" value="{{$tsr->point_reedem}}" disabled>
                </div>
                <div class="form-group" >
                  <label>Status Pesanan</label>
                  <select class="form-control" name="status" >
                    <option value="{{$tsr->status_reedem}}" selected>{{$tsr->status_reedem}}</option>
                    <option value="Selesai">Selesai</option>
                  </select>
                </div>
                <div class="card-footer text-right">
                  <input type="hidden" name="no_telp" value="{{$tsr->no_telp}}">
                  <input type="hidden" name="point_reedem" value="{{$tsr->point_reedem}}">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>   
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 