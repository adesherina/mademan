@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
   <section class="section">
      <div class="section-header">
        <h1>Brand Produk</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Brand Produk</a></div>
          <div class="breadcrumb-item">Produk</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Brand Produk</h4>
            <div class="card-header-action">
              <a href="{{route('merk.tambahdata')}}" class="btn btn-success btn-lg">
              Add
              </a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="tabel-data" class="table table-bordered table-md">
                <thead>
                  <tr>
                    <th class="text-center">
                       No
                    </th>
                    <th>Nama Brand</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach ($merk as $merk)
                 <tr>
                   <td class="text-center">
                     {{ $loop->iteration }}
                    </td>
                    <td>{{$merk->nama_merk}}</td>
                    <td>
                      <a href="{{route('merk.editdata', $merk->id_merk)}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i>
                      </a>
                      <form action="{{route('merk.deletedata', $merk->id_merk)}}" id="delete{{$merk->id_merk}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                      @method('delete')
                      <button class="btn btn-icon icon-left btn-danger"><i class="fas fa-trash-alt"></i></button>
                      @csrf
                      </form>
                    </td>
                 </tr>
                @endforeach
                </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</section>
</div> 
@endsection