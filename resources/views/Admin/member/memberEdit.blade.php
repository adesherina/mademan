@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Member</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Member</a></div>
        <div class="breadcrumb-item">Form Update Member</div>
      </div>
    </div>
    
    <div class="card-body">
      <form action="{{route('member.updateProcess', $member->id_member)}}" method="POST">
        @method('patch')
        @csrf
        <div class="row">
          <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
              <div class="card-header">
                <h4>Edit Member</h4>
                <div class="card-header-action">
                  <a href="{{url('datamember')}}" class="btn btn-success btn-lg">
                    kembali
                  </a>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Member</label>
                  <input type="text" class="form-control" name="nama_lengkap" value="{{$member->nama_lengkap}}" disabled>
                </div>
                <div class="form-group">
                  <label>No Telepon</label>
                  <input type="text" class="form-control" name="no_telp" value="{{$member->no_telp}}" disabled>
                </div>
                <div class="form-group">
                  <label>Point</label>
                  <input type="text" class="form-control" name="point" value="{{$member->point}}">
                </div>
                <div class="form-group" >
                  <label>Status</label>
                  <select class="form-control" name="status" >
                    <option value="{{$member->status}}" selected>{{$member->status}}</option>
                    <option value="Menunggu Verifikasi">Menunggu Verifikasi</option>
                    <option value="Aktif">Aktif</option>
                    <option value="Tidak Aktif">Tidak Aktif</option>
                  </select>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>   
              </div>
            </div>
          </div> 
         </div>
      </form>
    </div>
  </section>
</div> 
@endsection
 