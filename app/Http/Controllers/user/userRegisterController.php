<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\SendCode;
use Twilio\Rest\Client;
use App\User;
use Illuminate\Support\Facades\Validator;

class userRegisterController extends Controller
{
    //tampil halaman register
    public function index(){
        return view('user.register');
    }
    //proses register
    public function addProcess(Request $request)
    {
        $rules = [
            'nama_lengkap' => 'required|min:5|max:100',
            'no_telp' => 'required|min:10|unique:users',
            'email' => 'required|email|unique:users',
            'alamat' => 'required|min:5',
            'confirmPassword' => 'required',
            'password' => 'required|min:8'
        ];
 
        $messages = [
            'nama_lengkap.required'  => 'Nama Lengkap wajib diisi.',
            'no_telp.required'         => 'Nomor telepon wajib diisi.',
            'no_telp.unique'           => 'Nomor telepon sudah terdaftar.',
            'alamat.required'      => 'Alamat wajib diisi.',
            'password.required'      => 'Password wajib diisi.',
            'password.min'           => 'Password minimal diisi dengan 8 karakter.',
            'confirmPassword.required'  => 'Password Konfirmasi wajib diisi.',
            'email.required'         => 'Email wajib diisi.',
            'email.email'            => 'Email tidak valid.',
            'email.unique'           => 'Email sudah terdaftar.',
        ];
 
        $validator = Validator::make($request->all(), $rules, $messages);
         
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $no_telp  = $request->no_telp;
        // kadang ada penulisan no hp 0811 239 345
        $no_telp = str_replace(" ","",$no_telp);
        // kadang ada penulisan no hp (0274) 778787
        $no_telp = str_replace("(","",$no_telp);
        // kadang ada penulisan no hp (0274) 778787
        $no_telp = str_replace(")","",$no_telp);
        // kadang ada penulisan no hp 0811.239.345
        $no_telp = str_replace(".","",$no_telp);

        // cek apakah no hp mengandung karakter + dan 0-9
        if(!preg_match('/[^+0-9]/',trim($no_telp))){
            // cek apakah no hp karakter 1-3 adalah +62
            if(substr(trim($no_telp), 0, 3)=='+62'){
                $hp = trim($no_telp);
            }
            // cek apakah no hp karakter 1 adalah 0
            elseif(substr(trim($no_telp), 0, 1)=='0'){
                $hp = '+62'.substr(trim($no_telp), 1);
            }
        }
       $kode_referal   = $request->kode_referal;

        $timestamps = true;
        //query builder insert
        if($request->password != $request->confirmPassword){
             return redirect('user/register')->with('statusFailed', 'Password Konfirmasi Tidak Sama');
        }else{
        $token = getenv("TWILIO_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
        $twilio->verify->v2->services($twilio_verify_sid)
            ->verifications
            ->create($hp, "sms");
        $user = User::create(
            [
                'no_telp' => $no_telp,
                'nama_lengkap' => $request->nama_lengkap,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'password' => Hash::make($request->password),
            ]
        );
        
        //Redirect dengan status 
        return view('user.verifikasiOtp', compact('no_telp','kode_referal'))->with('statusRegister', 'Register berhasil silahkan bayar di kasir dengan memberikan nomor telepon anda');
        }
    }

    
}
