<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class TransaksiReedemController extends Controller
{
    //tampil halaman transaksi
    public function index()
    {
        $tsr = DB::table('transaksi_reedem')
        ->join('reedem_point', 'reedem_point.id_reedem', '=', 'transaksi_reedem.reedem_id')
        ->join('member', 'member.id_member', '=', 'transaksi_reedem.member_id')
        ->join('users', 'users.no_telp', '=', 'member.no_telp')
        ->select('reedem_point.*', 'member.*','transaksi_reedem.*','users.*', 'reedem_point.point as point_reedem','member.point as point_member')
        ->get();
        return view('Admin.transaksiReedem.list', compact('tsr'));
    }

    //tampil edit data
    public function update($id_transaksi){
        $tsr = DB::table('transaksi_reedem')
        ->join('reedem_point', 'reedem_point.id_reedem', '=', 'transaksi_reedem.reedem_id')
        ->join('member', 'member.id_member', '=', 'transaksi_reedem.member_id')
        ->join('users', 'users.no_telp', '=', 'member.no_telp')
        ->where('id_transaksi', $id_transaksi)
        ->select('reedem_point.*', 'member.*','transaksi_reedem.*','users.*', 'reedem_point.point as point_reedem','member.point as point_member', 'transaksi_reedem.status as status_reedem')
        ->first();
        return view('Admin.transaksiReedem.edit', ['tsr'=> $tsr]);

    }

    //proses update
    public function updateProcess(Request $request, $id_transaksi)
    {
        DB::table('transaksi_reedem')->where('id_transaksi', $id_transaksi)->update([
                'status' => $request->status, 
            ]);
        if($request->status === "Selesai"){
            $member = DB::table('member')->where('no_telp', $request->no_telp)->first();
                if($member != null ){
                    DB::table('member')->where('no_telp', $request->no_telp)->update([
                    'point' => (int)$member->point - (int)$request->point_reedem, 
                ]);
            }
        }

        // dd($request->all());
        return redirect('transaksiReedem')->with('status', 'Data Berhasil Diedit');

    }

    //detail data
    // public function detail($nota){
    //     $ts = DB::table('transaksi')->where('nota', $nota)
    //     ->join('users', 'users.no_telp', '=', 'transaksi.no_telp')
    //     ->first();
    //     return view('Admin.transaksi.detail', ['ts'=> $ts]);

    // }

    //hapus 
    public function delete($id)
    {
        DB::table('transaksi_reedem')->where('id_transaksi', $id)->delete();
        return redirect('transaksiReedem')->with('status', 'Data Berhasil Dihapus');
    }

}
