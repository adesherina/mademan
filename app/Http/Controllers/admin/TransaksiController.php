<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransaksiController extends Controller
{
    //tampil halaman transaksi
    public function index()
    {
        $ts = DB::table('transaksi')
        ->join('users', 'users.no_telp', '=', 'transaksi.no_telp')
        ->get();
        return view('Admin.transaksi.transaksi', compact('ts'));
    }

    //edit data
    public function update($nota){
        $ts = DB::table('transaksi')
        ->where('nota', $nota)
        ->join('users', 'users.no_telp', '=', 'transaksi.no_telp')
        ->first();
        return view('Admin.transaksi.Edit', ['ts'=> $ts]);

    }

    //update
    public function updateProcess(Request $request, $nota)
    {
        $data = DB::table('transaksi')->where('nota', $nota)->first();
        if($data->jasa_kirim != null){
            $datakurir = json_decode($data->jasa_kirim, true);

            foreach ($datakurir as $key => $d) {
                // Perbarui data kedua
                if ($d['no'] === $request->no_kurir) {
                    $datakurir[$key]['resi'] = $request->resi;
                }
            }
            DB::table('transaksi')->where('nota', $nota)->update([
                'status_pesanan' => $request->status_pesanan, 
                'jasa_kirim' => json_encode($datakurir)
            ]);
        }else{ 
            DB::table('transaksi')->where('nota', $nota)->update([
                'status_pesanan' => $request->status_pesanan, 
            ]);
        }

        if($request->status_pesanan === "Selesai"){
            $member = DB::table('member')->where('no_telp', $data->no_telp)->first();
                if($member != null ){
                    DB::table('member')->where('no_telp', $data->no_telp)->update([
                    'point' => (int)$member->point + 5, 
                ]);
            }
        }
        
        return redirect('transaksi')->with('status', 'Data Berhasil Diedit');

    }

    //detail data
    public function detail($nota){
        $ts = DB::table('transaksi')->where('nota', $nota)
        ->join('users', 'users.no_telp', '=', 'transaksi.no_telp')
        ->first();
        return view('Admin.transaksi.detail', ['ts'=> $ts]);

    }

    //hapus 
    public function delete($id)
    {
        DB::table('transaksi')->where('nota', $id)->delete();
        return redirect('transaksi')->with('status', 'Data Berhasil Dihapus');
    }
}
