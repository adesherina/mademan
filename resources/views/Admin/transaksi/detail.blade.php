@extends('Admin.master')
@section('title', 'Mademan Barbershop')


@section('content')
<div class="main-content">
  <section class="section">
      <div class="section-header">
        <h1>Detail Transaksi Penjualan</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Detail Transaksi Penjualan</a></div>
          <div class="breadcrumb-item">Transaksi Penjualan</div>
        </div>
      </div>
      <div class="section-body">
        <!-- Alert Tambah Data -->
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <div class="card card-primary">
          <div class="card-header">
            <h4>Detail Transaksi Penjualan</h4>
            <div class="card-header-action">
              <a href="{{url('transaksi')}}" class="btn btn-success btn-lg">
              kembali
            </a>
            </div>
          </div>

            <div class="col-12 mt-3 col-md-12 col-lg-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                      <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Nama Lengkap</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$ts->nama_lengkap}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">No Telepon</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$ts->no_telp}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">QTY</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> {{$ts->jumlah}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Total Bayar</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               <p class="card-text text-primary"> Rp.  {{number_format($ts->total,'0','.','.')}}</p>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Data Pembeli</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                                <?php   
                                  $pembeli = $ts->data_pembeli;
                                  $data_pembeli = json_decode($pembeli, true); 
                                ?>
                                @if($data_pembeli !== null)
                                    @foreach ($data_pembeli as $key => $d)
                                    <div class="row mb-3">
                                        <div class="col-lg-3 col-md-2 col-sm-12 col-12">
                                            <p class="card-text text-primary">Nama Lengkap</p>
                                        </div>
                                        <div class="col-1">
                                          :
                                        </div>
                                        <div class="col-lg-3 col-md-2 col-sm-12 col-12">
                                          <p class="card-text text-primary"> {{$d['namaLengkap']}}</p>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-lg-3 col-md-2 col-sm-12 col-12">
                                            <p class="card-text text-primary">Nomor Telepon</p>
                                        </div>
                                        <div class="col-1">
                                          :
                                        </div>
                                        <div class="col-lg-3 col-md-2 col-sm-12 col-12">
                                          <p class="card-text text-primary"> {{$d['noTelp']}}</p>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-lg-3 col-md-2 col-sm-12 col-12">
                                            <p class="card-text text-primary">Alamat</p>
                                        </div>
                                        <div class="col-1">
                                          :
                                        </div>
                                        <div class="col-lg-3 col-md-2 col-sm-12 col-12">
                                          <p class="card-text text-primary"> {{$d['alamat']}}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Jasa Kirim</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                                <?php   
                                  $jasa_kirim = $ts->jasa_kirim;
                                  $jasa_kirim = json_decode($jasa_kirim, true); 
                                ?>
                                @if($jasa_kirim !== null)
                                    @foreach ($jasa_kirim as $key => $d)
                                    <div class="row mb-3">
                                        <div class="col-lg-3 col-md-2 col-sm-12 col-12">
                                            <p class="card-text text-primary">Jasa Kirim</p>
                                        </div>
                                        <div class="col-1">
                                          :
                                        </div>
                                        <div class="col-lg-3 col-md-2 col-sm-12 col-12">
                                          <p class="card-text text-primary"> {{$d['jasaPengirim']}}</p>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-lg-3 col-md-2 col-sm-12 col-12">
                                            <p class="card-text text-primary">Nomor Resi</p>
                                        </div>
                                        <div class="col-1">
                                          :
                                        </div>
                                        <div class="col-lg-3 col-md-2 col-sm-12 col-12">
                                          <p class="card-text text-primary"> {{$d['resi']}}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-3 col-md-2 col-sm-12 col-12">
                                <p class="card-text text-primary">Bukti Transfer</p>
                            </div>
                            <div class="col-1">
                              :
                            </div>
                            <div class="col-8">
                               @if($ts->bukti_tf != null) <img src="{{url('/bukti-tf/'.$ts->bukti_tf)}}" alt="" style="width: 300px !important; height: 300px !important; "> @else Tidak Ada @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
      </div>
  </section>
</div> 
@endsection