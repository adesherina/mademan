<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->string('nota', 15)->primary();
            $table->string('no_telp', 15);
            $table->integer('jumlah');
            $table->integer('diskon');
            $table->integer('total');
            $table->string('status_pesanan', 50)->nullable();
            $table->string('bukti_tf', 255)->nullable();
            $table->json('jasa_kirim')->nullable();
            $table->json('data_pembeli')->nullable();
            $table->timestamps();

            $table->foreign('no_telp')->references('no_telp')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
