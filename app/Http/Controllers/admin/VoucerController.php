<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VoucerController extends Controller
{
    //tampil data
    public function index(){
        $voucer = DB::table('voucer')->get();
        return view('Admin.voucer.voucer', ['voucer'=> $voucer]);
    }

    //tambah data
    public function tambah(){
        return view('Admin.voucer.voucerTambah');
    }

    //simpan data
    public function simpan(Request $request){
        DB::table('voucer')->insert(
                [
                    'nama_voucer'=>$request->nama_voucer,
                    'kode_voucer' => $request->kode_voucer,
                    'diskon' => $request->diskon,
                    'expaired_voucer' => $request->expaired_voucer,
                    'deskripsi' => $request->deskripsi,
                ]);
        return redirect('voucer')->with('status', 'Data Berhasil Ditambahkan');
    }

    //edit data
    public function edit($id){
        $voucer = DB::table('voucer')->where('id_voucer', $id)->first();
        return view('Admin.voucer.voucerEdit', ['voucer'=> $voucer]);

    }

    //update
    public function update(Request $request, $id)
    {
        DB::table('voucer')->where('id_voucer', $id)->update([
            'nama_voucer'=>$request->nama_voucer,
            'kode_voucer' => $request->kode_voucer,
            'diskon' => $request->diskon,
            'expaired_voucer' => $request->expaired_voucer,
            'deskripsi' => $request->deskripsi,
        ]);
        
        return redirect('voucer')->with('status', 'Data Berhasil Diedit');

    }

    //detail data
    public function detail($id){
        $voucer = DB::table('voucer')->where('id_voucer', $id)->first();
        return view('Admin.voucer.detail', ['voucer'=> $voucer]);

    }

    //hapus data
    public function delete($id)
    {
        DB::table('voucer')->where('id_voucer', $id)->delete();
        
        return redirect('voucer')->with('status', 'Data Berhasil Dihapus');
    }
}
