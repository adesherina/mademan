
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    @include('user.layouts.head')
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
        @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Produk Favorit Saya</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Produk Favorit Saya</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start Shop Page  -->
    <div class="shop-box-inner">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-sm-12 col-xs-12 shop-content-right">
                    <div class="right-product-box">
                        <div class="product-item-filter row">
                            <div class="col-12 col-sm-8 text-center text-sm-left">
                                <div class="toolbar-sorter-right">
                                    <span>Produk Favorit </span>
                                </div>
                            </div>
                            <div class="col-12 col-sm-4 text-center text-sm-right">
                                <ul class="nav nav-tabs ml-auto">
                                    <li>
                                        <a class="nav-link active" href="#grid-view" data-toggle="tab"> <i class="fa fa-th"></i> </a>
                                    </li>
                                    <li>
                                        <a class="nav-link" href="#list-view" data-toggle="tab"> <i class="fa fa-list-ul"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                        
                        <div class="row product-categorie-box">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade show active" id="grid-view"> 
                                    
                                    <div class="row">
                                        @foreach($favourite as $item)
                                        <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 mt-4">
                                            <div class="products-single fix">
                                                <div class="box-img-hover">
                                                    <div class="type-lb">
                                                        <p class="new">New</p>
                                                    </div>
                                                    {{-- @php
                                                    $produk = DB::table('produk')->get();    
                                                    @endphp --}}
                                                    <img src="{{url('/foto-produk/'.$item->productRef->foto)}}" style="width:264px; height:264px" class="img-fluid" alt="Image">
                                                    <div class="mask-icon">
                                                        <ul>
                                                            <li><a href="{{url('keranjang/detail/'. $item->productRef->id_produk)}}" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                                            <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-sync-alt"></i></a></li>
                                                            <li><div id="favourite_product_{{$item->productRef->id_produk}}">
                                                                        @php
                                                                            $favourite = DB::table('favourites')->where('product_id',$item->productRef->id_produk )->where('no_telp',Session::get('no_telp') )->first();
                                                                        @endphp
                                                                        @if ($favourite === null)
                                                                            <a href="#" onclick="addFavourite('{{$item->productRef->id_produk}}')" id="favourite_{{$item->productRef->id_produk}}" data-toggle="tooltip" data-placement="right" data-id-user="{{Session::get('no_telp')}}" title="Add to Wishlist"><i class="far fa-heart"></i></a>
                                                                        @else
                                                                            <a href="#" onclick="removeFavourite('{{$item->productRef->id_produk}}')" id="favourite_{{$item->productRef->id_produk}}" data-toggle="tooltip" data-placement="right" data-id-user="{{Session::get('no_telp')}}" title="Remove from Wishlist"><i class="fas fa-heart"></i></a>
                                                                        @endif
                                                                    
                                                                    </div>
                                                                </li>
                                                        </ul>
                                                        <a class="cart tambah-keranjang" onclick="addKeranjang({{$item->productRef->id_produk}})" id="keranjang_btn_{{$item->productRef->id_produk}}" href="#" data-id-user="{{Session::get('no_telp')}}" data-produkid="{{$item->productRef->id_produk}}" data-produkharga="{{$item->productRef->harga}}" data-produkqty="1" data-produkcsrf="{{Session::token()}}">Add to Cart</a>
                                                    </div>
                                                </div>
                                                <div class="why-text"  style="height: 120px;">
                                                    <h4>{{$item->productRef->nama_produk}}</h4>
                                                    <h5>Rp.  {{number_format($item->productRef->harga,'0','.','.')}}</h5>
                                                </div>
                                            </div>
                                        </div>
                                          @endforeach
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="list-view">
                                    <div class="list-view-box">
                                        <?php
                                            use Illuminate\Support\Facades\DB;
                                            $list_produk = $favourite = App\Model\Favourite::with('productRef')->with('userRef')->where('no_telp',Session::get('no_telp'))->get();
                                        ?>
                                        @foreach($list_produk as $item)
                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 mt-5">
                                                <div class="products-single fix">
                                                    <div class="box-img-hover">
                                                        <div class="type-lb">
                                                            <p class="new">New</p>
                                                        </div>
                                                        <img src="{{url('/foto-produk/'.$item->productRef->foto)}}" class="img-fluid" alt="Image">
                                                        <div class="mask-icon">
                                                            <ul>
                                                                <li><a href="{{route('detail.keranjang', $item->productRef->id_produk)}}" data-toggle="tooltip" data-placement="right" title="View"><i class="fas fa-eye"></i></a></li>
                                                                <li><a href="#" data-toggle="tooltip" data-placement="right" title="Compare"><i class="fas fa-sync-alt"></i></a></li>
                                                                <li><div id="favourite_product_{{$item->productRef->id_produk}}">
                                                                        @php
                                                                            $favourite = DB::table('favourites')->where('product_id',$item->productRef->id_produk )->where('no_telp',Session::get('no_telp') )->first();
                                                                        @endphp
                                                                        @if ($favourite === null)
                                                                            <a href="#" onclick="addFavourite('{{$item->productRef->id_produk}}')" id="favourite_{{$item->productRef->id_produk}}" data-toggle="tooltip" data-placement="right" data-id-user="{{Session::get('no_telp')}}" title="Add to Wishlist"><i class="far fa-heart"></i></a>
                                                                        @else
                                                                            <a href="#" onclick="removeFavourite('{{$item->productRef->id_produk}}')" id="favourite_{{$item->productRef->id_produk}}" data-toggle="tooltip" data-placement="right" data-id-user="{{Session::get('no_telp')}}" title="Remove from Wishlist"><i class="fas fa-heart"></i></a>
                                                                        @endif
                                                                    
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-8 col-xl-8">
                                                <div class="why-text full-width single-produk" style="height: 315px;">
                                                    <h4>{{$item->productRef->nama_produk}}</h4>
                                                    <h5 class="mb-3">Rp. {{$item->productRef->harga}}</h5>
                                                    <p>{{\Illuminate\Support\Str::words($item->productRef->deskripsi, 20,'....')}}</p>
                                                    <input type="hidden" value="{{$item->productRef->harga }}" name="harga" id="harga_input_'{{$item->productRef->id_produk}}"  />
                                                    <a class="btn hvr-hover" href="#" onclick="addKeranjangDetail({{$item->productRef->id_produk}})" id="add_keranjang_detail_{{$item->productRef->id_produk}}" data-id-user="{{Session::get('no_telp')}}" data-produk-id="{{$item->productRef->id_produk}}" data-produkharga="{{$item->productRef->harga}}" data-produkqty="1" data-produkcsrf="{{Session::token()}}">Add to Cart</a>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Shop Page -->

    <!-- Start Instagram Feed  -->
    @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')

    <script>
         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function addFavourite(product_id){
            event.preventDefault();
            var url = "{{route('favourite.add')}}";
            var no_telp = $('#favourite_'+product_id).data('id-user') || "undefine";
            if(no_telp === "undefine"){
                swal({
                    title: "Opps!",
                    text: "Anda Harus login Terlebih Dahulu!",
                    icon: "error",
                 });
            }else{
               console.log(no_telp);

               $.ajax(
                {
                    url: url,
                    type: 'post',
                    data: {
                        product_id: product_id,
                        no_telp: no_telp
                    },
                    success: function (data){
                        swal({
                                title: "Ditambahkan Ke favourite.",
                                icon: "success",
                            });
                        document.getElementById('favourite_product_'+product_id).innerHTML = data.html;
                      
                    },
                    error: function(reject) {
                        console.log(reject);
                    }
                });
            }
        }

        function removeFavourite(product_id){
            event.preventDefault();
            var url = "{{route('favourite.remove')}}";
            var no_telp = $('#favourite_'+product_id).data('id-user') || "undefine";
            if(no_telp === "undefine"){
                swal({
                    title: "Opps!",
                    text: "Anda Harus login Terlebih Dahulu!",
                    icon: "error",
                 });
            }else{
               console.log(no_telp);

               $.ajax(
                {
                    url: url,
                    type: 'post',
                    data: {
                        product_id: product_id,
                        no_telp: no_telp
                    },
                    success: function (data){
                        swal({
                                title: "Dihapus dari favourite.",
                                icon: "success",
                            });
                        document.getElementById('favourite_product_'+product_id).innerHTML = data.html;
                        location.replace("{{ route('favourite.index') }}");
                      
                    },
                    error: function(reject) {
                        console.log(reject);
                    }
                });
            }
        }

        function addKeranjang(id_produk){
            event.preventDefault();
            let harga = $('#keranjang_btn_'+id_produk).data('produkharga');
            let qty = $('#keranjang_btn_'+id_produk).data('produkqty');
            let csrf = $('#keranjang_btn_'+id_produk).data('produkcsrf');
            var no_telp = $('#keranjang_btn_'+id_produk).data("id-user");
            
            if(no_telp != null){
                $.ajax({
                    url: "keranjang/add", //harus sesuai url di buat di route
                    type: "POST",
                    data: {
                        _token: csrf,
                        id_produk: id_produk,
                        qty: qty,
                        harga: harga,
                    },
                    cache: false,
                    success: function (dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        if (dataResult.statusCode == 200) {
                            swal({
                                title: "Produk telah di tambahkan ke keranjang",
                                icon: "success",
                                type: "success",
                            });
                        } else if (dataResult.statusCode == 201) {
                            alert("Error occured !");
                        }
                    },
                    error: function (error) {
                        swal({
                            title: "Opps!",
                            text: "Anda Harus login Terlebih Dahulu!",
                            icon: "error",
                        });
                    },
                });
            }else{
                swal({
                            title: "Opps!",
                            text: "Anda Harus login Terlebih Dahulu!",
                            icon: "error",
                        });
            }
        }

        // search by button keyword
        $('#btn-search').click(function(){
            let keyword = $('#keyword-search').val(); // get keyword
            let brand = $("input[name='brand']:checked").val(); //get radio brand value
            let categories = $("input[name='categories']:checked").val(); //get radio categories value
            let sorting = $('#sort-product').val(); // get sorting value
            let url = "{{url('product/search')}}";
            let url_list = "{{route('produk.seacrh.list')}}";

             if (keyword != "") {
                    $.ajax({
                        url: url, //harus sesuai url di buat di route
                        type: "POST",
                        data: {
                            sorting : sorting,
                            keyword : keyword,
                            categories: categories,
                            brand: brand,
                        },
                        cache: false,
                        success: function (dataResult) {
                            document.getElementById('grid-view').innerHTML = dataResult;
                        },
                        error: function (error) {
                        },
                    });

                    $.ajax({
                        url: url_list, //harus sesuai url di buat di route
                        type: "POST",
                        data: {
                            sorting : sorting,
                            keyword : keyword,
                            categories: categories,
                            brand: brand,
                        },
                        cache: false,
                        success: function (dataResults) {
                            document.getElementById('list-view').innerHTML = dataResults;
                        },
                        error: function (error) {
                        },
                    });
        } else {
            alert("Please fill all the field !");
        }
        });

        //tambah keranjang
            function addKeranjangDetail(id){
                event.preventDefault();
                
                let harga = $('#harga_input_'+id).val();
                var qty = $('#add_keranjang_detail'+id).data("produkqty");
                var id_user = $('#add_keranjang_detail'+id).data("id-user");
                var csrf = $('#add_keranjang_detail'+id).data("produkcsrf");
                //console.log(harga,qty);

                if (id_user != "") {
                    if (id != "") {
                        $.ajax({
                            url: "keranjang/add", //harus sesuai url di buat di route
                            type: "POST",
                            data: {
                                _token: csrf,
                                id_produk: id,
                                qty: '1',
                                harga: harga,
                            },
                            cache: false,
                            success: function (dataResult) {
                                var dataResult = JSON.parse(dataResult);
                                //console.log(dataResult.produk, dataResult.keranjang);
                                if (dataResult.statusCode == 200) {
                                    swal({
                                        title: "Produk telah di tambahkan ke keranjang",
                                        icon: "success",
                                        type: "success",
                                    });
                                } else if (dataResult.statusCode == 201) {
                                    alert("Error occured !");
                                }
                            },
                            error: function (error) {
                                //console.log(error);
                                swal({
                                    title: "Opps!",
                                    text: "Anda Harus login Terlebih Dahulu!",
                                    icon: "error",
                                });
                            },
                        });
                    } else {
                        alert("Please fill all the field !");
                    }
                } else {
                    swal({
                        icon: "error",
                        title: "Oops...",
                        text: "Anda Harus Login Terlebih Dahulu",
                    });
                }
            };
        
        // seacrh by categories radio
        $('.categories-product').each(function(){
            $(this).change(function(){
                  
                
                let keyword = $('#keyword-search').val() ;
                let brand = $("input[name='brand']:checked").val();
                let categories = $(this).val();
                let sorting = $('#sort-product').val();
                let url = "{{url('product/search')}}";
                let url_list = "{{route('produk.seacrh.list')}}";
                if (brand != "") {
                        $.ajax({
                            url: url, //harus sesuai url di buat di route
                            type: "POST",
                            data: {
                                sorting : sorting,
                                keyword : keyword,
                                categories: categories,
                                brand: brand,
                            },
                            cache: false,
                            success: function (dataResult) {
                                document.getElementById('grid-view').innerHTML = dataResult;
                            },
                            error: function (error) {
                                //console.log(error);
                            },
                        });

                        $.ajax({
                        url: url_list, //harus sesuai url di buat di route
                        type: "POST",
                        data: {
                            sorting : sorting,
                            keyword : keyword,
                            categories: categories,
                            brand: brand,
                        },
                        cache: false,
                        success: function (dataResults) {
                            document.getElementById('list-view').innerHTML = dataResults;
                        },
                        error: function (error) {
                            //console.log(error);
                        },
                    });
                } else {
                    alert("Please fill all the field !");
                }
            })
        })

        // search by brand radio
        $('.brand-product').each(function(){
            $(this).change(function(){
                let keyword = $('#keyword-search').val() ;
                let brand = $(this).val();
                let categories = $("input[name='categories']:checked").val();
                let sorting = $('#sort-product').val();
                let url = "{{url('product/search')}}";
                let url_list = "{{route('produk.seacrh.list')}}";

                if (brand != "") {
                        $.ajax({
                            url: url, //harus sesuai url di buat di route
                            type: "POST",
                            data: {
                                sorting : sorting,
                                keyword : keyword,
                                categories: categories,
                                brand: brand,
                            },
                            cache: false,
                            success: function (dataResult) {
                                document.getElementById('grid-view').innerHTML = dataResult;
                            },
                            error: function (error) {
                            },
                        });

                        $.ajax({
                        url: url_list, //harus sesuai url di buat di route
                        type: "POST",
                        data: {
                            sorting : sorting,
                            keyword : keyword,
                            categories: categories,
                            brand: brand,
                        },
                        cache: false,
                        success: function (dataResults) {
                            //console.log(dataResults);
                            document.getElementById('list-view').innerHTML = dataResults;
                        },
                        error: function (error) {
                            //console.log(error);
                        },
                    });
                } else {
                    alert("Please fill all the field !");
                }
            })
        })

        $('#sort-product').change(function(){
            let sorting = $(this).val();
            let keyword = $('#keyword-search').val() ;
            let brand = $("input[name='brand']:checked").val();
            let categories = $("input[name='categories']:checked").val();
            let url_list = "{{route('produk.seacrh.list')}}";
           
            let url = "{{url('product/search')}}";
                if (sorting != "") {
                        $.ajax({
                            url: url, //harus sesuai url di buat di route
                            type: "POST",
                            data: {
                                sorting:sorting,
                                keyword : keyword,
                                categories: categories,
                                brand: brand,
                            },
                            cache: false,
                            success: function (dataResult) {
                                document.getElementById('grid-view').innerHTML = dataResult;
                            },
                            error: function (error) {
                            },
                        });

                        $.ajax({
                        url: url_list, //harus sesuai url di buat di route
                        type: "POST",
                        data: {
                            sorting : sorting,
                            keyword : keyword,
                            categories: categories,
                            brand: brand,
                        },
                        cache: false,
                        success: function (dataResults) {
                            // console.log(dataResults);
                            document.getElementById('list-view').innerHTML = dataResults;
                        },
                        error: function (error) {
                            // console.log(error);
                        },
                    });
                } else {
                    alert("Please fill all the field !");
                }
        })
    </script>
</body>

</html>