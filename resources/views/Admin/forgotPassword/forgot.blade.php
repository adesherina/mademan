<!DOCTYPE html>
<html lang="en">
<head>
@include('Admin.layouts.header')
</head>

<body>
    <div id="app">
        <section class="section">
        <div class="container mt-5">
            <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                <div class="login-brand mt-5">
                    Forgot Password
                </div>

                <div class="card card-primary">
                <div class="card-header"><h4>Forgot Password</h4></div>

                <div class="card-body">
                    <form action="{{url('admin/forgot/pw')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('patch')
                        <input type="text" class="form-control" name="no_telp" value="{{$no_telp}}" hidden> 
                        <div class="form-group">
                            <label for="no_telp">Password Baru</label>
                            <input  type="password" class="form-control" name="newPassword" tabindex="1" placeholder="Masukan Nomor Telepon Anda" required autofocus >
                        </div>
                        <div class="form-group">
                            <label for="no_telp">Konfirmasi Password</label>
                            <input  type="password" class="form-control" name="confirmPassword" tabindex="1" placeholder="Masukan Nomor Telepon Anda" required autofocus >
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                            Forgort Passwords
                            </button>
                        </div>
                    </form>
                </div>
                </div>
                <div class="simple-footer">
                Copyright &copy; Stisla 2018
                </div>
            </div>
            </div>
        </div>
        </section>
    </div>
   @include('Admin.layouts.js')
</body>
</html>