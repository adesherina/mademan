<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class userMemberController extends Controller
{

    //tampil status member
    public function status(){
        $member = DB::table('member')
        ->join('users', 'users.no_telp', '=', 'member.no_telp')
        ->where('users.no_telp', Session::get('no_telp'))
        ->first();
        return view('user.statusMember', ['member'=> $member]);
    }

    //tampil reedem point
    public function reedemPoint(){
        $reedem = DB::table('reedem_point')->get();
        return view('user.member.reedemPoint', compact('reedem'));
    }

    public function generateRandomId($length = 7) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    //proses tambah reedem transaksi
    public function addProcess(Request $request, $reedem_id)
    {
        $member = DB::table('member')->where('no_telp',Session::get('no_telp'))->first();
        if($member != null){
            $reedem = DB::table('transaksi_reedem')
            ->join('reedem_point', 'reedem_point.id_reedem', '=', 'transaksi_reedem.reedem_id')
            ->join('member', 'member.id_member', '=', 'transaksi_reedem.member_id')
            ->where('member.id_member',$member->id_member)
            ->select('transaksi_reedem.*','reedem_point.*')
            ->get();
            $id_transaksi = $this->generateRandomId();
            DB::table('transaksi_reedem')->insert(
                [
                    'id_transaksi' => $id_transaksi,
                    'reedem_id' => $reedem_id,
                    'member_id' => $member->id_member,
                    'status' => 'Belum Dikonfirmasi',
                    'created_at' => now()
                ]
            );
        }
        //Redirect dengan status 
        // $this->reedemSaya();
        return view('user.member.reedemSaya', compact('reedem'));
    }


    //tampil reedem Saya
     public function reedemSaya(){
        $member = DB::table('member')->where('no_telp',Session::get('no_telp'))->first();
        $reedem = DB::table('transaksi_reedem')
        ->join('reedem_point', 'reedem_point.id_reedem', '=', 'transaksi_reedem.reedem_id')
        ->join('member', 'member.id_member', '=', 'transaksi_reedem.member_id')
        ->where('member.id_member',$member->id_member)
        ->select('transaksi_reedem.*','reedem_point.*')
        ->get();
        return view('user.member.reedemSaya', compact('reedem'));
    }

    //tampil Cek Reedem
    public function cekReedem($id_transaksi){
        return view('user.member.cek-reedem',compact('id_transaksi'));
    }
}
