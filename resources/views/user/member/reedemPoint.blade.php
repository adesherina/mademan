<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    @include('user.layouts.head')
</head>

<body>
    <!-- Start Main Top -->
    <header class="main-header">
        @include('user.layouts.navbar')
    </header>
    <!-- End Main Top -->

    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Reedem Point</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Reedem Point</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start Shop Page  -->
    <div class="shop-box-inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 col-12 mb-4">
                    <p class="h3">Reedem point</p>
                    <p class="lead">Akumulasi point dari pembelian produk Mademan dapat ditukarkan dengan reward menarik.</p>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                    <div class="row">
                        @foreach ($reedem as $item)
                            <div class="col-12 col-lg-6 col-md-6 mb-4">
                                <div class="card shadow p-3" style="height:550px" >
                                    <img src="{{url('/foto-reedem/'.$item->foto)}}" style="" class="card-img-top" alt="...">
                                    <div class="card-body" >
                                        <div class="reedem-title">
                                            <p class="h5 card-title" style="color: #ae8547">{{$item->judul}}</p>
                                        </div>
                                        <p class="card-text mt-2 mb-2" style="line-height:normal !important">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    </div>
                                    <div class="card-footer">
                                        <div class="point float-left">
                                            <p><i class="fa fa-money mr-2" aria-hidden="true"></i>{{$item->point}} Point</p>
                                        </div>
                                        <div class="reedem float-right">
                                            <form action="{{route('reedem.add', $item->id_reedem)}}" method="post">
                                                @csrf
                                                @method('post')
                                                <button class="btn hvr-hover text-white">Reedem</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Shop Page -->

    <!-- Start Instagram Feed  -->
    @include('user.layouts.ig')
    <!-- End Instagram Feed  -->


    <!-- Start Footer  -->
    @include('user.layouts.footer')

    <!-- ALL JS FILES -->
    @include('user.layouts.js')
</body>

</html>