<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class UserAdminController extends Controller
{
    public function data()
    {
        $users = DB::table('users')->get();

        return view('Admin.user.user', ['users' => $users]);
    }
    
    public function delete($id)
    {
        DB::table('users')->where('no_telp', $id)->delete();
        return redirect('datauser')->with('status', 'Data Berhasil Dihapus');
    }
}